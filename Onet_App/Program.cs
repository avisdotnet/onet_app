﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Onet_App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseApplicationInsights()
            .UseStartup<Startup>()
            .UseKestrel(options =>
            {
                options.listen(IPAddress.Any, 4002, listenoptions =>
                {
                    var certificate = new x509certificate2("onetcrm.pfx", "avissol");
                    listenoptions.usehttps(certificate);
                });
                options.limits.maxrequestbodysize = null;
                options.listen(IPAddress.Any, 4001);
            })
            .UseEnvironment("Development")
            .UseUrls("http://*:4001")
            .Build();
    }
}
