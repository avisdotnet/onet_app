﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class MenuMasterController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IMenuMaster menu_obj => new MenuMasterService(_db);
        public MenuMasterController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            MenuMasterVM vm_obj = new MenuMasterVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                vm_obj = menu_obj.GetAllMenus();
                if (id != null && id > 0)
                {
                    var _data = menu_obj.GetMenuByID(id.Value);
                    vm_obj.action_name = _data.action_name;
                    vm_obj.area_name = _data.area_name;
                    vm_obj.controller_name = _data.controller_name;
                    vm_obj.createdon = _data.createdon;
                    vm_obj.isactive = _data.isactive;
                    vm_obj.query_string = _data.query_string;
                    vm_obj.menu_id = _data.menu_id;
                    vm_obj.menu_name = _data.menu_name;
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return View(vm_obj);
        }
        [HttpPost]
        public IActionResult Index(MenuMasterVM model)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {

                try
                {
                StringBuilder str = new StringBuilder();
                if (String.IsNullOrWhiteSpace(model.menu_name))
                {
                    str.Append("Menu Name");
                }               
                if (String.IsNullOrWhiteSpace(model.action_name))
                {
                    if (str.Length != 0)
                        str.Append(", ");
                    str.Append("Action Name");
                }
                if (String.IsNullOrWhiteSpace(model.controller_name))
                {
                    if (str.Length != 0)
                        str.Append(", ");
                    str.Append("Controller Name");
                }
               
                if (str.Length > 0)
                {
                    str.Append(" Fields are Mandatory.");
                    return Json(Convert.ToString(str));
                }
                else
                {
                    bool _Result = menu_obj.SaveUpdateMenu(model);
                    if (_Result)
                        return Json("Menu Created Successfully.");
                    else
                        return Json("Menu Creation Failed.");
                }
                }
                catch (Exception ex)
                {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return Json("Unhandled Exception");
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
    }
}
