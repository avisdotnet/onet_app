﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IUser userservice => new UserServices(_db);
        private IRoleMaster roleservices => new RoleMasterServices(_db);

        public UserController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            UserVM user = new UserVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                user.RoleList = _db.roleMasters.ToList();
                user.RoleList.Insert(0, new RoleMasterVM { role_id = 0, role_name = "--Select--" });
                user.UserVMList = userservice.GetUsers();
                user.CampaignList = _db.campaigns.ToList();
                user.UserGroupList = _db.usergroups.Where(x => x.isactive == true)
                       .Select(obj => new UserGroupVM
                       {
                           group_name = obj.group_name,
                           group_id = obj.group_id
                       }).ToList();

                user.CampaignList.Insert(0, new Models.CampaignModel { campaign_id = 0, campaign_name = "--Select--" });
                //user.UserGroupList.Insert(0, new UserGroupVM { group_id = 0, group_name = "--Select--" });
                foreach (var item in user.UserVMList)
                {
                    if (item.role_name == 0)
                    {
                        item.role_name_text = "Admin";
                    }
                    else
                    {
                        item.role_name_text = _db.roleMasters.Where(x => x.role_id == item.role_name).Select(x => x.role_name).FirstOrDefault();
                    }
                }
                if (id != null)
                {
                    user = userservice.GetUser(id.Value);
                }
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
            return View(user);
        }
        [HttpPost]
        public IActionResult Index(UserVM model)
        {
            string result = userservice.SaveUpdateUser(model);

            TempData["msg"] = result;

            return RedirectToAction("Index", "User");
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            UserVM user = new UserVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
               
                user = userservice.GetUser(id);
                user.RoleList = _db.roleMasters.ToList();
                user.RoleList.Insert(0, new RoleMasterVM { role_id = 0, role_name = "--Select--" });
                user.CampaignList = _db.campaigns.ToList();
                user.CampaignList.Insert(0, new Models.CampaignModel { campaign_id = 0, campaign_name = "--Select--" });
                user.UserGroupAssignList = _db.usergroupassigns.Where(x => x.user_id == id).ToList();
                user.UserGroupList = _db.usergroups.Where(x => x.isactive == true)
                         .Select(obj => new UserGroupVM
                         {
                             group_name = obj.group_name,
                             group_id = obj.group_id,

                         }).ToList();
                if (user.UserGroupAssignList != null && user.UserGroupAssignList.Count != 0)
                {
                    foreach (var item in user.UserGroupList)
                    {
                        var __Data = user.UserGroupAssignList.FirstOrDefault(x => x.group_id == item.group_id && x.user_id == id);
                        if (__Data != null)
                        {
                            item.is_selected = __Data.is_selected;
                        }

                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
            return View(user);
        }
        [HttpPost]
        public IActionResult Edit(UserVM model)
        {
            string result = userservice.SaveUpdateUser(model);

            TempData["msg"] = result;

            return RedirectToAction("Index", "User");
        }
        public IActionResult EnableDisableUser(int id)
        {
            UserVM user = new UserVM();
            user = userservice.GetUser(id);
            bool result = userservice.ActiveInactiveUser(user);
            if (result)
            {
                TempData["msg"] = "Successfully Done";
            }
            else
            {
                TempData["msg"] = "Error";

            }
            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (HttpContext.Session.Keys.FirstOrDefault() != null)
                HttpContext.Session.Remove(HttpContext.Session.Keys.FirstOrDefault());
            return View();
        }

        [HttpPost]
        public IActionResult Login(UserVM userVM)
        {
           
            var result = userservice.VerifyUser(userVM);
            if (result != null)
            {
                HttpContext.Session.SetObjectAsJson(RoleType.QA, result);
                if (result.role_name_text == RoleType.Admin)
                {
                    return RedirectToAction("Index", "User");
                }
                else if (result.role_name_text == RoleType.Supervisor)
                {
                    return RedirectToAction("LeadUpload", "CRM");
                }
                else
                {
                    return RedirectToAction("CRMBucket", "CRM");
                }
            }
            else
            {
                TempData["msg"] = "Unauthorized User";
                return RedirectToAction("Login", "User");
            }
        }
        [HttpGet]
        public IActionResult ChangePassword(int id)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                UserVM user = new UserVM();
                user.username = _db.users.Where(x => x.user_id == id).Select(x => x.username).FirstOrDefault();
                user.user_id = id;
                return View(user);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpPost]
        public IActionResult ChangePassword(UserVM userModel)
        {
            bool result = userservice.ChangePassword(userModel);
            if (result == true)
            {
                TempData["msg"] = "Password Changed Successfully";
            }
            else
            {
                TempData["msg"] = "Error";
            }
            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public IActionResult ExeLogin(string Username, String Password)
        {
            UserVM userVM = new UserVM()
            {
                username = Username,
                user_password = Password
            };
            var result = userservice.VerifyUser(userVM);
            if (result != null)
            {
                HttpContext.Session.SetObjectAsJson(RoleType.QA, result);
                var _Cam = result.CampaignList.FirstOrDefault();
                if (_Cam != null)
                {
                    Logger.WriteErrLog("User Logged In", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    if (result.role_name_text == RoleType.Agent && _Cam.campaign_isautomated)
                    {
                        return RedirectToAction("index", "CRM", new { isautomate = true });

                    }
                    else if (result.role_name_text == RoleType.Agent && !_Cam.campaign_isautomated)
                    {
                        return RedirectToAction("CRMBucket", "CRM");
                    }
                    else
                        return Json("Username not mapped with CRM Page. Contact Administrator");
                }
                else
                {
                    Logger.WriteErrLog("User Campaign Not exist", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return Json("User Campaign Not exist. Contact Administrator");
                }

            }
            else
            {
                return Json("Unauthorized User. Contact Administrator");
            }
        }
    }
}