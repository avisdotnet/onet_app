﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class TemplateController : Controller
    {
        private readonly ApplicationDbContext _db;
        private ITemplate template => new TemplateServices(_db);

        public TemplateController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult TemplateCreation(int? Id)
        {
                return View();
        }
        [HttpPost]
        public IActionResult TemplateCreation(TemplateVM templateVM)
        {
           try
            {
                StringBuilder str = new StringBuilder();
                if (String.IsNullOrWhiteSpace(templateVM.template_name))
                {
                    str.Append("Templete Name");
                }
                if (String.IsNullOrWhiteSpace(templateVM.template_action_name))
                {
                    if (str.Length != 0)
                        str.Append(", ");
                    str.Append("Templete Action Name");
                }
                if (String.IsNullOrWhiteSpace(templateVM.template_controller_name))
                {
                    if (str.Length != 0)
                        str.Append(", ");
                    str.Append("Templete Controller Name");
                }

                if (str.Length > 0)
                {
                    str.Append(" Fields are Mandatory.");
                    return Json(Convert.ToString(str));
                }
                else
                {
                    bool result = template.SaveTemplateCreation(templateVM);
                    if (result)
                        return Json("Templete Created Successfully.");
                    else
                        return Json("Templete Creation Failed.");
                }
               
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return Json("Unhandled Exception");
            }
          
        }
    }
}
