﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models;
using Onet_App.Models.ViewModel;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.Services;

namespace Onet_App.Controllers
{
    public class SMSTemplateController : Controller
    {
        private readonly ApplicationDbContext _db;
        ISMSTemplateServices smsServices => new SMSTemplateServices(_db);
        IKeywordServices keywordServices => new KeywordServices(_db);
        public SMSTemplateController(ApplicationDbContext db){  _db = db;   }

        [HttpGet]
        public IActionResult Index(int? id)
        {
            SMSTemplateVM sms = new SMSTemplateVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                sms.createdby = _userModel.user_id;
                sms.updatedby = _userModel.user_id;
                sms.createdbyname = _userModel.user_fullname;
                sms.updatedbyname = _userModel.user_fullname;

                var list = keywordServices.GetAllKeyword();
                if (list != null)
                {
                    sms.KeywordList = list;
                }

                sms.SMSTemplateVMList = smsServices.GetSMSTemplateList();

                if (id != null && id != 0)
                {
                    sms = smsServices.GetSMSTemplateById(id.Value);
                }
            }
            else
            {
               return RedirectToAction("Login", "User");
            }
            return View(sms);
        }

        [HttpPost]
        public IActionResult Index(SMSTemplateVM sms)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);

            sms.createdby = _userModel.user_id;
            sms.updatedby = _userModel.user_id;
            sms.createdbyname = _userModel.user_fullname;
            sms.updatedbyname = _userModel.user_fullname;

            if (_userModel != null)
            {
                string result = string.Empty;
                result = smsServices.SaveUpdateSMSTemplate(sms);
                TempData["Msg"] = result;
                return RedirectToAction("Index", "SMSTemplate", new { id=0});
            }
            else
            {
               return RedirectToAction("Login", "User");
            }
        }

        public IActionResult DeleteTemplate(SMSTemplateVM sms)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);

            sms.createdby = _userModel.user_id;
            sms.updatedby = _userModel.user_id;
            sms.createdbyname = _userModel.user_fullname;
            sms.updatedbyname = _userModel.user_fullname;

            if (_userModel != null)
            {
                bool result = smsServices.DeleteTemplate(sms);
                if (result)
                    TempData["Msg"] = "Deleted Successfully";
                else
                    TempData["Msg"] = "Error";
                return RedirectToAction("Index", "SMSTemplate");
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

    }
}