﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Onet_App.Models.Services;
using Onet_App.Models.ViewModel;
namespace Onet_App.Controllers
{
    [Route("api/DispositionMapping")]
    public class DispositionMappingController : Controller
    {
        public IConfiguration Configuration { get; }
        private readonly ApplicationDbContext _db;
        public DispositionMappingController(ApplicationDbContext db, IConfiguration configuration)
        {
            _db = db;
            Configuration = configuration;
        }
        [HttpGet]
        public IActionResult Disposition(string Phone, string disposition, string campaign)
        {
            if (String.IsNullOrWhiteSpace(Phone) || String.IsNullOrWhiteSpace(disposition) || string.IsNullOrWhiteSpace(campaign))
            {
                return Json("Parameter Values Missing");
            }
            else
            {
                try
                {

                    var _Campaign = _db.campaigns.FirstOrDefault(c => c.campaign_name.ToLower() == campaign.ToLower());
                    if (_Campaign != null)
                    {
                        var num = _Campaign.campaign_id;
                        var name = Phone;
                        //Find the Record from crms table on basis of campaing id and phone no. 
                        var _data = _db.crms.FirstOrDefault(x => x.campaign_id == _Campaign.campaign_id && ((x.phone1 == Phone) || (x.phone2 == Phone) || (x.phone3 == Phone) || (x.phone4 == Phone) || (x.give_new_contact_no == Phone)));
                        if (_data != null)
                        {
                            //Set the Data for NGUCC Dispostion and Last Contacted Number fields
                            _data.dailer_disposition = disposition;
                            _data.last_called_phoneno = Phone;
                            _db.SaveChanges();
                            return Json("Data Saved Successfully.");
                        }
                        else
                        {
                            return Json("Data with Campaign and Phone NO not Matched in the Database.");
                        }
                    }
                    else
                    {
                        Logger.WriteErrLog(campaign + " Campaign Not Exist", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        return Json("Campaign not Exist.");
                    }

                }
                catch (Exception ex)
                {
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return Json("Exception Occured. Please check the Values");
                }
            }
        }
    }

    public class ResponseDispositionObj
    {
        public string Phone { get; set; }
        public string disposition { get; set; }
        public string campaign { get; set; }
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }

}
