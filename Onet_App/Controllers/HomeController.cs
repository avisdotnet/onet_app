﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models;

namespace Onet_App.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Logger.WriteErrLog("Demo Testing", String.Format("{0} ==> {1}", this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
