﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Onet_App.Models;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class CRMController : Controller
    {
        public IConfiguration Configuration { get; }
        private readonly ApplicationDbContext _db;
        private ICRMServices crm_obj => new CRMServices(_db);
        private IDispositionServices dispositionServices => new DispositionServices(_db);
        private ICampaignServices campaignservices => new CampaignServices(_db);
        private ILeadAssignmentServices leadassignServices => new LeadAssignmentServices(_db);
        private IRemarkServices remark_services => new RemarkServices(_db);
        private IActivityServices activityServices => new ActivityServices(_db);
        private IManualAssignmentService manualAssignmentService => new ManualAssignmentService(_db);
        public CRMController(ApplicationDbContext db, IConfiguration configuration)
        {
            _db = db;
            Configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index(string mobile, bool isautomate)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM CrmVM = new CRMVM();
                StringBuilder str = new StringBuilder();

                CrmVM.isautomate = isautomate;
                CrmVM.dispositionModels = dispositionServices.GetAllParentDisposition();

                if (_userModel.campaign_id > 0)
                {
                    CrmVM.campaign_name = _db.campaigns.FirstOrDefault(x => x.campaign_id == _userModel.campaign_id).campaign_name;
                    if (CrmVM.campaign_name.ToLower().Trim() == Configuration.GetConnectionString("Campaign").ToLower().Trim())
                    {
                        CrmVM.EndorserModelList = _db.endorsers.ToList();
                    }
                    else
                    {
                        CrmVM.EndorserModelList = _db.endorsers.Where(x => x.campaign_id == _userModel.campaign_id).ToList();
                    }
                }
                CrmVM.EndorserModelList.Insert(0, new EndorserModel { id = 0, endorser_name = "--Select--" });
                #region Balance for Collection
                CrmVM.balanceForCollectionModels = crm_obj.GetAllBalanceForCollection();
                CrmVM.balanceForCollectionModels.Insert(0, new BalanceForCollectionModel { id = 0, balance_collection_name = "--Select--" });
                #endregion
                CrmVM.campaign_id = _userModel.campaign_id;
                CrmVM.cityModels = crm_obj.GetAllCity();
                CrmVM.cityModels.Insert(0, new CityModel { city_id = 0, city_name = "--Select--" });
                CrmVM.modeOfSettlementModels = crm_obj.GetAllModeOfSettlement();
                CrmVM.modeOfSettlementModels.Insert(0, new ModeOfSettlementModel { id = 0, modeofsettlement_name = "--Select--" });
                CrmVM.paymentChannelModels = crm_obj.GetAllPayementChannel();
                CrmVM.paymentChannelModels.Insert(0, new PaymentChannelModel { id = 0, payment_channel_name = "--Select--" });
                CrmVM.reasonForNonPaymentModels = crm_obj.GetAllReasonForNonPayment();
                CrmVM.reasonForNonPaymentModels.Insert(0, new ReasonForNonPaymentModel { id = 0, reason_for_non_payment_name = "--Select--" });
                CrmVM.regionModel = crm_obj.GetAllRegion();
                CrmVM.regionModel.Insert(0, new RegionModel { id = 0, region_name = "--Select--" });
                CrmVM.skipTraceModels = crm_obj.GetAllSkipTrace();
                CrmVM.skipTraceModels.Insert(0, new SkipTraceModel { id = 0, skip_trace_name = "--Select--" });
                CrmVM.statusModels = crm_obj.GetAllStatus();
                CrmVM.statusModels.Insert(0, new StatusModel { id = 0, status_name = "--Select--" });

                if (!String.IsNullOrWhiteSpace(mobile))
                {
                    CRMModel Data = new CRMModel();
                    if (_userModel.role_name_text == RoleType.Agent)
                    {
                        Data = crm_obj.GetCrmByMobileNoAndCamp(mobile, _userModel.campaign_id);
                    }
                    else
                    {
                        Data = crm_obj.GetCrmByMobileNo(mobile);
                    }

                    if (Data != null)
                    {
                        CrmVM.OnetId = "Onet" + Data.crm_id.ToString();
                        CrmVM.crm_id = Data.crm_id;
                        CrmVM.account_name = Data.account_name;
                        CrmVM.account_no = Data.account_no;
                        CrmVM.address = Data.address;
                        CrmVM.agent_id = Data.agent_id;
                        CrmVM.age_group = Data.age_group;
                        CrmVM.amount_collected = Data.amount_collected;
                        CrmVM.authorized_representatives = Data.authorized_representatives;
                        CrmVM.balance_for_collection = Data.balance_for_collection;
                        CrmVM.company_name = Data.company_name;
                        CrmVM.phone1 = Data.phone1;
                        CrmVM.count_of_attempted_contact_numbers = Data.count_of_attempted_contact_numbers;
                        CrmVM.createdby = Data.createdby;
                        CrmVM.date_invoice = Data.date_invoice.HasValue == true ? Data.date_invoice : (DateTime?)null;
                        CrmVM.email = Data.email;
                        CrmVM.financial_contact_person = Data.financial_contact_person;
                        CrmVM.give_new_contact_no = Data.give_new_contact_no;
                        CrmVM.invoice_amount = Data.invoice_amount;
                        CrmVM.invoice_no = Data.invoice_no;
                        CrmVM.mode_of_settlement = Data.mode_of_settlement;
                        CrmVM.number_of_accounts = Data.number_of_accounts;
                        CrmVM.outstanding_balance = Data.outstanding_balance;
                        CrmVM.overdue_balance = Data.overdue_balance;
                        CrmVM.payment_channel = Data.payment_channel;
                        CrmVM.phone2 = Data.phone2;
                        CrmVM.phone3 = Data.phone3;
                        CrmVM.phone4 = Data.phone4;
                        CrmVM.ptp_amount = Data.ptp_amount;
                        CrmVM.ptp_date = Data.ptp_date.HasValue == true ? Data.ptp_date : (DateTime?)null;
                        CrmVM.ptp_dated_by = Data.ptp_dated_by;
                        CrmVM.recommendation = Data.recommendation;
                        CrmVM.region_primary = Data.region_primary;
                        CrmVM.region_secondary = Data.region_secondary;
                        CrmVM.city = Data.city;
                        CrmVM.service_id_no = Data.service_id_no;
                        CrmVM.service_type = Data.service_type;
                        CrmVM.skip_trace = Data.skip_trace;
                        CrmVM.skip_trace_child = Data.skip_trace_child;
                        CrmVM.status = Data.status;
                        CrmVM.supervisor_id = Data.supervisor_id;
                        CrmVM.remarklist = remark_services.GetRemarkByLeadId(CrmVM.crm_id);
                        CrmVM.activityHistoryList = activityServices.GetActivityByLeadId(CrmVM.crm_id);
                        CrmVM.campaign_id = Data.campaign_id;
                        CrmVM.endorsement_id = Data.endorsement_id;
                        CrmVM.list_name = Data.list_name;
                    }
                    else
                    {
                        int id = 0;
                        //var _id = _db.crms.LastOrDefault();
                        //if (_id != null)
                        //    id = _id.crm_id;
                        CrmVM.crm_id = 0;
                        CrmVM.OnetId = "Onet" + id.ToString();
                        CrmVM.phone1 = mobile;
                        CrmVM.remarklist = new List<RemarkVM>();
                        CrmVM.activityHistoryList = new List<ActivityHistoryVM>();
                    }
                }
                return View(CrmVM);
            }
            else
            {
                Logger.WriteErrLog("Session Expired", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                //Logger.WriteErrLog(, "CRM=>Index");
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public IActionResult Index(CRMVM model, string CommandName)
        {
            string url = "#";
            try
            {
                if (CommandName == "Add")
                {
                    UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
                    model.createdby = _userModel.user_id;
                    model.agent_id = _userModel.user_id;
                    model.createdon = DateTime.Now;
                    model.createdbyname = _userModel.user_fullname;
                    model.campaign_id = _userModel.campaign_id;
                    string endorser_name = _db.endorsers.FirstOrDefault(x => x.id == model.endorsement_id).endorser_name;
                    if (String.IsNullOrWhiteSpace(model.list_name))
                        model.list_name = String.Format("{0}{1}", endorser_name, DateTime.Now.ToString("ddMMMyyyyhhmmss"));
                    bool _Result = crm_obj.AddEditCRM(model, Configuration);


                    if (model.crm_id > 0)
                    {
                        if (_Result == true)
                        {
                            TempData["Msg"] = "Successfully Updated";
                        }
                        else
                        {
                            TempData["Msg"] = "Failed";
                        }
                    }
                    else
                    {
                        if (_Result == true)
                        {
                            TempData["Msg"] = "Successfully Added";
                        }
                        else
                        {
                            TempData["Msg"] = "Failed";
                        }
                    }
                }

                else if (CommandName == "Add Activity")
                {
                    UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
                    model.createdby = _userModel.user_id;
                    model.agent_id = _userModel.user_id;
                    model.createdon = DateTime.Now;
                    model.createdbyname = _userModel.user_fullname;
                    model.campaign_id = _userModel.campaign_id;
                    if (model.crm_id == 0)
                        crm_obj.AddEditCRM(model, Configuration);
                    var Data = crm_obj.GetCrmByMobileNo(model.phone1);
                    if (Data != null)
                    {
                        model.crm_id = Data.crm_id;
                        bool result = crm_obj.AddActivityDetails(model);
                        if (result == true)
                        {
                            TempData["Msg"] = "Successfully Added";
                        }
                        else
                        {
                            TempData["Msg"] = "Failed";
                        }
                    }
                    else
                    {
                        TempData["Msg"] = "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            
            //url = string.Format("/CRM/Index?mobile={0}&isautomate={1}", model.phone1, model.isautomate);
            return Redirect(Url.Action("Index", "CRM", new { mobile = model.phone1, isautomate = model.isautomate }));
        }

        [HttpGet]
        public IActionResult CRMBucket()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM crmvm_obj = new CRMVM();
                crmvm_obj.dispositionModels = dispositionServices.GetAllParentDisposition();
                crmvm_obj.dispositionModels.Insert(0, new DispositionVM { disposition_id = 0, disposition_name = "--Select--" });
                List<int> _Ids = new List<int>();
                var data = crm_obj.GetCrmDetails(_userModel.campaign_id);
                if (_userModel.CampaignList != null)
                {
                    data = new List<CRMVM>();
                    foreach (var item in _userModel.CampaignList)
                    {
                        data.AddRange(crm_obj.GetCrmDetails(item.campaign_id));
                    }
                }
                crmvm_obj.role_name = _db.roleMasters.Where(x => x.role_id == _userModel.role_name).FirstOrDefault().role_name.Trim();

                if (_userModel.role_name_text == RoleType.Agent)
                {
                    crmvm_obj.LeadAssignmentVMList = leadassignServices.GetLeadByAssignedAgentId(_userModel.user_id);
                    if (crmvm_obj.LeadAssignmentVMList.Count > 0)
                    {
                        _Ids = crmvm_obj.LeadAssignmentVMList.Select(x => x.lead_id).ToList();
                        if (_Ids != null && _Ids.Count > 0 && data != null && data.Count > 0)
                        {
                            crmvm_obj._crmlst = data.Where(x => _Ids.Contains(x.crm_id)).ToList();
                        }
                    }
                }
                else if (_userModel.role_name_text == RoleType.Supervisor)
                {
                    List<int> AgentAssignedListIds = new List<int>();
                    var Supervisior_userGroupIds = _db.usergroupassigns.Where(x => x.user_id == _userModel.user_id && x.is_selected == true).Select(x => x.group_id).ToList();
                    foreach (var item in Supervisior_userGroupIds)
                    {
                        AgentAssignedListIds = _db.groupsuserassigns.Where(x => x.group_id == item && x.is_selected == true).Select(x => x.agent_id).ToList();
                    }
                    _Ids = _db.leadassigns.Where(x => AgentAssignedListIds.Contains(x.agent_id)).Select(x => x.lead_id).ToList();

                    if (_Ids != null && _Ids.Count > 0 && data != null && data.Count > 0)
                    {
                        crmvm_obj._crmlst = data.Where(x => _Ids.Contains(x.crm_id)).ToList();
                    }
                }
                else
                {
                    crmvm_obj._crmlst = crm_obj.GetAllCrmDetails();
                }

                if (_userModel.role_name_text == RoleType.Agent)
                {
                    crmvm_obj._crmlst = crmvm_obj._crmlst.Where(x => x.campaign_id == _userModel.campaign_id).ToList();
                    crmvm_obj._crmlst = crmvm_obj._crmlst.Where(x => x.callout_status_level1_Text.ToLower() != Convert.ToString(Configuration.GetConnectionString("FirstDisposition")).ToLower()).ToList();
                    crmvm_obj._crmlst = crmvm_obj._crmlst.Where(x => x.callout_status_level1_Text.ToLower() != Convert.ToString(Configuration.GetConnectionString("SecondDispoaition")).ToLower()).ToList();

                }


                crmvm_obj.CallBackList = crm_obj.GetCallBacksDetails();

                return View(crmvm_obj);

            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public IActionResult CRMBucket(CRMVM model)
        {
            List<int> _Ids = new List<int>();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            var data = crm_obj.GetAllCrmDetails().Where(x => x.campaign_id == _userModel.campaign_id).ToList();
            List<int> AgentAssignedListIds = new List<int>();
            var Supervisior_userGroupIds = _db.groupsuserassigns.Where(x => x.agent_id == _userModel.user_id && x.is_selected == true).Select(x => x.group_id).ToList();
            foreach (var item in Supervisior_userGroupIds)
            {
                AgentAssignedListIds = _db.groupsuserassigns.Where(x => x.group_id == item && x.is_selected == true).Select(x => x.agent_id).ToList();
            }
            _Ids = _db.leadassigns.Where(x => AgentAssignedListIds.Contains(x.agent_id)).Select(x => x.lead_id).ToList();
            Logger.WriteErrLog(Convert.ToString(_Ids.Count()), String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            if (_Ids != null && _Ids.Count > 0)
            {
                model._crmlst = data.Where(x => _Ids.Contains(x.crm_id)).ToList();
            }
            var _data = model._crmlst;
            if (model.filter_text == "Last_Disposition")
            {
                model._crmlst = _data.Where(x => x.callout_status_level1 == model.callout_status_level1).ToList();
            }
            else
            {
                model._crmlst = _data.Where(x => x.updatedon == model.filter_date).ToList();
            }
            model.role_name = _db.roleMasters.Where(x => x.role_id == _userModel.role_name).FirstOrDefault().role_name.Trim();
            return View(model);
        }
        public JsonResult GetCallBackData(String MobileNo)
        {
            return Json(true);
        }

        [HttpGet]
        public IActionResult LeadUpload()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM crm_obj = new CRMVM();
                crm_obj.EndorserModelList = _db.endorsers.ToList();
                //crm_obj.EndorserModelList.Insert(0, new EndorserModel { id = 0, endorser_name = "--Select--" });
                crm_obj.campaignlist = campaignservices.GetAllCampaign(_userModel);
                //crm_obj.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                return View(crm_obj);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public IActionResult LeadUpload(CRMVM crmVM, string CommandName)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            int mode = 0, region_primary = 0, region_secondary = 0, skip_tracer_child = 0, skip_tracer = 0, status = 0, callout1 = 0, callout2 = 0, callout3 = 0, callout4 = 0, callout5 = 0, balance = 0;
            try
            {
                if (CommandName == "Upload")
                {
                    if (crmVM.campaign_id > 0)
                    {
                        DataTable _ds = UploadExcel();
                        var _SettlementData = crm_obj.GetAllModeOfSettlement();

                        if (_ds != null && _ds.Rows.Count > 0)
                        {
                            StringBuilder BUilder = new StringBuilder();
                            int i = 0;
                            List<CRMVM> CRMVMlst = new List<CRMVM>();
                            foreach (DataRow item in _ds.Rows)
                            {
                                #region Mode of settlement
                                var mode_text = String.IsNullOrWhiteSpace(Convert.ToString(item["mode_of_settlement"])) == true ? null : Convert.ToString(item["mode_of_settlement"]);
                                if (mode_text != null)
                                {
                                    var _mode = _SettlementData.Where(x => x.modeofsettlement_name.ToLower() == Convert.ToString(item["mode_of_settlement"]).ToLower()).ToList();
                                    if (_mode != null && _mode.FirstOrDefault() != null)
                                    {
                                        mode = _mode.FirstOrDefault().id;
                                    }
                                    else
                                    {
                                        mode = crm_obj.SaveModeofSettlement(Convert.ToString(item["mode_of_settlement"]));
                                        _SettlementData = crm_obj.GetAllModeOfSettlement();
                                    }
                                }
                                #endregion

                                #region Region
                                var region_primary_text = String.IsNullOrWhiteSpace(Convert.ToString(item["region_primary"])) == true ? null : Convert.ToString(item["region_primary"]);
                                if (region_primary_text != null)
                                {
                                    region_primary = crm_obj.GetRegionPrimaryId(Convert.ToString(item["region_primary"]));
                                    if (region_primary == 0)
                                        region_primary = crm_obj.SaveRegionPrimary(Convert.ToString(item["region_primary"]));
                                }

                                var region_secondary_text = String.IsNullOrWhiteSpace(Convert.ToString(item["region_secondary"])) == true ? null : Convert.ToString(item["region_secondary"]);
                                if (region_secondary_text != null)
                                {
                                    region_secondary = crm_obj.GetRegionSecondaryId(Convert.ToString(item["region_secondary"]));
                                    if (region_secondary == 0)
                                        region_secondary = crm_obj.SaveRegionSecondary(Convert.ToString(item["region_primary"]), Convert.ToString(item["region_secondary"]));
                                }
                                #endregion

                                #region Skip Trace
                                var skip_tracer_text = String.IsNullOrWhiteSpace(Convert.ToString(item["skip_trace"])) == true ? null : Convert.ToString(item["skip_trace"]);
                                if (skip_tracer_text != null)
                                {
                                    skip_tracer = crm_obj.GetSkipTracerId(Convert.ToString(item["skip_trace"]));
                                    if (skip_tracer == 0)
                                        skip_tracer = crm_obj.SaveSkipTracer(Convert.ToString(item["skip_trace"]));
                                }


                                var skip_tracer_child_text = String.IsNullOrWhiteSpace(Convert.ToString(item["skip_trace_child"])) == true ? null : Convert.ToString(item["skip_trace_child"]);
                                if (skip_tracer_child_text != null)
                                {
                                    skip_tracer_child = crm_obj.GetSkipTracerChildId(Convert.ToString(item["skip_trace_child"]));
                                    if (skip_tracer_child == 0)
                                        skip_tracer_child = crm_obj.SaveSkipTracerChild(Convert.ToString(item["skip_trace"]), Convert.ToString(item["skip_trace_child"]));
                                }
                                #endregion

                                #region Status
                                var status_text = String.IsNullOrWhiteSpace(Convert.ToString(item["status"])) == true ? null : Convert.ToString(item["status"]);
                                if (status_text != null)
                                {
                                    status = crm_obj.GetStatusId(Convert.ToString(item["status"]));
                                    if (status == 0)
                                        status = crm_obj.SaveStatus(Convert.ToString(item["status"]));
                                }
                                #endregion

                                #region CallOutStatus
                                var callout1_text = String.IsNullOrWhiteSpace(Convert.ToString(item["callout_status_level1"])) == true ? null : Convert.ToString(item["callout_status_level1"]);
                                if (callout1_text != null)
                                {
                                    callout1 = crm_obj.GetCalloutstatusId(Convert.ToString(item["callout_status_level1"]));
                                    if (callout1 == 0)
                                        callout1 = crm_obj.SaveCallOutStatus(Convert.ToString(item["callout_status_level1"]));
                                }

                                var callout2_text = String.IsNullOrWhiteSpace(Convert.ToString(item["callout_status_level2"])) == true ? null : Convert.ToString(item["callout_status_level2"]);
                                if (callout2_text != null)
                                {
                                    callout2 = crm_obj.GetCalloutstatusId(Convert.ToString(item["callout_status_level2"]));
                                    if (callout2 == 0)
                                        callout2 = crm_obj.SaveCallOutStatusChild(Convert.ToString(item["callout_status_level1"]), Convert.ToString(item["callout_status_level2"]));
                                }

                                var callout3_text = String.IsNullOrWhiteSpace(Convert.ToString(item["callout_status_level3"])) == true ? null : Convert.ToString(item["callout_status_level3"]);
                                if (callout3_text != null)
                                {
                                    callout3 = crm_obj.GetCalloutstatusId(Convert.ToString(item["callout_status_level3"]));
                                    if (callout3 == 0)
                                        callout3 = crm_obj.SaveCallOutStatusChild(Convert.ToString(item["callout_status_level2"]), Convert.ToString(item["callout_status_level3"]));
                                }

                                var callout4_text = String.IsNullOrWhiteSpace(Convert.ToString(item["callout_status_level4"])) == true ? null : Convert.ToString(item["callout_status_level4"]);
                                if (callout4_text != null)
                                {
                                    callout4 = crm_obj.GetCalloutstatusId(Convert.ToString(item["callout_status_level4"]));
                                    if (callout4 == 0)
                                        callout4 = crm_obj.SaveCallOutStatusChild(Convert.ToString(item["callout_status_level3"]), Convert.ToString(item["callout_status_level4"]));
                                }

                                var callout5_text = String.IsNullOrWhiteSpace(Convert.ToString(item["callout_status_level5"])) == true ? null : Convert.ToString(item["callout_status_level1"]);
                                if (callout5_text != null)
                                {
                                    callout5 = crm_obj.GetCalloutstatusId(Convert.ToString(item["callout_status_level5"]));
                                    if (callout5 == 0)
                                        callout5 = crm_obj.SaveCallOutStatusChild(Convert.ToString(item["callout_status_level4"]), Convert.ToString(item["callout_status_level5"]));
                                }
                                #endregion

                                #region BalanceForCollection
                                var balance_text = String.IsNullOrWhiteSpace(Convert.ToString(item["balance_for_collection"])) == true ? null : Convert.ToString(item["balance_for_collection"]);
                                if (balance_text != null)
                                {
                                    balance = crm_obj.GetBalanceForCollectionsId(Convert.ToString(item["balance_for_collection"]));
                                    if (balance == 0)
                                        balance = crm_obj.SaveBalanceForCollections(Convert.ToString(item["balance_for_collection"]));
                                }
                                #endregion

                                var _Status = (new CRMVM()
                                {
                                    account_name = Convert.ToString(item["account_name"]),
                                    account_no = Convert.ToString(item["account_no"]),
                                    address = Convert.ToString(item["address"]),
                                    age_group = Convert.ToString(item["age_group"]),
                                    amount_collected = Convert.ToString(item["amount_collected"]),
                                    authorized_representatives = Convert.ToString(item["authorized_representatives"]),
                                    balance_for_collection = balance,
                                    callout_status_level1 = callout1,
                                    callout_status_level2 = callout2,
                                    callout_status_level3 = callout3,
                                    callout_status_level4 = callout4,
                                    callout_status_level5 = callout5,
                                    city = Convert.ToString(item["city"]),
                                    company_name = Convert.ToString(item["company_name"]),
                                    phone1 = Convert.ToString(item["phone1"]),
                                    count_of_attempted_contact_numbers = Convert.ToString(item["count_of_attempted_contact_numbers"]),
                                    email = Convert.ToString(item["email"]),
                                    financial_contact_person = Convert.ToString(item["financial_contact_person"]),
                                    give_new_contact_no = Convert.ToString(item["give_new_contact_no"]),
                                    invoice_amount = Convert.ToString(item["invoice_amount"]),
                                    invoice_no = Convert.ToString(item["invoice_no"]),
                                    mode_of_settlement = mode,
                                    number_of_accounts = Convert.ToString(item["number_of_accounts"]),
                                    outstanding_balance = Convert.ToString(item["outstanding_balance"]),
                                    overdue_balance = Convert.ToString(item["overdue_balance"]),
                                    phone2 = Convert.ToString(item["phone2"]),
                                    phone3 = Convert.ToString(item["phone3"]),
                                    ptp_amount = Convert.ToString(item["ptp_amount"]),
                                    ptp_date = String.IsNullOrWhiteSpace(Convert.ToString(item["ptp_date"])) == true ? (DateTime?)null : Convert.ToDateTime(item["ptp_date"]),
                                    ptp_dated_by = Convert.ToString(item["ptp_dated_by"]),
                                    recommendation = Convert.ToString(item["recommendation"]),
                                    region_primary = region_primary,
                                    region_secondary = region_secondary,
                                    remarks = Convert.ToString(item["remarks"]),
                                    service_id_no = Convert.ToString(item["service_id_no"]),
                                    service_type = Convert.ToString(item["service_type"]),
                                    skip_trace = skip_tracer,
                                    status = status,
                                    skip_trace_child = skip_tracer_child,
                                    phone_extension = Convert.ToString(item["phone_extension"]),
                                    agent_id = _userModel.user_id,
                                    createdby = _userModel.user_id,
                                    createdbyname = _userModel.user_fullname,
                                    createdon = DateTime.Now,
                                    isactive = true,
                                    updatedby = _userModel.user_id,
                                    updatedon = DateTime.Now,
                                    updatedbyname = _userModel.user_fullname,
                                    campaign_id = crmVM.campaign_id,
                                    endorsement_id = crmVM.endorsement_id,
                                    list_name = crmVM.list_name,
                                    enable_get_lead = crmVM.enable_get_lead,
                                    updatedtimes = 0,
                                });
                                CRMVMlst.Add(_Status);
                                i++;
                            }
                            var result = crm_obj.AddCRM(CRMVMlst, Configuration, crmVM.campaign_id, crmVM.endorsement_id);
                            if (result == i)
                            {
                                TempData["msg"] = "Successful";
                                return RedirectToAction("LeadUpload", "CRM");
                            }
                            else if (result == -2)
                            {
                                TempData["msg"] = "Lead with same account number already exist";
                                return RedirectToAction("LeadUpload", "CRM");
                            }
                            else if (result == -3)
                            {
                                TempData["msg"] = "Lead with same account name already exist";
                                return RedirectToAction("LeadUpload", "CRM");
                            }
                            else if (result == -4)
                            {
                                TempData["msg"] = "Lead with same invoice number already exist";
                                return RedirectToAction("LeadUpload", "CRM");
                            }
                            else if (result == -5)
                            {
                                TempData["msg"] = String.Format("Lead with phone number already exists");
                                return RedirectToAction("LeadUpload", "CRM");
                            }
                            else
                            {
                                TempData["msg"] = String.Format("Successful");
                                return RedirectToAction("LeadUpload", "CRM");
                            }
                        }
                        else
                        {
                            TempData["msg"] = "No Record Found in File.";
                            return RedirectToAction("LeadUpload", "CRM");
                        }
                    }
                    else
                    {
                        TempData["msg"] = "Select a Campaign";
                        return RedirectToAction("LeadUpload", "CRM");
                    }
                }
                else if (CommandName == "Get Format")
                {
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Format", "Format.xlsx");
                    byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                    string fileName = "Format.xlsx";
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    TempData["msg"] = "Unknown Action";
                    return RedirectToAction("LeadUpload", "CRM");
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteErrLog(ex, "LeadUpload");
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                TempData["msg"] = "Something went Wrong. Please Share log Files with System Administrator.";
                return RedirectToAction("LeadUpload", "CRM");
            }
        }

        private DataTable UploadExcel()
        {
            try
            {
                DataTable dataSet = new DataTable();
                if (Request.Form.Files["postedFile"].Length > 0)
                {
                    var filename = Path.GetFileName(Request.Form.Files["postedFile"].FileName);
                    string extension = Path.GetExtension(filename).ToLower();
                    string[] validFileTypes = { ".xls", ".xlsx", ".csv" };
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", filename);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
                    }
                    if (validFileTypes.Contains(extension))
                    {
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            Request.Form.Files["postedfile"].CopyTo(stream);
                        }
                        #region Read Excel Data and Insert in Temporary Table                      
                        string value = string.Empty;
                        using (SpreadsheetDocument doc = SpreadsheetDocument.Open(path, true))
                        {
                            var sheet = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().First();
                            var worksheetPart = (WorksheetPart)doc.WorkbookPart.GetPartById(sheet.Id.Value);
                            IEnumerable<Row> rows = worksheetPart.Worksheet.GetFirstChild<SheetData>().Elements<Row>();
                            foreach (Row row in rows)
                            {
                                #region get First Row as Header from Excel file
                                if (row.RowIndex.Value == 1)
                                {
                                    foreach (Cell cell in row.Descendants<Cell>())
                                    {
                                        dataSet.Columns.Add(GetCellValue(doc, cell).Trim(), typeof(String));
                                    }
                                }
                                #endregion
                                else
                                {
                                    DataRow dr = dataSet.NewRow();
                                    #region Insert All records in Table
                                    //From second row of excel onwards, add data rows to data table.
                                    IEnumerable<Cell> cells = GetCellsFromRowIncludingEmptyCells(row);
                                    string columnValues = null;
                                    int i = 0;
                                    foreach (Cell currentCell in cells)
                                    {
                                        value = GetCellValue(doc, currentCell);
                                        if (string.IsNullOrEmpty(value) == true)
                                            value = "";
                                        bool isNumeric = !string.IsNullOrEmpty(value) && value.All(Char.IsDigit);

                                        columnValues = value;
                                        dr[i] = columnValues;
                                        i++;
                                    }
                                    #endregion
                                    dataSet.Rows.Add(dr);
                                }
                            }
                            doc.Close();
                        }
                        #endregion
                        return dataSet;
                    }
                    else
                    {
                        ViewBag.Error = "Please Upload Files in .xls, .xlsx or .csv format";
                        return null;
                    }
                }
                else
                {
                    ViewBag.Error = "Please Choose File for uploading Data.";
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteErrLog(ex, "Lead=>LeadUpload");
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }

        private string GetCellValue(SpreadsheetDocument doc, Cell cell)
        {
            Regex rgx = new Regex(@"^[0-3][0-9]\/[0-1][0-9]\/[0-9]{4} [0-1][0-9]:[0-5][0-9]:[0-5][0-9]$");

            var _Type = false;
            if (cell == null)
                return null;
            if (cell.DataType == null)
            {
                return cell.InnerText;
            }
            string value = cell.InnerText;
            _Type = rgx.IsMatch(cell.InnerText);
            if (_Type == true)
                value = DateTime.Parse(cell.InnerText).ToShortDateString();

            if (cell.DataType != null && cell.DataType.Value == DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString)
            {
                return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
            }
            return value.Trim();
        }

        private static IEnumerable<Cell> GetCellsFromRowIncludingEmptyCells(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }
        public static int ConvertColumnNameToNumber(string columnName)
        {
            Regex alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            int convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                int current = i == 0 ? letter - 65 : letter - 64; // ASCII 'A' = 65
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        public static string GetColumnName(string cellReference)
        {
            // Match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);

            return match.Value;
        }

        [HttpGet]
        public IActionResult LeadDownload()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM crmvm = new CRMVM();
                crmvm.campaignlist = campaignservices.GetAllCampaign(_userModel);
                crmvm.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                return View(crmvm);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public IActionResult LeadDownload(CRMVM model, string CommandName, DateTime FromDate, DateTime TODate)
        {
            if (CommandName == "Submit")
            {
                UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
                if (_userModel != null)
                {
                    model.campaignlist = campaignservices.GetAllCampaign(_userModel);
                    model.cRMDATAVMs = crm_obj.DownloadCrmDetails(model.campaign_id, model.list_name, FromDate.Date, TODate.Date);

                    model.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
            else if (CommandName == "Download")
            {
                try
                {
                    model.cRMDATAVMs = crm_obj.DownloadCrmDetails(model.campaign_id, model.list_name, FromDate.Date, TODate.Date);
                    string Filenmae = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                    var _path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Download");
                    System.IO.Directory.CreateDirectory(_path);
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Download", Filenmae);
                    DataTable _ds = Logger.ToDataTable<CRMDATAVM>(model.cRMDATAVMs);
                    ExportDataSet(_ds, path);
                    //   Logger.WriteDownload(text, path);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                    string fileName = Filenmae;
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                catch (Exception ex)
                {
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return View(model);
                }
            }
            else
            {
                model.CRMModelListAll = _db.crms.Where(x => x.campaign_id == model.campaign_id).ToList();
                TempData["Msg"] = "Successfully Downloaded";
                return View(model);
            }

        }
        private void ExportDataSet(DataTable table, string destination)
        {
            using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = workbook.AddWorkbookPart();

                workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                if (table != null && table.Rows.Count > 0)
                {
                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                        headerRow.AppendChild(cell);
                    }


                    sheetData.AppendChild(headerRow);

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (String col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                            newRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(newRow);
                    }

                }
            }
        }

        public IActionResult LeadAssigment()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            CRMVM crmvm_obj = new CRMVM();
            crmvm_obj.userVM = _userModel;

            bool result = leadassignServices.LeadAssigment(crmvm_obj);
            if (result == true)
            {
                TempData["Msg"] = "Successful";
            }
            else
            {
                TempData["Msg"] = "Error";
            }
            return RedirectToAction("CRMBucket", "CRM");
        }

        public IActionResult ClicktoCall(String ContactNo)
        {
            Logger.WriteErrLog("Connect No for Connect :" + ContactNo, "ClicktoCall");
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                Logger.ClicktoCall(ContactNo, _userModel.username, Configuration);
                return Json("true");
            }
            else
            {
                return Json("False");
            }
        }

        [HttpGet]
        public IActionResult LeadReAssignment()
        {
            LeadAssignmentVM leadassignVM = new LeadAssignmentVM();
            leadassignVM.campaignlist = _db.campaigns.ToList();
            leadassignVM.campaignlist.Insert(0, new CampaignModel { campaign_id = 0, campaign_name = "--Select--" });
            ViewData["ButtonName"] = "Get Data";
            return View(leadassignVM);
        }

        [HttpPost]
        public IActionResult LeadReAssignment(LeadAssignmentVM model, string CommandName)
        {

            if (CommandName == "Get Data")
            {
                List<int> _Ids = new List<int>();


                model.LeadAssignmentVMList = leadassignServices.GetLeadByAssignedAgentClass(model);
                _Ids = model.LeadAssignmentVMList.Select(x => x.lead_id).ToList();
                if (_Ids != null && _Ids.Count > 0)
                {
                    model.LeadList = crm_obj.GetAllCrmDetails(_Ids);
                    if (model.LeadList != null)
                    {
                        if (!String.IsNullOrWhiteSpace(model.RowCount))
                            model.LeadList = model.LeadList.Take(Convert.ToInt32(model.RowCount)).ToList();
                        ViewData["ButtonName"] = "ReAssign";
                    }
                    else
                    {
                        TempData["Msg"] = "No Lead found for Re-Assign";
                        ViewData["ButtonName"] = "Get Data";
                    }
                }
                else
                {
                    TempData["Msg"] = "No Lead found for Re-Assign";
                    ViewData["ButtonName"] = "Get Data";
                }
                model.campaignlist = _db.campaigns.ToList();
                model.campaignlist.Insert(0, new CampaignModel { campaign_id = 0, campaign_name = "--Select--" });

            }
            else if (CommandName == "ReAssign")
            {
                bool result = leadassignServices.LeadReassignment(model);
                if (result == true)
                {
                    TempData["Msg"] = "Lead ReAssigned Succesfully";
                    ViewData["ButtonName"] = "Get Data";
                    return RedirectToAction("LeadReAssignment", "CRM");
                }
                else
                {
                    TempData["Msg"] = "Error";
                    ViewData["ButtonName"] = "Get Data";
                    return View(model);
                }
            }
            return View(model);
        }

        public JsonResult GetDispositionData(String DispositionId)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            var Data = dispositionServices.GetAllChildDisposition(Convert.ToInt32(DispositionId));
            return Json(Data);
        }

        public JsonResult GetSecondaryRegionData(int id)
        {

            var Data = crm_obj.GetRegionSecondary(id);
            return Json(Data);
        }

        public JsonResult GetSkipTrace(int id)
        {

            var Data = crm_obj.GetSkipTraceChildModel(id);
            return Json(Data);
        }
        public JsonResult GetListNameByCampaign(int Id)
        {
            CRMVM cRMVM = new CRMVM();
            cRMVM.listModels = crm_obj.GetListModels(Id);
            ViewBag.ButtonName = "Get Assigned";
            return Json(cRMVM.listModels);
        }

        public JsonResult GetReasonforNonPayment(int id)
        {

            var Data = crm_obj.GetReasonForNonPayementChildModel(id);
            return Json(Data);
        }

        public JsonResult GetAgentList(int id)
        {
            var Data = _db.users.Where(x => x.campaign_id == id).ToList();
            return Json(Data);
        }

        [HttpPost]
        public IActionResult SaveActivity(CRMVM model, string CommandName)
        {
            try
            {

                bool result = crm_obj.AddActivityDetails(model);
                if (result == true)
                {
                    TempData["Msg"] = "Successfully Added";
                    return RedirectToAction("Index", "CRM", new { mobile = model.phone1, isautomate = false });
                }
                else
                {
                    TempData["Msg"] = "Failed";
                    return RedirectToAction("Index", "CRM", new { mobile = model.phone1, isautomate = false });
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteErrLog(ex, "CRMServices=>AddEditCrm");
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return RedirectToAction("Index", "CRM", new { mobile = model.phone1, isautomate = false });
            }

        }

        [HttpGet]
        public IActionResult RemarkDownload()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM crmvm = new CRMVM();
                crmvm.campaignlist = campaignservices.GetAllCampaign(_userModel);
                crmvm.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                return View(crmvm);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpPost]
        public IActionResult RemarkDownload(CRMVM model, string CommandName, DateTime FromDate, DateTime TODate)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                //if (CommandName == "Submit")
                //{
                model.campaignlist = campaignservices.GetAllCampaign(_userModel);

                model.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                model.RemarkDownloadList = (from crm in _db.crms
                                            join remark in _db.remarks on crm.crm_id equals remark.lead_id
                                            select new RemarkDownloadVM
                                            {
                                                account_name = crm.account_name,
                                                account_no = crm.account_no,
                                                email = crm.email,
                                                phone1 = crm.phone1,
                                                phone2 = crm.phone2,
                                                phone3 = crm.phone3,
                                                remark = remark.remark,
                                                createdby = remark.createdbyname,
                                                createdon = remark.createdon,
                                                campaign_id = crm.campaign_id,
                                                lead_id = crm.crm_id
                                            }).Where(x => x.campaign_id == model.campaign_id && (x.createdon.Date >= FromDate.Date && x.createdon.Date <= TODate.Date)).ToList();

                return View(model);
                //}
                //else
                //{
                //    model.RemarkDownloadList = (from crm in _db.crms
                //                                join remark in _db.remarks on crm.crm_id equals remark.lead_id
                //                                select new RemarkDownloadVM
                //                                {
                //                                    account_name = crm.account_name,
                //                                    account_no = crm.account_no,
                //                                    email = crm.email,
                //                                    phone1 = crm.phone1,
                //                                    phone2 = crm.phone2,
                //                                    phone3 = crm.phone3,
                //                                    remark = remark.remark,
                //                                    createdby = remark.createdbyname,
                //                                    createdon = remark.createdon,
                //                                    campaign_id = crm.campaign_id,
                //                                    lead_id = crm.crm_id
                //                                }).Where(x => x.campaign_id == model.campaign_id).ToList();

                //    TempData["Msg"] = "Successfully Downloaded";
                //}
                //return RedirectToAction("RemarkDownload", "CRM");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpGet]
        public IActionResult CrmActivity()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM crmvm = new CRMVM();
                crmvm.campaignlist = campaignservices.GetAllCampaign(_userModel);
                crmvm.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                return View(crmvm);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpPost]
        public IActionResult CrmActivity(CRMVM model, string CommandName, DateTime FromDate, DateTime TODate)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                //if (CommandName == "Submit")
                //{
                model.campaignlist = campaignservices.GetAllCampaign(_userModel);

                model.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                model.CrmActivitylst = (from crm in _db.crms
                                        join activity in _db.activityhistories on crm.crm_id equals activity.lead_id
                                        select new CrmActivityVM
                                        {
                                            activity_name = activity.activity_name,
                                            status = activity.status,
                                            email = crm.email,
                                            phone1 = crm.phone1,
                                            phone2 = crm.phone2,
                                            phone3 = crm.phone3,
                                            summary = activity.summary,
                                            time_stamp = activity.time_stamp,
                                            createdon = activity.createdon,
                                            campaign_id = crm.campaign_id,
                                            lead_id = crm.crm_id
                                        }).Where(x => x.campaign_id == model.campaign_id && (x.createdon.Date >= FromDate.Date && x.createdon.Date <= TODate.Date)).ToList();

                return View(model);
                //}
                //else
                //{
                //    model.CrmActivitylst = (from crm in _db.crms
                //                            join activity in _db.activityhistories on crm.crm_id equals activity.lead_id
                //                            select new CrmActivityVM
                //                            {
                //                                activity_name = activity.activity_name,
                //                                status = activity.status,
                //                                email = crm.email,
                //                                phone1 = crm.phone1,
                //                                phone2 = crm.phone2,
                //                                phone3 = crm.phone3,
                //                                summary = activity.summary,
                //                                time_stamp = activity.time_stamp,
                //                                createdon = activity.createdon,
                //                                campaign_id = crm.campaign_id,
                //                                lead_id = crm.crm_id
                //                            }).Where(x => x.campaign_id == model.campaign_id).ToList();

                //    TempData["Msg"] = "Successfully Downloaded";
                //}
                //return RedirectToAction("CrmActivity", "CRM");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpGet]
        public IActionResult ManualAssignment()
        {
            ManualAssignmentVM manualAssignmentVM = new ManualAssignmentVM();
            manualAssignmentVM.campaignModels = _db.campaigns.Where(x => x.isactive == true && x.campaign_isautomated == false).ToList();
            ViewBag.ButtonName = "Get Lead";
            return View(manualAssignmentVM);
        }
        [HttpPost]
        public IActionResult ManualAssignment(ManualAssignmentVM manualAssignmentVM, string Command)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                ViewBag.Campaign_id = manualAssignmentVM.Campaign_id;
                ViewBag.Endorser_id = manualAssignmentVM.Endorser_id;
                ViewBag.Listname = manualAssignmentVM.Listname;

                manualAssignmentVM.campaignModels = _db.campaigns.Where(x => x.isactive == true && x.campaign_isautomated == false).ToList();
                manualAssignmentVM.endorserModels = manualAssignmentService.GetEndorserList(manualAssignmentVM.Campaign_id);
                manualAssignmentVM.listModels = manualAssignmentService.GetListModels(manualAssignmentVM.Endorser_id);

                if (Command == "Get Lead")
                {
                    manualAssignmentVM.LeadList = manualAssignmentService.GetDataForManualAssignment(manualAssignmentVM);
                    var RoleName = _db.roleMasters.FirstOrDefault(x => x.role_name == RoleType.Agent);
                    if (RoleName != null)
                        manualAssignmentVM.agentList = _db.users.Where(x => x.campaign_id == manualAssignmentVM.Campaign_id && x.role_name == RoleName.role_id).ToList();

                    if (manualAssignmentVM.LeadList.Count > 0)
                        ViewBag.ButtonName = "Get Assigned";
                    else
                        ViewBag.ButtonName = "Get Lead";
                }
                else if (Command == "Get Assigned")
                {
                    if (manualAssignmentVM.Agent_id > 0)
                    {
                        foreach (var item in manualAssignmentVM.LeadList)
                        {
                            if (item.is_selected == true)
                            {
                                LeadAssignmentModel leadassign = new LeadAssignmentModel();
                                leadassign.agent_id = manualAssignmentVM.Agent_id;
                                leadassign.lead_id = item.crm_id;
                                leadassign.campaign_id = manualAssignmentVM.Campaign_id;
                                leadassign.isactive = true;
                                leadassign.createdby = _userModel.user_id;
                                leadassign.createdbyname = _userModel.user_fullname;
                                leadassign.createdon = DateTime.Now;
                                _db.leadassigns.Add(leadassign);
                                _db.SaveChanges();
                            }
                        }
                        TempData["Msg"] = "Lead Successfully Assigned";
                        ViewBag.ButtonName = "Get Lead";
                        return RedirectToAction("ManualAssignment", "CRM");
                    }
                    else
                    {
                        ViewBag.ButtonName = "Get Assigned";
                        TempData["Msg"] = "Select Agent to Assign Lead";
                        manualAssignmentVM.LeadList = manualAssignmentService.GetDataForManualAssignment(manualAssignmentVM);
                        manualAssignmentVM.agentList = _db.users.Where(x => x.campaign_id == _userModel.campaign_id).ToList();
                    }
                }
                return View(manualAssignmentVM);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpPost]
        public JsonResult GetEndorserName(int Id)
        {
            ManualAssignmentVM manualAssignmentVM = new ManualAssignmentVM();
            manualAssignmentVM.endorserModels = manualAssignmentService.GetEndorserList(Id);
            ViewBag.ButtonName = "Get Lead";
            return Json(manualAssignmentVM.endorserModels);
        }

        [HttpPost]
        public JsonResult GetListName(int Id)
        {
            ManualAssignmentVM manualAssignmentVM = new ManualAssignmentVM();
            manualAssignmentVM.listModels = manualAssignmentService.GetListModels(Id);
            ViewBag.ButtonName = "Get Assigned";
            return Json(manualAssignmentVM.listModels);
        }
        [HttpGet]
        public IActionResult PaymentFileUpload()
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                CRMVM crmvm = new CRMVM();
                crmvm.campaignlist = campaignservices.GetAllCampaign(_userModel);
                crmvm.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                return View(crmvm);
            }
            else
            {
                return RedirectToAction("PaymentFileUpload", "CRM");
            }
        }

        [HttpPost]
        public IActionResult PaymentFileUpload(CRMVM crmVM, string CommandName)
        {

            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            try
            {
                if (CommandName == "Upload")
                {
                    if (crmVM.campaign_id > 0)
                    {

                        DataTable _ds = UploadExcel();
                        if (_ds != null && _ds.Rows.Count > 0)
                        {
                            StringBuilder BUilder = new StringBuilder();
                            int i = 0;
                            foreach (DataRow item in _ds.Rows)
                            {
                                var _Status = new CRMVM()
                                {
                                    account_no = Convert.ToString(item["Account_No"]),
                                    account_name = Convert.ToString(item["AccountName"]),
                                    invoice_no = Convert.ToString(item["Invoice_No"]),
                                    amount_collected = Convert.ToString(item["Amount_Collected"]),
                                    company_name = Convert.ToString(item["Company_Name"]),
                                    updatedby = _userModel.user_id,
                                    updatedon = DateTime.Now,
                                    updatedbyname = _userModel.user_fullname,
                                    campaign_id = crmVM.campaign_id
                                };
                                var result = crm_obj.EditPaymentField(_Status);
                                if (result)
                                {
                                    i++;
                                }
                                else
                                {
                                    TempData["Msg"] = "Invalid Record";
                                    BUilder.Append(String.Format("{0},", _Status.account_no));
                                }
                            }
                            if (_ds.Rows.Count == i)
                            {
                                TempData["Msg"] = "Successful";
                                return RedirectToAction("PaymentFileUpload", "CRM");
                            }
                            else
                            {
                                TempData["Msg"] = String.Format("Lead with Account No/ is not exists (You Can only update the Existing Record). Account No are {0}", BUilder.ToString());
                                return RedirectToAction("PaymentFileUpload", "CRM");
                            }
                        }
                        else
                        {
                            TempData["Msg"] = "No Record Found in File.";
                            return RedirectToAction("PaymentFileUpload", "CRM");
                        }
                    }
                    else
                    {
                        TempData["msg"] = "Select a Campaign";
                        return RedirectToAction("PaymentFileUpload", "CRM");
                    }
                }
                else if (CommandName == "Get Format")
                {
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Format", "FormatPayment.xlsx");
                    byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                    string fileName = "FormatPayment.xlsx";
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    TempData["Msg"] = "Unknown Action";
                    return RedirectToAction("PaymentFileUpload", "CRM");
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                TempData["Msg"] = "Something went Wrong. Please Share log Files with System Administrator.";
                return RedirectToAction("PaymentFileUpload", "CRM");
            }
        }

        private DataTable PaymentUploadExcel()
        {
            try
            {
                DataTable dataSet = new DataTable();
                if (Request.Form.Files["postedFile"].Length > 0)
                {
                    var filename = Path.GetFileName(Request.Form.Files["postedFile"].FileName);
                    string extension = Path.GetExtension(filename).ToLower();
                    string[] validFileTypes = { ".xls", ".xlsx", ".csv" };
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", filename);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload"));
                    }
                    if (validFileTypes.Contains(extension))
                    {
                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            Request.Form.Files["postedfile"].CopyTo(stream);
                        }
                        #region Read Excel Data and Insert in Temporary Table                      
                        string value = string.Empty;
                        using (SpreadsheetDocument doc = SpreadsheetDocument.Open(path, true))
                        {
                            var sheet = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().First();
                            var worksheetPart = (WorksheetPart)doc.WorkbookPart.GetPartById(sheet.Id.Value);
                            IEnumerable<Row> rows = worksheetPart.Worksheet.GetFirstChild<SheetData>().Elements<Row>();
                            foreach (Row row in rows)
                            {
                                #region get First Row as Header from Excel file
                                if (row.RowIndex.Value == 1)
                                {
                                    foreach (Cell cell in row.Descendants<Cell>())
                                    {
                                        dataSet.Columns.Add(GetCellValue(doc, cell).Trim(), typeof(String));
                                    }
                                }
                                #endregion
                                else
                                {
                                    DataRow dr = dataSet.NewRow();
                                    #region Insert All records in Table
                                    //From second row of excel onwards, add data rows to data table.
                                    IEnumerable<Cell> cells = GetPaymentCellValue(row);
                                    string columnValues = null;
                                    int i = 0;
                                    foreach (Cell currentCell in cells)
                                    {
                                        value = GetPamentValue(doc, currentCell);
                                        if (string.IsNullOrEmpty(value) == true)
                                            value = "";
                                        bool isNumeric = !string.IsNullOrEmpty(value) && value.All(Char.IsDigit);

                                        columnValues = value;
                                        dr[i] = columnValues;
                                        i++;
                                    }
                                    #endregion
                                    dataSet.Rows.Add(dr);
                                }
                            }
                            doc.Close();
                        }
                        #endregion
                        return dataSet;
                    }
                    else
                    {
                        ViewBag.Error = "Please Upload Files in .xls, .xlsx or .csv format";
                        return null;
                    }
                }
                else
                {
                    ViewBag.Error = "Please Choose File and checkbox for uploading Data.";
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteErrLog(ex, "Lead=>LeadUpload");
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }

        private string GetPamentValue(SpreadsheetDocument doc, Cell cell)
        {
            Regex rgx = new Regex(@"^[0-3][0-9]\/[0-1][0-9]\/[0-9]{4} [0-1][0-9]:[0-5][0-9]:[0-5][0-9]$");

            var _Type = false;
            if (cell == null)
                return null;
            if (cell.DataType == null)
            {
                return cell.InnerText;
            }
            string value = cell.InnerText;
            _Type = rgx.IsMatch(cell.InnerText);
            if (_Type == true)
                value = DateTime.Parse(cell.InnerText).ToShortDateString();

            if (cell.DataType != null && cell.DataType.Value == DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString)
            {
                return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
            }
            return value.Trim();
        }

        private static IEnumerable<Cell> GetPaymentCellValue(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetPaymentColumn(cell.CellReference);

                int currentColumnIndex = ConvertPaymentFieldToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)
                {
                    var emptycell = new Cell()
                    {
                        DataType = null,
                        CellValue = new CellValue(string.Empty)
                    };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        public static string GetPaymentColumn(string cellReference)
        {
            // Match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);

            return match.Value;
        }

        public static int ConvertPaymentFieldToNumber(string columnName)
        {
            Regex alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            int convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                int current = i == 0 ? letter - 65 : letter - 64; // ASCII 'A' = 65
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }


        [HttpPost]
        public JsonResult GetListNameTxt(int id)
        {
            DateTime current = new DateTime();
            current = DateTime.Now;
            string endorsement_name = _db.endorsers.FirstOrDefault(x => x.id == id).endorser_name;
            string list_name = String.Format("{0}{1}", endorsement_name, DateTime.Now.ToString("ddMMMyyyyhhmmss"));
            Logger.WriteErrLog(list_name, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            return Json(list_name);
        }

    }
}
