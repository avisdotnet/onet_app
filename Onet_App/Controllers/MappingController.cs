﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Onet_App.Models.Services;

namespace Onet_App.Controllers
{
    [Produces("application/json")]
    [Route("api/Mapping")]
    public class MappingController : Controller
    {

        public IConfiguration Configuration { get; }
        private readonly ApplicationDbContext _db;
        public MappingController(ApplicationDbContext db, IConfiguration configuration)
        {
            _db = db;
            Configuration = configuration;
        }

        [HttpPost]
        public IActionResult UpdateMapping([FromBody]ResponseObj mappingObject)
        {
            Logger.WriteErrLog("API Called", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            if (String.IsNullOrWhiteSpace(mappingObject.userName) || String.IsNullOrWhiteSpace(mappingObject.skillName) || string.IsNullOrWhiteSpace(mappingObject.campName) || string.IsNullOrWhiteSpace(mappingObject.actionFor))
            {
                mappingObject.ResponseCode = 500;
                mappingObject.ResponseMessage = "Parameter Values Missing";
            }
            else
            {
                try
                {
                    var _UserDetails = _db.users.FirstOrDefault(x => x.username.ToLower() == mappingObject.userName.ToLower());
                    if (_UserDetails != null)
                    {
                        var _Campaign = _db.campaigns.FirstOrDefault(c => c.campaign_name.ToLower() == mappingObject.campName.ToLower());
                        if (_Campaign != null)
                        {
                            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                            {
                                try
                                {
                                    var _UserData = _db.users.FirstOrDefault(x => x.username.ToLower() == mappingObject.userName.ToLower());
                                    _UserData.campaign_id = _Campaign.campaign_id;
                                    _db.SaveChanges();

                                    var _GroupUsersAssign = _db.groupsuserassigns.Where(x => x.agent_id == _UserDetails.user_id).ToList();
                                    if (_GroupUsersAssign != null)
                                    {
                                        _db.groupsuserassigns.RemoveRange(_GroupUsersAssign);
                                        _db.SaveChanges();
                                    }
                                    scope.Complete();
                                    scope.Dispose();
                                    //Logger.WriteErrLog(mappingObject.userName + " User Updated", "UpdateMapping");
                                    Logger.WriteErrLog(mappingObject.userName + "User Updated", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                                    mappingObject.ResponseCode = 200;
                                    mappingObject.ResponseMessage = "Mapping Changed Successfully.";
                                }
                                catch (Exception eex)
                                {
                                    //Logger.WriteErrLog(eex, "UpdateMapping");
                                    Logger.WriteErrLog(eex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                                    mappingObject.ResponseCode = 300;
                                    mappingObject.ResponseMessage = "Value not Updated.";
                                }
                            }
                        }
                        else
                        {
                            //Logger.WriteErrLog(mappingObject.campName + " Campaign Not Exist", "UpdateMapping");
                            Logger.WriteErrLog(mappingObject.campName + " Campaign Not Exist", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            mappingObject.ResponseCode = 400;
                            mappingObject.ResponseMessage = "Campaign not Exist.";
                        }
                    }
                    else
                    {
                        Logger.WriteErrLog("User Not Exist", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        mappingObject.ResponseCode = 400;
                        mappingObject.ResponseMessage = "User not Exist.";
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    mappingObject.ResponseCode = 500;
                    mappingObject.ResponseMessage = "Exception Occured. Please check the Values";
                }
            }
            return Json(mappingObject);
        }
    }

    public class ResponseObj
    {
        public string userID { get; set; }
        public string userName { get; set; }
        public string campName { get; set; }
        public string skillName { get; set; }
        public string actionFor { get; set; }
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }

}