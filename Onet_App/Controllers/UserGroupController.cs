﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using Onet_App.Models;
using System.Reflection;

namespace Onet_App.Controllers
{
    public class UserGroupController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IUserGroupServices usergroup_obj => new UserGroupServices(_db);
        private ICampaignServices campaignservices => new CampaignServices(_db);
        private IUser userservices => new UserServices(_db);
        public UserGroupController(ApplicationDbContext db) => _db = db;


        [HttpGet]
        public IActionResult Index(int? id)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                UserGroupVM usergroup_vm = new UserGroupVM
                {
                    UserGroupList = usergroup_obj.GetUserGroup(),
                    campaignlist = campaignservices.GetAllCampaign(_userModel)
                };
                usergroup_vm.user_obj.UserVMList = new List<UserVM>();
                usergroup_vm.campaignlist.Insert(0, new CampaignVM { campaign_id = 0, campaign_name = "--Select--" });
                usergroup_vm._agentlist = (from agent in userservices.GetUsers().Where(x => x.campaign_id == 1)
                                           select new GroupUserAssignModel()
                                           {
                                               agent_id = agent.user_id,
                                               AgentName = agent.username,
                                               is_selected = false

                                           }).ToList();
                if (id != null && id > 0)
                {

                    ViewData["ButtonName"] = "Update";
                    var _data = usergroup_obj.GetUserGroupById(id.Value);
                    if (_data != null)
                    {
                        usergroup_vm.group_id = _data.group_id;
                        usergroup_vm.group_name = _data.group_name;
                        usergroup_vm.campaign_id = _data.campaign_id;
                        usergroup_vm._agentlist = _db.groupsuserassigns.Where(x => x.group_id == usergroup_vm.group_id).ToList();
                        //usergroup_vm.AgentList = userservices.GetUsers().Where(x => x.campaign_id == usergroup_vm.campaign_id).ToList();
                        usergroup_vm.user_obj.UserVMList = userservices.GetUsers().Where(x => x.campaign_id == usergroup_vm.campaign_id).ToList(); usergroup_vm.isactive = _data.isactive;
                    }
                }
                else
                {
                    ViewData["ButtonName"] = "Add";
                }

                return View(usergroup_vm);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpPost]
        public IActionResult Index(UserGroupVM user)
        {
            try
            {
                if (user.campaign_id == 0)
                {
                    TempData["Msg"] = "Campaign was not selected";
                    return RedirectToAction("Index", "UserGroup");
                }
                if (user.group_id > 0)
                {
                    bool _result = usergroup_obj.EditUserGroup(user);
                    if (_result == true)
                    {
                        TempData["Msg"] = "Successfully Updated, Please ReAssign Users using Map User Button";
                    }
                    else
                    {
                        TempData["Msg"] = "Updation Failed";
                    }
                }
                else
                {
                    bool result = usergroup_obj.AddUserGroup(user);
                    if (result == true)
                    {
                        TempData["Msg"] = "Successfully Added, Please Assign Users using Map User Button";
                    }
                    else
                    {
                        TempData["Msg"] = "Failed";
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return RedirectToAction("Index", "UserGroup");
        }
        public IActionResult Delete(int id)
        {
            bool result = usergroup_obj.DeleteUserGroup(id);
            if (result == true)
            {
                TempData["Msg"] = "Successfully Deleted";
            }
            else
            {
                TempData["Msg"] = "Failed";
            }
            return RedirectToAction("Index", "UserGroup");

        }
        [HttpGet]
        public IActionResult AssignUser(int group_id, int campaign_id)
        {
            UserGroupVM model = new UserGroupVM();
            try
            {
                model.group_name = _db.usergroups.Where(x => x.group_id == group_id).Select(x => x.group_name).FirstOrDefault();
                model.campaign_id = campaign_id;
                model.group_id = group_id;
                model.campaign_name = _db.campaigns.Where(x => x.campaign_id == campaign_id).Select(x => x.campaign_name).FirstOrDefault();
                var groupassign_agentids = _db.groupsuserassigns.Select(x => x.agent_id).Distinct().ToList();
                var groupassign_groupids = _db.groupsuserassigns.Where(x => x.group_id == group_id).Select(x => x.agent_id).Distinct().ToList();
                var temprecord = userservices.GetUsers().Where(x => groupassign_groupids.Contains(x.user_id)).ToList();
                foreach (var item in temprecord)
                {
                    item.is_selected = true;
                    model.ExistingAgentList.Add(item);
                }
                //if (groupassign_groupids.Contains(group_id))
                //{
                //    var group_assignsdetails = _db.groupsuserassigns.Where(x => x.group_id == group_id).ToList();
                //    model.ExistingAgentList = userservices.GetUsers().Where(x => group_assignsdetails.Select(a => a.agent_id).Contains(x.user_id)).ToList();
                //    foreach (var item in model.ExistingAgentList)
                //    {
                //        item.is_selected = group_assignsdetails.Where(x => x.agent_id == item.user_id).FirstOrDefault().is_selected;
                //    }
                //}


                model.NewAgentList = userservices.GetUsers().Where(x => x.campaign_id == campaign_id && !groupassign_agentids.Contains(x.user_id)).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }



            //var groupuserassignlist = _db.groupsuserassigns.Select(x=>x.agent_id).ToList();
            //foreach (var item in model.AgentList)
            //{
            //    if (groupuserassignlist.Contains(item.user_id))
            //    {
            //        item.is_selected = _db.groupsuserassigns.FirstOrDefault(x => x.agent_id == item.user_id && x.group_id == group_id).is_selected;
            //    }
            //}
            return View(model);
        }
        [HttpPost]
        public IActionResult AssignUser(UserGroupVM user)
        {
            bool result = usergroup_obj.AddEditGroupUserAssign(user);
            if (result == true)
            {
                TempData["msg"] = "Successfully Mapped";
            }
            else
            {
                TempData["msg"] = "Error";
            }
            return RedirectToAction("Index", "UserGroup");
        }

        [HttpPost]
        public PartialViewResult AgentListPartial(int CampaignId)
        {
            UserGroupVM model = new UserGroupVM();
            model._agentlist = (from agent in userservices.GetUsers().Where(x => x.campaign_id == CampaignId)
                                select new GroupUserAssignModel()
                                {
                                    agent_id = agent.user_id,
                                    AgentName = agent.username,
                                    is_selected = false

                                }).ToList();
            return PartialView("_AgentList", model);
        }
    }
}