﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System.Reflection;

namespace Onet_App.Controllers
{
    public class CampaignController : Controller
    {
        private readonly ApplicationDbContext _db;
        private ICampaignServices campaign_obj => new CampaignServices(_db);
        public CampaignController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            CampaignVM campaignVM_obj = new CampaignVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
               
                campaignVM_obj._campaignslst = campaign_obj.GetAllCampaign(_userModel);
                ViewData["ButtonName"] = "Add";
                if (id != null && id > 0)
                {
                    ViewData["ButtonName"] = "Update";
                    var _data = campaign_obj.GetCampaignById(id.Value);
                    if (_data != null)
                    {
                        campaignVM_obj.campaign_id = _data.campaign_id;
                        campaignVM_obj.campaign_isautomated = _data.campaign_isautomated;
                        campaignVM_obj.list_name = _data.list_name;
                        campaignVM_obj.UniqueValue = _data.UniqueValue;
                        campaignVM_obj.skills_name = _data.skills_name;
                        campaignVM_obj.campaign_name = _data.campaign_name;
                        campaignVM_obj.q_name = _data.q_name;
                        campaignVM_obj.isactive = _data.isactive;

                    }
                    else
                    {
                        ViewData["ButtonName"] = "Add";
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return View(campaignVM_obj);
        }
        [HttpPost]
        public IActionResult Index(CampaignVM model)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                try
                {
                   if(model.campaign_id>0)
                   {
                    //Edit
                    bool _result = campaign_obj.EditCampaign(model);
                    if (_result == true)
                    {
                        TempData["Msg"] = "Successfully Updated";
                    }
                    else
                    {
                        TempData["Msg"] = "Failed";
                    }

                }
                else
                {
                    //Add
                    bool _Result = campaign_obj.AddCampaign(model);
                    if (_Result == true)
                    {
                        TempData["Msg"] = "Successfully Added";
                    }
                    else
                    {
                        TempData["Msg"] = "Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
        }
        else
        {
                return RedirectToAction("Index", "User");
        }
            return RedirectToAction( "Index", "Campaign", new { id = 0});
    }
      
        public IActionResult Delete(int id)
        {
            bool _Result = campaign_obj.DeleteCampaign(id);
            if (_Result == true)
            {
                TempData["Msg"] = "Successfully Deleted";
            }
            else
            {
                TempData["Msg"] = "Failed";
            }
            return RedirectToAction("Index", "Campaign");
        }
    }
}