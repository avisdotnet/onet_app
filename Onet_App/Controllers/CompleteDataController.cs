﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Onet_App.Models.Services;
using Onet_App.Models.ViewModel;
using Onet_App.Models;

namespace Onet_App.Controllers
{
    [Produces("application/json")]
    [Route("api/CompleteData")]
    public class CompleteDataController : Controller
    {
        public IConfiguration Configuration { get; }
        private readonly ApplicationDbContext _db;
        private CompleteDataServices completeDataService;
        APIData apiDate = new APIData();
        private DateTime currentDateTime;
        private DateTime lastAPIDateTime = new DateTime(DateTime.Now.Year, 1, 1);
        public CompleteDataController(ApplicationDbContext db, IConfiguration configuration)
        {
            _db = db;
            completeDataService = new CompleteDataServices(_db);
            Configuration = configuration;
        }
        [HttpGet]
        public string SendData()
        {
            try
            {
                currentDateTime = DateTime.Now;
                CRMVM model = new CRMVM();
                var _lastAPIDateTime = _db.apidata.LastOrDefault();
                if (_lastAPIDateTime != null)
                {
                    lastAPIDateTime = _lastAPIDateTime.lastapitime;
                }
                model.cRMDATAVMs = completeDataService.DownloadCrmDetails(lastAPIDateTime, currentDateTime);
                if (model.cRMDATAVMs.Count == 0)
                {
                    Logger.WriteErrLog("No Data Modified.", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return "No Data Modified.";
                }
                else
                {
                    string Filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls";
                    var _path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Download");
                    System.IO.Directory.CreateDirectory(_path);
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Download", Filename);
                    DataTable _ds = Logger.ToDataTable<CRMDATAVM>(model.cRMDATAVMs);
                    ExportDataSet(_ds, path);

                    if (String.IsNullOrWhiteSpace(Configuration.GetConnectionString("FromEmail")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("ToEmail")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("MessageSubject")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("MessageBody")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("FromEmailPassword")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("SMTP")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("PORT")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("EnabledSSL")) || String.IsNullOrWhiteSpace(Configuration.GetConnectionString("TimeOut")))
                    {
                        Logger.WriteErrLog("Mail Configuration Missing", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        return "Mail Configuration Missing";
                    }
                    else
                    {
                        var Result = SendMail_GoDaddy(_From: Configuration.GetConnectionString("FromEmail"), _Recipient: Configuration.GetConnectionString("ToEmail"), _Subject: Configuration.GetConnectionString("MessageSubject"), _Msg: Configuration.GetConnectionString("MessageBody"), _UserName: Configuration.GetConnectionString("FromEmail"), _Password: Configuration.GetConnectionString("FromEmailPassword"), _HostName: Configuration.GetConnectionString("SMTP"), _Port: Convert.ToInt32(Configuration.GetConnectionString("PORT")), _EnabledSSL: Convert.ToBoolean(Configuration.GetConnectionString("EnabledSSL")), _TimeOut: Convert.ToInt32(Configuration.GetConnectionString("TimeOut")), _path: path);

                        if (Result)
                        {
                            apiDate.lastapitime = currentDateTime;
                            _db.apidata.Add(apiDate);
                            _db.SaveChanges();
                            Logger.WriteErrLog("Mail Sent Successfully.", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return "Mail Sent Successfully.";
                        }
                        else
                        {
                            Logger.WriteErrLog("Mail Sending Unsuccessful.", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return "Mail Sending Unsuccessful.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return "Mail Sending Unsuccessful.";
            }

        }

        public bool SendMail_GoDaddy(string _From, string _Recipient, string _Subject, string _Msg, string _UserName, string _Password, string _HostName, int _Port, bool _EnabledSSL, int _TimeOut, String _path)
        {
            try
            {
                MailMessage mail = new MailMessage(_From, _Recipient);
                mail.Subject = _Subject;
                mail.Body = _Msg;
                mail.Attachments.Add(new System.Net.Mail.Attachment(_path));
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient
                {
                    Host = _HostName,
                    Port = _Port,
                    EnableSsl = _EnabledSSL,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(_UserName, _Password),
                    Timeout = _TimeOut
                };
                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }


        //public bool SendMail(string _EmailID, string _FromMail, string _Subject, string _MsgBody, string _Password, String _path, String _smtp, int _Port)
        //{
        //    try
        //    {
        //        MailMessage mail = new MailMessage(_FromMail, _EmailID);
        //        SmtpClient smtp = new SmtpClient();
        //        NetworkCredential net = new NetworkCredential(_FromMail, _Password);
        //        byte[] fileBytes = System.IO.File.ReadAllBytes(_path);
        //        mail.Subject = _Subject;
        //        mail.Body = _MsgBody;
        //        mail.Attachments.Add(new System.Net.Mail.Attachment(_path));
        //        mail.IsBodyHtml = true;
        //        smtp.Host = _smtp;
        //        smtp.EnableSsl = true;

        //        smtp.UseDefaultCredentials = true;
        //        smtp.Credentials = net;
        //        smtp.Port = _Port;
        //        smtp.Send(mail);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
        //        return false;
        //    }
        //}


        private void ExportDataSet(DataTable table, string destination)
        {
            using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = workbook.AddWorkbookPart();

                workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                if (table != null && table.Rows.Count > 0)
                {
                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    foreach (System.Data.DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                        headerRow.AppendChild(cell);
                    }


                    sheetData.AppendChild(headerRow);

                    foreach (System.Data.DataRow dsrow in table.Rows)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                        foreach (String col in columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                            newRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(newRow);
                    }

                }
            }
        }


    }
}