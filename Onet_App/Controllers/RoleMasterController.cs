﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models;
using Onet_App.Models.ViewModel;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using System.Reflection;

namespace Onet_App.Controllers
{
    public class RoleMasterController : Controller
    {

        private readonly ApplicationDbContext _db;
        private IRoleMaster role_obj => new RoleMasterServices(_db);
        private IMenuMaster menuMasterServices => new MenuMasterService(_db);

        public RoleMasterController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult RoleCreation(int? id)
        {
            RoleMasterVM roleMasterVM = new RoleMasterVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
               
                roleMasterVM.RoleList = role_obj.GetAllRoles();

                roleMasterVM.ParentRoleList = role_obj.GetAllRoles();

                roleMasterVM.MenuCheckList = menuMasterServices.GetAllMenus().MenuMasterVMList;
                roleMasterVM.ParentRoleList.Insert(0, new RoleMasterVM { role_id = 0, role_name = "--Select--" });
                if (id != null && id > 0)
                {
                    roleMasterVM.MenuCheckList = new List<MenuMasterVM>();
                    roleMasterVM.MenuCheckList = menuMasterServices.GetAllMenusByRoleId(id.Value).MenuMasterVMList;
                    ViewData["ButtonName"] = "Update";
                    var _data = role_obj.GetRolesById(id.Value);
                    if (_data != null)
                    {
                        roleMasterVM.role_id = _data.role_id;
                        roleMasterVM.role_name = _data.role_name;
                        roleMasterVM.description = _data.description;
                        roleMasterVM.isactive = _data.isactive;
                    }
                }
                else
                {
                    ViewData["ButtonName"] = "Add";
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return View(roleMasterVM);
        }

        [HttpPost]
        public IActionResult RoleCreation(RoleMasterVM model)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                try
                {
                    if (model.role_id > 0)
                    {
                        bool i = role_obj.EditRole(model);
                        if (i == true)
                        {
                            TempData["Msg"] = "Successfully Updated";
                        }
                    else
                    {
                        TempData["Msg"] = "Updation Failed";
                    }
                }
                else
                {
                    bool i = role_obj.AddRole(model);
                    if (i == true)
                    {
                        TempData["Msg"] = "Successfully Added";
                    }
                    else
                    {
                        TempData["Msg"] = "Failed";
                    }
                }

                }
                catch (Exception ex)
                {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return RedirectToAction("RoleCreation", "RoleMaster", new { id = 0});
        }

        public IActionResult Delete(int id)
        {
            bool i = role_obj.DeleteRole(id);
            if (i == true)
            {
                TempData["Msg"] = "Successfully Deleted";
            }
            else
            {
                TempData["Msg"] = "Failed";
            }
            return RedirectToAction("RoleCreation", "RoleMaster");
        }

        public PartialViewResult MenuCheckBoxPartial()
        {
            return PartialView("_MenuList", new RoleMasterModel());
        }

    }
}