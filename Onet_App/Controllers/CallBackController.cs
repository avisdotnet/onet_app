﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class CallBackController : Controller
    {
        public IActionResult Index()
        {
            CallBackVM call_obj = new CallBackVM();
            return View(call_obj);
        }
    }
}