﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class DispositionController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IDispositionServices disposition_obj => new DispositionServices(_db);
        public DispositionController(ApplicationDbContext db) => _db = db;

        [HttpGet]
        public IActionResult Index(int? id)
        {
            DispositionVM dispositionVM = new DispositionVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
              
                dispositionVM._dispositionlst = disposition_obj.GetAllDisposition();
                dispositionVM._parentDispositionList = disposition_obj.GetAllDisposition();
                dispositionVM._parentDispositionList.Insert(0, new DispositionVM { disposition_id = 0, disposition_name = "--Select--" });

                if (id != null && id > 0)
                {
                   
                    var data = disposition_obj.GetDispositionById(id.Value);
                    dispositionVM.disposition_name = data.disposition_name;
                    dispositionVM.disposition_id = data.disposition_id;
                    dispositionVM.short_name = data.short_name;
                    dispositionVM.parent_disposition = data.parent_disposition;
                    dispositionVM.isactive = data.isactive;
                    dispositionVM.parent_disposition_name = data._dispositionlst.Where(x => x.parent_disposition == dispositionVM.parent_disposition).Select(x => x.disposition_name).FirstOrDefault();
                }
                else
                {
                    ViewData["ButtonName"] = "Add";
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return View(dispositionVM);
        }
        [HttpPost]
        public IActionResult Index(DispositionVM model)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                try
                {
                //if (model.disposition_id > 0)
                //{
                //    //Edit
                //    bool _result = disposition_obj.EditDisposition(model);
                //    if (_result == true)
                //    {
                //        TempData["Msg"] = "Successfully Updated";
                //    }
                //    else
                //    {
                //        TempData["Msg"] = "Failed";
                //    }
                //}
                    //Add
                    bool _Result = disposition_obj.AddDisposition(model);
                    if (_Result == true)
                    {
                        TempData["Msg"] = "Successfully Added";
                    }
                    else
                    {
                        TempData["Msg"] = "Failed";
                    }
               
                }
                catch (Exception ex)
                {
                //Logger.WriteErrLog(ex, "DispositionServices => AddDisposition");
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return RedirectToAction("Index", "Disposition");
        }

        public IActionResult Delete(int id)
        {
            bool result = disposition_obj.DeleteDisposition(id);
            if (result == true)
            {
                TempData["Msg"] = "Successfully Deleted";
            }
            else
            {
                TempData["Msg"] = "Failed";
            }
            return RedirectToAction("Index", "Disposition");
        
        }
    }
}