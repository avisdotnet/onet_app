﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class KeywordController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IKeywordServices key_obj => new KeywordServices(_db);
        public KeywordController(ApplicationDbContext db) => _db = db;
        [HttpGet]
       public IActionResult Index(int? id)
        {
            KeywordsCreationVM key_model = new KeywordsCreationVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
               
                key_model.keywordsList = key_obj.GetAllKeyword();
                if (id != null && id > 0)
                {
                    var data = key_obj.GetKeywordById(id.Value);
                    key_model.key_id = data.key_id;
                    key_model.keyword_name = data.keyword_name;
                    key_model.keyword_description = data.keyword_description;
                    key_model.isactive = data.isactive;
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return View(key_model);
        }
        [HttpPost]
        public IActionResult Index(KeywordsCreationVM model)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
              try
              { 
                 bool _Result = key_obj.savekeyword(model);
                if (_Result == true)
                {
                    TempData["Msg"] = "Successfully Added";
                }
                else
                {
                    TempData["Msg"] = "Failed";
                }

              }

              catch (Exception ex)
              {
             
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
              }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return RedirectToAction("Index", "Keyword");
        }

    }
}
