﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Onet_App.Models.Services;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;

namespace Onet_App.Controllers
{
    public class EmailTemplateController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IEmailTemplateServices email_obj => new EmailTemplateServices(_db);
        IKeywordServices keywordServices => new KeywordServices(_db);
        public EmailTemplateController(ApplicationDbContext db) => _db = db;
        [HttpGet]
        public IActionResult Index(int? id)
        {
            EmailTemplateVM model = new EmailTemplateVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            if (_userModel != null)
            {
                
                model.createdby = _userModel.user_id;
                model.updatedby = _userModel.user_id;
                model.createdbyname = _userModel.user_fullname;
                model.updatedbyname = _userModel.user_fullname;
                var list = keywordServices.GetAllKeyword();
                if (list != null)
                {
                    model.keywordlst = list;
                }
                model._emailtemplatelst = email_obj.GetEmailTemplateList();

                if (id != null && id != 0)
                {
                   var  data = email_obj.GetEmailTemplateById(id.Value);
                    if(data!=null)
                    {
                        
                        model.id = data.id;
                        model.email_template_definition = data.email_template_definition;
                        model.email_template_name = data.email_template_name;
                        model.email_template_description = data.email_template_description;
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(EmailTemplateVM model)
        {
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            model.createdby = _userModel.user_id;
            model.updatedby = _userModel.user_id;
            model.createdbyname = _userModel.user_fullname;
            model.updatedbyname = _userModel.user_fullname;
            if (_userModel != null)
            {
                try
                {
                    if (model.id > 0)
                    {
                        //Edit
                        bool _result = email_obj.EditEmailTemplate(model);
                        if (_result == true)
                        {
                            TempData["Msg"] = "Successfully Updated";
                        }
                        else
                        {
                            TempData["Msg"] = "Failed";
                        }

                    }
                    else
                    {
                        //Add
                        bool _Result = email_obj.AddEmailTemplate(model);
                        if (_Result == true)
                        {
                            TempData["Msg"] = "Successfully Added";
                        }
                        else
                        {
                            TempData["Msg"] = "Failed";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                }
                return RedirectToAction("Index", "EmailTemplate");
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public IActionResult Delete(int id)
        {
            EmailTemplateVM model = new EmailTemplateVM();
            UserVM _userModel = HttpContext.Session.GetObjectFromJson<UserVM>(RoleType.QA);
            model.createdby = _userModel.user_id;
            model.updatedby = _userModel.user_id;
            model.createdbyname = _userModel.user_fullname;
            model.updatedbyname = _userModel.user_fullname;
            if (_userModel != null)
            {
                bool _Result = email_obj.DeleteEmailTemplate(id);
                if (_Result == true)
                {
                    TempData["Msg"] = "Successfully Deleted";
                }
                else
                {
                    TempData["Msg"] = "Failed";
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            return RedirectToAction("Index", "EmailTemplate");
        }
    }    
}