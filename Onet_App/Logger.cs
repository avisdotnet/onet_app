﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Onet_App
{
    public static class Logger
    {
        /// <summary>
        ///  Write Error Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="Function"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        ///
        public static void WriteErrLog(string message, String Function)
        {
            StreamWriter sw = null;
            try
            {
                var path = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Msg_Files");
                System.IO.Directory.CreateDirectory(path);
                sw = new StreamWriter(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Msg_Files", DateTime.Now.ToString("ddMMyyyy")), true);
                sw.WriteLine(DateTime.Now.ToString() + " ==> " + Function + " ==> " + message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static void WriteDownload(string message, string Path)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(Path, false);
                sw.WriteLine(message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        /// <summary>
        ///  Write Error Log by passing message and function
        /// </summary>
        /// <param name="message"></param> 
        /// <param name="Function"></param> 
        /// <returns></returns>
        /// <remarks></remarks>
        public static void WriteErrLog(Exception ex, string FunctionName)
        {
            StreamWriter sw = null;
            try
            {
                var path = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Log_Files");
                System.IO.Directory.CreateDirectory(path);
                sw = new StreamWriter(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Log_Files", DateTime.Now.ToString("ddMMyyyy")), true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + FunctionName + " : " + Convert.ToString(ex.Source).Trim() + "; " + Convert.ToString(ex.Message).Trim() + "; " + Convert.ToString(ex.InnerException));
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        public static bool ClicktoCall(string ContactNumber, string AgentName, IConfiguration configuration)
        {
            try
            {
                //if (!ContactNumber.StartsWith("0") && !string.IsNullOrWhiteSpace(ContactNumber))
                //{
                //    ContactNumber = "0" + ContactNumber;
                //}
                string sURL = Convert.ToString("https://" + (String.IsNullOrWhiteSpace(configuration.GetConnectionString("DialerIP")) == true ? "127.0.0.1" : Convert.ToString(configuration.GetConnectionString("DialerIP"))) + ":8475/CrmDial?phoneNumber=" + (ContactNumber == null ? "" : ContactNumber) + "&exeUserName=" + (AgentName == null ? "" : AgentName) + "@" + Convert.ToString(configuration.GetConnectionString("Domainname")));
                WriteErrLog(sURL, "ClicktoCall");
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(sURL);
                WebProxy myProxy = new WebProxy("myproxy", 80);
                myProxy.BypassProxyOnLocal = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (Object obj, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors) { return (true); };
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);
                WriteErrLog("Call Connected :" + ContactNumber, "ClicktoCall");
                return true;
            }
            catch (Exception ex)
            {
                WriteErrLog(ex, "ClicktoCall");
                return false;
            }
        }

        public static bool UploadLeadtoDialer(string ContactNumber, string campname, string skillname, string qname, string listname, IConfiguration configuration, string phone2 = "", string phone3 = "", string phone4 = "")
        {
            try
            {
                string sURL = String.Format("http://{0}:{1}/test.ajax?do=manualUpload&domainname={2}&username={3}&password={4}&campname={5}&skillname={6}&qname={7}&listname={8}&{9}={10}&{11}={12}&{13}={14}&{15}={16}&status=NEW", Convert.ToString(configuration.GetConnectionString("DialerIP")), Convert.ToString(configuration.GetConnectionString("DomainPort")), Convert.ToString(configuration.GetConnectionString("Domainname")), Convert.ToString(configuration.GetConnectionString("DomainUsername")), Convert.ToString(configuration.GetConnectionString("DomainUserPassword")), campname, skillname.ToUpper(), qname, listname, Convert.ToString(configuration.GetConnectionString("uniquecolumnname")), ContactNumber, Convert.ToString(configuration.GetConnectionString("uniquecolumnname2")), phone2, Convert.ToString(configuration.GetConnectionString("uniquecolumnname3")), phone3, Convert.ToString(configuration.GetConnectionString("uniquecolumnname4")), phone4);

                WriteErrLog(sURL, "UploadLeadtoDialer");
                WebRequest wrGETURL;
                wrGETURL = WebRequest.Create(sURL);
                WebProxy myProxy = new WebProxy("myproxy", 80);
                myProxy.BypassProxyOnLocal = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (Object obj, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors) { return (true); };
                Stream objStream;
                objStream = wrGETURL.GetResponse().GetResponseStream();
                StreamReader objReader = new StreamReader(objStream);

                WriteErrLog("Upload Lead To Dialer :" + objReader.ReadToEnd(), "UploadLeadtoDialer");
                return true;
            }
            catch (Exception ex)
            {
                WriteErrLog(ex, "UploadLeadtoDialer");
                return false;
            }
        }

        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
    public class MyConfiguration
    {
        public String DialerIP { get; set; }
    }

}
