﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class RoleMasterModel : CommonAttributeModel
    {
        [Key]
        public int role_id { get; set; }

        [Required(ErrorMessage = "Role Name is Required")]
        [Display(Name = "Role name")]
        
        public string role_name { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        [Display(Name = "Description")]
        public string description { get; set; }
        
    }


}
