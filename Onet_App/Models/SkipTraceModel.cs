﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class SkipTraceModel:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string skip_trace_name { get; set; }
    }
}
