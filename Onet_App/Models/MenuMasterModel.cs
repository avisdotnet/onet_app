﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Onet_App.Models
{
    public class MenuMasterModel : CommonAttributeModel
    {
        [Key]
        public int menu_id { get; set;}
       
        [Display(Name = "Menu Name")]
        public string menu_name { get; set;}
       
        [Display(Name = "Area Name")]
        public string area_name { get; set; }

        [Display(Name = "Controller Name")]
        public string controller_name { get; set;}
        
        [Display(Name = "Action Name")]
        public string action_name { get; set;}
       
        [Display(Name = "Query String")]
        public string query_string { get; set; }

    }
}
