﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class ListModel:CommonAttributeModel
    {
        public int id { get; set; }
        public string listname { get; set; }
    }
}
