﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class EndorserModel : CommonAttributeModel
    {
        public int id { get; set; }
        public String endorser_name { get; set; }
        public int campaign_id { get; set; }
    }
}
