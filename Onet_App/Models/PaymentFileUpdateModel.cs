﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class PaymentFileUpdateModel:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string account_no { get; set; }
        public string invoice_no { get; set; }
    }
}
