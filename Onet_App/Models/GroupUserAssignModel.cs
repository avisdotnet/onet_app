﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class GroupUserAssignModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("group_id")]
        public int group_id { get; set; }
        public int agent_id { get; set; }
        public bool is_selected { get; set; }
        public String  AgentName { get; set; }
    }
}
