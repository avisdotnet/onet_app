﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // TODO: This is messy, but needed for migrations.
            optionsBuilder.EnableSensitiveDataLogging(true);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CRMModel>().Property(a => a.phone1).HasDefaultValueSql("0");
            modelBuilder.Entity<CRMModel>().Property(a => a.phone2).HasDefaultValueSql("0");
            modelBuilder.Entity<CRMModel>().Property(a => a.phone3).HasDefaultValueSql("0");
            modelBuilder.Entity<CRMModel>().Property(a => a.phone4).HasDefaultValueSql("0");
            modelBuilder.Entity<CRMModel>().Property(a => a.give_new_contact_no).HasDefaultValueSql("0");
            modelBuilder.Entity<APIData>().Property(b => b.lastapitime).HasDefaultValueSql("now()");
            modelBuilder.Entity<ActivityHistoryModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<BalanceForCollectionModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<CallBackModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<CampaignModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<CityModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<CRMModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<DispositionModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<GroupUserAssignModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<LeadAssignmentModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<MenuMasterModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ModeOfSettlementModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<PaymentChannelModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ReasonForNonPayementChildModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ReasonForNonPaymentModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RegionModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RegionSecondaryModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RemarksModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RoleMasterModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<RoleMenuDetailsModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<SkipTraceChildModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<SkipTraceModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<StatusModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<TemplateModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<UserGroupModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<UserGroupAssignModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<UserModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EndorserModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<ListModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<KeywordsCreationModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<SMSTemplateModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EmailTemplateCreationModel>().Property(b => b.createdon).HasDefaultValueSql("now()");
            modelBuilder.Entity<EmailTemplateCreationModel>().Property(b => b.isactive).HasDefaultValueSql("true");
        }



        public DbSet<ActivityHistoryModel> activityhistories { get; set; }
        public DbSet<BalanceForCollectionModel> balanceForCollections { get; set; }
        public DbSet<CallBackModel> callBacks { get; set; }
        public DbSet<CampaignModel> campaigns { get; set; }
        public DbSet<CityModel> cities { get; set; }
        public DbSet<CRMModel> crms { get; set; }
        public DbSet<DispositionModel> dispositions { get; set; }
        public DbSet<GroupUserAssignModel> groupsuserassigns { get; set; }
        public DbSet<LeadAssignmentModel> leadassigns { get; set; }
        public DbSet<MenuMasterModel> menumaster { get; set; }
        public DbSet<ModeOfSettlementModel> modeofsettlements { get; set; }
        public DbSet<PaymentChannelModel> paymentchannels { get; set; }
        public DbSet<ReasonForNonPayementChildModel> reasonfornonpaymentchilds { get; set; }
        public DbSet<ReasonForNonPaymentModel> reasonfornonpayments { get; set; }
        public DbSet<RegionModel> regionprimarys { get; set; }
        public DbSet<RegionSecondaryModel> regionsecondarys { get; set; }
        public DbSet<RemarksModel> remarks { get; set; }
        public DbSet<RoleMasterModel> roleMasters { get; set; }
        public DbSet<RoleMenuDetailsModel> rolemenudetails { get; set; }
        public DbSet<SkipTraceChildModel> skiptracechilds { get; set; }
        public DbSet<SkipTraceModel> skipstrace { get; set; }
        public DbSet<StatusModel> statuses { get; set; }
        public DbSet<TemplateModel> templates { get; set; }
        public DbSet<UserGroupModel> usergroups { get; set; }
        public DbSet<UserGroupAssignModel> usergroupassigns { get; set; }
        public DbSet<UserModel> users { get; set; }
        public DbSet<EndorserModel> endorsers { get; set; }
        public DbSet<KeywordsCreationModel> keywords_Creations { get; set; }
        public DbSet<SMSTemplateModel> smstemplates { get; set; }
        public DbSet<EmailTemplateCreationModel> email_template { get; set; }
        public DbSet<APIData> apidata { get; set; }

    }

    public class RoleType
    {
        public const string Admin = "Admin";
        public const string QA = "QA";
        public const string Supervisor = "Supervisor";
        public const string Agent = "Agent";
        public const string Session = "Session";
    }
}
