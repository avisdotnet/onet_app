﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace Onet_App.Models.Services
{
    public class ManualAssignmentService : IManualAssignmentService
    {
        private readonly ApplicationDbContext _db;
        public ManualAssignmentService(ApplicationDbContext db) => _db = db;

        public List<CRMVM> GetDataForManualAssignment(ManualAssignmentVM manualAssignmentVM)
        {
            List<CRMVM> cRMModels = new List<CRMVM>();
            List<CRMModel> lstAssigned = new List<CRMModel>();
            try
            {
                // TODO : Write the Code for Get Unassigned Lead from Campaign, endorser and List name
                lstAssigned = (from crm in _db.crms
                               join assign in _db.leadassigns on crm.crm_id equals assign.lead_id
                               select new CRMModel()
                               {
                                   account_name = crm.account_name,
                                   account_no = crm.account_no,
                                   address = crm.address,
                                   agent_id = crm.agent_id,
                                   age_group = crm.age_group,
                                   amount_collected = crm.amount_collected,
                                   authorized_representatives = crm.authorized_representatives,
                                   balance_for_collection = crm.balance_for_collection,
                                   callout_status_level1 = crm.callout_status_level1,
                                   callout_status_level2 = crm.callout_status_level2,
                                   callout_status_level3 = crm.callout_status_level3,
                                   callout_status_level4 = crm.callout_status_level4,
                                   callout_status_level5 = crm.callout_status_level5,
                                   campaign_id = crm.campaign_id,
                                   city = crm.city,
                                   company_name = crm.company_name,
                                   count_of_attempted_contact_numbers = crm.count_of_attempted_contact_numbers,
                                   createdby = crm.createdby,
                                   createdbyname = crm.createdbyname,
                                   createdon = crm.createdon,
                                   crm_id = crm.crm_id,
                                   date_invoice = crm.date_invoice.HasValue == true ? crm.date_invoice : (DateTime?)null,
                                   email = crm.email,
                                   enable_get_lead = crm.enable_get_lead,
                                   endorsement_id = crm.endorsement_id,
                                   financial_contact_person = crm.financial_contact_person,
                                   give_new_contact_no = crm.give_new_contact_no,
                                   invoice_amount = crm.invoice_amount,
                                   invoice_no = crm.invoice_no,
                                   isactive = crm.isactive,
                                   LeadAssignmentDetails = crm.LeadAssignmentDetails,
                                   list_name = crm.list_name,
                                   mode_of_settlement = crm.mode_of_settlement,
                                   number_of_accounts = crm.number_of_accounts,
                                   outstanding_balance = crm.outstanding_balance,
                                   overdue_balance = crm.overdue_balance,
                                   payment_channel = crm.payment_channel,
                                   phone1 = crm.phone1,
                                   phone2 = crm.phone2,
                                   phone3 = crm.phone3,
                                   phone4 = crm.phone4,
                                   phone_extension = crm.phone_extension,
                                   ptp_amount = crm.ptp_amount,
                                   ptp_date = crm.ptp_date.HasValue == true ? crm.ptp_date : (DateTime?)null,
                                   ptp_dated_by = crm.ptp_dated_by,
                                   reason_for_non_payment_level1 = crm.reason_for_non_payment_level1,
                                   reason_for_non_payment_level2 = crm.reason_for_non_payment_level2,
                                   recommendation = crm.recommendation,
                                   region_primary = crm.region_primary,
                                   region_secondary = crm.region_secondary,
                                   remarks = crm.remarks,
                                   service_id_no = crm.service_id_no,
                                   service_type = crm.service_type,
                                   skip_trace = crm.skip_trace,
                                   skip_trace_child = crm.skip_trace_child,
                                   status = crm.status,
                                   supervisor_id = crm.supervisor_id,
                                   updatedby = crm.updatedby,
                                   updatedbyname = crm.updatedbyname,
                                   updatedon = crm.updatedon
                               }).ToList();
                List<int> _Ids = new List<int>();
                _Ids = lstAssigned.Select(x => x.crm_id).ToList();
                if (lstAssigned.Count != 0 && lstAssigned != null)
                {
                    cRMModels = (from crm in _db.crms.Where(x => x.campaign_id == manualAssignmentVM.Campaign_id && x.endorsement_id == manualAssignmentVM.Endorser_id && x.list_name == manualAssignmentVM.Listname && !_Ids.Contains(x.crm_id))
                                 select new CRMVM()
                                 {
                                     account_name = crm.account_name,
                                     account_no = crm.account_no,
                                     address = crm.address,
                                     agent_id = crm.agent_id,
                                     age_group = crm.age_group,
                                     amount_collected = crm.amount_collected,
                                     authorized_representatives = crm.authorized_representatives,
                                     balance_for_collection = crm.balance_for_collection,
                                     callout_status_level1 = crm.callout_status_level1,
                                     callout_status_level2 = crm.callout_status_level2,
                                     callout_status_level3 = crm.callout_status_level3,
                                     callout_status_level4 = crm.callout_status_level4,
                                     callout_status_level5 = crm.callout_status_level5,
                                     campaign_id = crm.campaign_id,
                                     city = crm.city,
                                     company_name = crm.company_name,
                                     count_of_attempted_contact_numbers = crm.count_of_attempted_contact_numbers,
                                     createdby = crm.createdby,
                                     createdbyname = crm.createdbyname,
                                     createdon = crm.createdon,
                                     crm_id = crm.crm_id,
                                     date_invoice = crm.date_invoice.HasValue == true ? crm.date_invoice : (DateTime?)null,
                                     email = crm.email,
                                     enable_get_lead = crm.enable_get_lead,
                                     endorsement_id = crm.endorsement_id,
                                     financial_contact_person = crm.financial_contact_person,
                                     give_new_contact_no = crm.give_new_contact_no,
                                     invoice_amount = crm.invoice_amount,
                                     invoice_no = crm.invoice_no,
                                     isactive = crm.isactive,
                                     LeadAssignmentDetails = crm.LeadAssignmentDetails,
                                     list_name = crm.list_name,
                                     mode_of_settlement = crm.mode_of_settlement,
                                     number_of_accounts = crm.number_of_accounts,
                                     outstanding_balance = crm.outstanding_balance,
                                     overdue_balance = crm.overdue_balance,
                                     payment_channel = crm.payment_channel,
                                     phone1 = crm.phone1,
                                     phone2 = crm.phone2,
                                     phone3 = crm.phone3,
                                     phone4 = crm.phone4,
                                     phone_extension = crm.phone_extension,
                                     ptp_amount = crm.ptp_amount,
                                     ptp_date = crm.ptp_date.HasValue == true ? crm.ptp_date : (DateTime?)null,
                                     ptp_dated_by = crm.ptp_dated_by,
                                     reason_for_non_payment_level1 = crm.reason_for_non_payment_level1,
                                     reason_for_non_payment_level2 = crm.reason_for_non_payment_level2,
                                     recommendation = crm.recommendation,
                                     region_primary = crm.region_primary,
                                     region_secondary = crm.region_secondary,
                                     remarks = crm.remarks,
                                     service_id_no = crm.service_id_no,
                                     service_type = crm.service_type,
                                     skip_trace = crm.skip_trace,
                                     skip_trace_child = crm.skip_trace_child,
                                     status = crm.status,
                                     supervisor_id = crm.supervisor_id,
                                     updatedby = crm.updatedby,
                                     updatedbyname = crm.updatedbyname,
                                     updatedon = crm.updatedon
                                 }).ToList();
                }
                else
                {
                    cRMModels = (from crm in _db.crms.Where(x => x.campaign_id == manualAssignmentVM.Campaign_id && x.endorsement_id == manualAssignmentVM.Endorser_id && x.list_name == manualAssignmentVM.Listname)
                                 select new CRMVM()
                                 {
                                     account_name = crm.account_name,
                                     account_no = crm.account_no,
                                     address = crm.address,
                                     agent_id = crm.agent_id,
                                     age_group = crm.age_group,
                                     amount_collected = crm.amount_collected,
                                     authorized_representatives = crm.authorized_representatives,
                                     balance_for_collection = crm.balance_for_collection,
                                     callout_status_level1 = crm.callout_status_level1,
                                     callout_status_level2 = crm.callout_status_level2,
                                     callout_status_level3 = crm.callout_status_level3,
                                     callout_status_level4 = crm.callout_status_level4,
                                     callout_status_level5 = crm.callout_status_level5,
                                     campaign_id = crm.campaign_id,
                                     city = crm.city,
                                     company_name = crm.company_name,
                                     count_of_attempted_contact_numbers = crm.count_of_attempted_contact_numbers,
                                     createdby = crm.createdby,
                                     createdbyname = crm.createdbyname,
                                     createdon = crm.createdon,
                                     crm_id = crm.crm_id,
                                     date_invoice = crm.date_invoice.HasValue == true ? crm.date_invoice : (DateTime?)null,
                                     email = crm.email,
                                     enable_get_lead = crm.enable_get_lead,
                                     endorsement_id = crm.endorsement_id,
                                     financial_contact_person = crm.financial_contact_person,
                                     give_new_contact_no = crm.give_new_contact_no,
                                     invoice_amount = crm.invoice_amount,
                                     invoice_no = crm.invoice_no,
                                     isactive = crm.isactive,
                                     LeadAssignmentDetails = crm.LeadAssignmentDetails,
                                     list_name = crm.list_name,
                                     mode_of_settlement = crm.mode_of_settlement,
                                     number_of_accounts = crm.number_of_accounts,
                                     outstanding_balance = crm.outstanding_balance,
                                     overdue_balance = crm.overdue_balance,
                                     payment_channel = crm.payment_channel,
                                     phone1 = crm.phone1,
                                     phone2 = crm.phone2,
                                     phone3 = crm.phone3,
                                     phone4 = crm.phone4,
                                     phone_extension = crm.phone_extension,
                                     ptp_amount = crm.ptp_amount,
                                     ptp_date = crm.ptp_date.HasValue == true ? crm.ptp_date : (DateTime?)null,
                                     ptp_dated_by = crm.ptp_dated_by,
                                     reason_for_non_payment_level1 = crm.reason_for_non_payment_level1,
                                     reason_for_non_payment_level2 = crm.reason_for_non_payment_level2,
                                     recommendation = crm.recommendation,
                                     region_primary = crm.region_primary,
                                     region_secondary = crm.region_secondary,
                                     remarks = crm.remarks,
                                     service_id_no = crm.service_id_no,
                                     service_type = crm.service_type,
                                     skip_trace = crm.skip_trace,
                                     skip_trace_child = crm.skip_trace_child,
                                     status = crm.status,
                                     supervisor_id = crm.supervisor_id,
                                     updatedby = crm.updatedby,
                                     updatedbyname = crm.updatedbyname,
                                     updatedon = crm.updatedon
                                 }).ToList();
                }
                foreach (var item in cRMModels)
                {
                    if (item.callout_status_level1 > 0)
                    {
                        var _data = _db.dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1);
                        if (_data != null)
                        {
                            item.callout_status_level1_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level2 > 0)
                    {
                        var _data = _db.dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level2);
                        if (_data != null)
                        {
                            item.callout_status_level2_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level3 > 0)
                    {
                        var _data = _db.dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level3);
                        if (_data != null)
                        {
                            item.callout_status_level3_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level4 > 0)
                    {
                        var _data = _db.dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level4);
                        if (_data != null)
                        {
                            item.callout_status_level4_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level5 > 0)
                    {
                        var _data = _db.dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level5);
                        if (_data != null)
                        {
                            item.callout_status_level5_Text = _data.disposition_name;
                        }
                    }
                }
                if (!String.IsNullOrEmpty(manualAssignmentVM.RowCount))
                {
                    cRMModels = cRMModels.Take(Convert.ToInt32(manualAssignmentVM.RowCount)).ToList();
                }

                return cRMModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return cRMModels;
            }
        }

        public List<EndorserModel> GetEndorserList(int CampaignId)
        {
            List<EndorserModel> lstendorser = new List<EndorserModel>();
            try
            {
                lstendorser = _db.endorsers.Where(x => x.isactive == true && x.campaign_id == CampaignId).ToList();
                return lstendorser;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return lstendorser;
            }
        }

        public List<ListModel> GetListModels(int EndorserId)
        {
            List<ListModel> lstendorser = new List<ListModel>();
            try
            {
                lstendorser = _db.crms.Where(x => x.endorsement_id == EndorserId).Select(x => new ListModel() { listname = x.list_name }).Distinct().ToList();
                return lstendorser;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return lstendorser;
            }
        }

        public bool ManualAssign()
        {
            throw new NotImplementedException();
        }
    }
}
