﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class KeywordServices : IKeywordServices
    {
        private readonly ApplicationDbContext _db;

        public KeywordServices(ApplicationDbContext db) => _db = db;
        /// <summary>
        /// Get AllKeyword
        /// </summary>
        /// <returns></returns>
        public List<KeywordsCreationVM> GetAllKeyword()
        {
            KeywordsCreationVM keyVM = new KeywordsCreationVM();
            try
            {
                var data = _db.keywords_Creations.Where(x => x.isactive == true)
                .Select(obj => new KeywordsCreationVM
                {
                    key_id = obj.key_id,
                    keyword_name = obj.keyword_name,
                    keyword_description = obj.keyword_description,
                    createdby = obj.createdby,
                    createdbyname = obj.createdbyname,
                    createdon = obj.createdon,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    isactive = obj.isactive,
                    updatedon = DateTime.Now

                }).ToList();
                if (data != null)
                {
                    keyVM.keywordsList = data;
                    return keyVM.keywordsList;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get KeywordById
        /// </summary>
        /// <param name="CampaignID"></param>
        /// <returns></returns>

        public KeywordsCreationVM GetKeywordById(int KeyID)
        {
            KeywordsCreationVM key_obj = new KeywordsCreationVM();
            try
            {
                var Data = _db.keywords_Creations.FirstOrDefault(x => x.key_id == KeyID);
                if (Data != null)
                {
                    key_obj.key_id = Data.key_id;
                    key_obj.keyword_name = Data.keyword_name;
                    key_obj.keyword_description = Data.keyword_description;
                    key_obj.isactive = Data.isactive;
                    key_obj.createdby = Data.createdby;
                    key_obj.createdbyname = Data.createdbyname;
                    key_obj.createdon = Data.createdon;
                    key_obj.updatedby = Data.updatedby;
                    key_obj.updatedbyname = Data.updatedbyname;
                    key_obj.updatedon = DateTime.Now;
                }
                return key_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// save keyword
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public bool savekeyword(KeywordsCreationVM keywords)
        {
            try
            {
                bool bool_sameNameExists = _db.keywords_Creations.Any(x => x.keyword_name.ToLower().Trim() == keywords.keyword_name.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            KeywordsCreationModel keyword_obj = new KeywordsCreationModel();
                            {

                                keyword_obj.key_id = keywords.key_id;
                                keyword_obj.keyword_name = keywords.keyword_name;
                                keyword_obj.keyword_description = keywords.keyword_description;
                                keyword_obj.isactive = true;
                                keyword_obj.createdby = keywords.createdby;
                                keyword_obj.createdbyname = keywords.createdbyname;
                                keyword_obj.createdon = DateTime.Now;
                                keyword_obj.updatedby = keywords.updatedby;
                                keyword_obj.updatedbyname = keywords.updatedbyname;
                                keyword_obj.updatedon = DateTime.Now;
                            };

                            _db.keywords_Creations.Add(keyword_obj);
                            _db.SaveChanges();
                            return true;
                        }

                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }


    }
}




