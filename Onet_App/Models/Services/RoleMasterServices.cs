﻿using Onet_App.Models.Services.IServices;
using System;
using System.Collections.Generic;
using Onet_App.Models.ViewModel;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Reflection;

namespace Onet_App.Models.Services
{
    public class RoleMasterServices : IRoleMaster
    {
        private ApplicationDbContext _db;

        public RoleMasterServices(ApplicationDbContext db)
        {
            _db = db;
        }

        List<RoleMasterModel> IRoleMaster.MenuCheckList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public List<RoleMasterVM> GetAllRoles()
        {
            RoleMasterVM roleMasterVM = new RoleMasterVM();
            try
            {
               roleMasterVM.RoleList= _db.roleMasters.Where(x => x.isactive == true)
                    .Select(obj => new RoleMasterVM
                    {
                        role_id = obj.role_id,
                        role_name = obj.role_name,
                        description = obj.description,
                    }).ToList();
                return roleMasterVM.RoleList;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get Roles By Id
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public RoleMasterVM GetRolesById(int RoleID)
        {
            RoleMasterVM role_obj = new RoleMasterVM();
            try
            {
                var Data = _db.roleMasters.FirstOrDefault(x => x.role_id == RoleID);
                if (Data != null)
                {
                    role_obj.role_name = Data.role_name;
                    role_obj.description = Data.description;
                    role_obj.isactive = Data.isactive;
                    role_obj.role_id = Data.role_id;

                };
                return role_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Add Role
        /// </summary>
        /// <param name="roleMasterVM"></param>
        /// <returns></returns>
        public bool AddRole(RoleMasterVM roleMasterVM)
        {
            try
            {
                bool bool_sameNameExists = _db.roleMasters.Any(x => x.role_name.ToLower().Trim() == roleMasterVM.role_name.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            RoleMasterModel role_obj = new RoleMasterModel();
                            {
                                role_obj.role_name = roleMasterVM.role_name;
                                role_obj.description = roleMasterVM.description;
                                role_obj.isactive = true;
                                role_obj.createdon = DateTime.Now;
                                role_obj.createdby = roleMasterVM.createdby;
                                role_obj.updatedon = DateTime.Now;
                                role_obj.updatedby = roleMasterVM.updatedby;
                                role_obj.createdbyname = roleMasterVM.createdbyname;
                                role_obj.updatedbyname = roleMasterVM.updatedbyname;

                            };
                            _db.roleMasters.Add(role_obj);
                            _db.SaveChanges();
                            if (role_obj.role_id > 0)
                            {
                                foreach (var item in roleMasterVM.MenuCheckList)
                                {
                                    RoleMenuDetailsModel roleMenuDetailsModel = new RoleMenuDetailsModel
                                    {
                                        role_id = role_obj.role_id,
                                        is_selected = item.is_selected,
                                        menu_id = item.menu_id
                                    };
                                    _db.rolemenudetails.Add(roleMenuDetailsModel);
                                    _db.SaveChanges();
                                    scope.Complete();
                                    scope.Dispose();
                                }
                            }
                            return true;
                        }
                        catch (Exception ex)
                        {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
                        }
                    }
                }
            }
           
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
        /// <summary>
        /// Edit Role
        /// </summary>
        /// <param name="roleMasterVM"></param>
        /// <returns></returns>
        public bool EditRole(RoleMasterVM roleMasterVM)
        {
            try
            {
                bool bool_sameNameExists = _db.roleMasters.Any(x => x.role_name.ToLower().Trim() == roleMasterVM.role_name.ToLower().Trim() && x.role_id != roleMasterVM.role_id);
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    RoleMasterVM role_obj = new RoleMasterVM();
                    RoleMenuDetailsModel roleMenuDetail;
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            var Data = _db.roleMasters.FirstOrDefault(x => x.role_id == roleMasterVM.role_id);
                            if (Data != null)
                            {
                                Data.role_id = roleMasterVM.role_id;
                                Data.role_name = roleMasterVM.role_name;
                                Data.description = roleMasterVM.description;

                                Data.createdon = roleMasterVM.createdon;
                                Data.createdby = roleMasterVM.createdby;
                                Data.updatedon = DateTime.Now;
                                Data.updatedby = roleMasterVM.updatedby;
                                Data.createdbyname = roleMasterVM.createdbyname;
                                Data.updatedbyname = roleMasterVM.updatedbyname;
                                Data.isactive = true;
                                _db.SaveChanges();
                                if (Data.role_id > 0)
                                {
                                    foreach (var item in roleMasterVM.MenuCheckList)
                                    {
                                        var MenuPage = _db.rolemenudetails.FirstOrDefault(x => x.role_id == Data.role_id && x.menu_id == item.menu_id);
                                        if (MenuPage != null)
                                        {
                                            MenuPage.is_selected = item.is_selected;
                                            _db.SaveChanges();
                                        }
                                        else
                                        {
                                            roleMenuDetail = new RoleMenuDetailsModel
                                            {
                                                Id = item.menu_id,
                                                role_id = Data.role_id,
                                                is_selected = item.is_selected
                                            };
                                            _db.rolemenudetails.Add(roleMenuDetail);
                                            _db.SaveChanges();
                                        }
                                    }
                                }
                                scope.Complete();
                                scope.Dispose();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteRole(int id)
        {
            RoleMasterVM role = new RoleMasterVM();
            try
            {
                var Data = _db.roleMasters.FirstOrDefault(x => x.role_id == id);
                Data.isactive = false;
                Data.createdon = role.createdon;
                Data.createdby = role.createdby;
                Data.updatedon = DateTime.Now;
                Data.updatedby = role.updatedby;
                Data.createdbyname = role.createdbyname;
                Data.updatedbyname = role.updatedbyname;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                return false;
            }
        }

    }
}
