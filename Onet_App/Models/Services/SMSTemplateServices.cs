﻿using Onet_App.Models.ViewModel;
using Onet_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Transactions;
using Onet_App.Models.Services.IServices;

namespace Onet_App.Models.Services
{
    public class SMSTemplateServices : ISMSTemplateServices
    {
        private readonly ApplicationDbContext _db;
        public SMSTemplateServices(ApplicationDbContext db) => _db = db; 

        public List<SMSTemplateVM> GetSMSTemplateList ()
        {
            SMSTemplateVM sms = new SMSTemplateVM();
            sms.SMSTemplateVMList = _db.smstemplates.Where(x => x.isactive == true).Select(
                x => new SMSTemplateVM
                {
                    id = x.id,
                    sms_template_name = x.sms_template_name,
                    sms_template_definition = x.sms_template_definition,
                    sms_template_description = x.sms_template_description,
                    isactive = x.isactive,
                    createdby = x.createdby,
                    createdbyname = x.createdbyname,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedbyname = x.updatedbyname,
                    updatedon = x.updatedon
                }).ToList();
            return sms.SMSTemplateVMList;
        }

        public SMSTemplateVM GetSMSTemplateById(int id)
        {
            SMSTemplateVM sms = new SMSTemplateVM();
            sms = _db.smstemplates.Select(
                x => new SMSTemplateVM
                {
                    id = x.id,
                    sms_template_name = x.sms_template_name,
                    sms_template_definition = x.sms_template_definition,
                    sms_template_description = x.sms_template_description,
                    isactive = x.isactive,
                    createdby = x.createdby,
                    createdbyname = x.createdbyname,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedbyname = x.updatedbyname,
                    updatedon = x.updatedon
                }).FirstOrDefault(x=>x.id == id);
            return sms;
        }

        public string SaveUpdateSMSTemplate(SMSTemplateVM sms)
        {
            string result = string.Empty;
           
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                try
                {
                    bool same_template = _db.smstemplates.Any(x => x.sms_template_definition.ToLower().Replace('#', ' ').Trim() == sms.sms_template_definition.ToLower().Replace('#', ' ').Trim());
                    if (same_template)
                    {
                        result = "Duplicate smstemplate";
                    }
                    else
                    {
                        if (sms.id == 0)
                        {


                            SMSTemplateModel obj = new SMSTemplateModel();
                            obj.sms_template_definition = sms.sms_template_definition;
                            obj.sms_template_description = sms.sms_template_description;
                            obj.sms_template_name = sms.sms_template_name;
                            obj.isactive = true;
                            obj.createdby = sms.createdby;
                            obj.createdbyname = sms.createdbyname;
                            obj.createdon = DateTime.Now;
                            obj.updatedby = sms.createdby;
                            obj.updatedbyname = sms.createdbyname;
                            obj.updatedon = DateTime.Now;
                            _db.smstemplates.Add(obj);
                            _db.SaveChanges();
                            scope.Complete();
                            result = "SMS Template Successfully Saved.";

                        }
                        else
                        {
                            var _data = _db.smstemplates.FirstOrDefault(x => x.id == sms.id);
                            _data.sms_template_definition = sms.sms_template_definition;
                            _data.sms_template_description = sms.sms_template_description;
                            _data.sms_template_name = sms.sms_template_name;
                            _data.updatedby = sms.updatedby;
                            _data.updatedbyname = sms.updatedbyname;
                            _data.updatedon = sms.updatedon;
                            _db.SaveChanges();
                            scope.Complete();
                            result = "SMS Template Successfully Updated.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    result = "Error";
                }
                }
            return result;
        }

        public bool DeleteTemplate(SMSTemplateVM sms)
        {
            try
            {
                var _data = _db.smstemplates.FirstOrDefault(x => x.id == sms.id);
                _data.isactive = false;
                _data.updatedby = sms.updatedby;
                _data.updatedbyname = sms.updatedbyname;
                _data.updatedon = DateTime.Now;
                _db.SaveChanges();
                
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

    }
}
