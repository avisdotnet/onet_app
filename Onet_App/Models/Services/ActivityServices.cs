﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Onet_App.Models.Services
{
    public class ActivityServices : IActivityServices
    {
        private readonly ApplicationDbContext _db;
        public ActivityServices(ApplicationDbContext db) => _db = db;
        /// <summary>
        /// Get Activity By Id
        /// </summary>
        /// <param name="leadId"></param>
        /// <returns></returns>
        public List<ActivityHistoryVM> GetActivityByLeadId(int leadId)
        {
            ActivityHistoryVM activity_obj = new ActivityHistoryVM();
            try
            {
                activity_obj.activityHistoryList = _db.activityhistories.Where(x => x.lead_id == leadId)
                    .Select(obj => new ActivityHistoryVM
                    {
                        activity_name = obj.activity_name,
                        lead_id = obj.lead_id,
                        createdby = obj.createdby,
                        createdbyname = obj.createdbyname,
                        createdon = obj.createdon,
                        updatedby = obj.updatedby,
                        updatedbyname = obj.updatedbyname,
                        updatedon = DateTime.Now,
                        time_stamp = DateTime.Now,
                        status = obj.status,
                        summary = obj.summary,
                        isactive=obj.isactive
                    }).ToList();
                return activity_obj.activityHistoryList;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }

        /// <summary>
        /// Get All Activity 
        /// </summary>
        /// <returns></returns>
        public List<ActivityHistoryVM> GetAllActivityDetails()
        {
            ActivityHistoryVM activityVM = new ActivityHistoryVM();
            try
            {
                activityVM.activityHistoryList = _db.activityhistories
                   .Select(obj => new ActivityHistoryVM
                   {
                       activity_name = obj.activity_name,
                       lead_id = obj.lead_id,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedbyname = obj.updatedbyname,
                       updatedon = DateTime.Now,
                       time_stamp = DateTime.Now,
                       status = obj.status,
                       summary = obj.summary,
                       isactive=obj.isactive

                   }).ToList();

                return activityVM.activityHistoryList;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
    }

}
