﻿using Microsoft.Extensions.Configuration;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class CRMServices : ICRMServices
    {
        private readonly ApplicationDbContext _db;
        public CRMServices(ApplicationDbContext db) => _db = db;

        /// <summary>
        /// ADD and EDIT DATA IN CRM FIELD
        /// </summary>
        /// <param name="crmvm"></param>
        /// <returns></returns>

        public bool AddEditCRM(CRMVM crmvm, IConfiguration configuration)
        {
            try
            {
                if (crmvm.campaign_id > 0)
                {
                    if (crmvm.crm_id > 0)
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            try
                            {
                                var Update = _db.crms.FirstOrDefault(x => x.crm_id == crmvm.crm_id);
                                if (Update != null)
                                {
                                    Update.agent_id = crmvm.agent_id;
                                    Update.account_name = crmvm.account_name;
                                    Update.account_no = crmvm.account_no;
                                    Update.address = crmvm.address;
                                    Update.date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null;
                                    Update.amount_collected = crmvm.amount_collected;
                                    Update.authorized_representatives = crmvm.authorized_representatives;
                                    Update.age_group = crmvm.age_group;
                                    Update.balance_for_collection = crmvm.balance_for_collection;
                                    Update.phone1 = crmvm.phone1;
                                    Update.callout_status_level1 = crmvm.callout_status_level1;
                                    Update.callout_status_level2 = crmvm.callout_status_level2;
                                    Update.callout_status_level3 = crmvm.callout_status_level3;
                                    Update.callout_status_level4 = crmvm.callout_status_level4;
                                    Update.callout_status_level5 = crmvm.callout_status_level5;
                                    Update.reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1;
                                    Update.reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2;
                                    Update.city = crmvm.city;
                                    Update.company_name = crmvm.company_name;
                                    Update.count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers;
                                    Update.createdby = crmvm.createdby;
                                    Update.email = crmvm.email;
                                    Update.financial_contact_person = crmvm.financial_contact_person;
                                    Update.give_new_contact_no = crmvm.give_new_contact_no;
                                    Update.invoice_amount = crmvm.invoice_amount;
                                    Update.invoice_no = crmvm.invoice_no;
                                    Update.mode_of_settlement = crmvm.mode_of_settlement;
                                    Update.number_of_accounts = crmvm.number_of_accounts;
                                    Update.outstanding_balance = crmvm.outstanding_balance;
                                    Update.overdue_balance = crmvm.overdue_balance;
                                    Update.payment_channel = crmvm.payment_channel;
                                    Update.phone2 = crmvm.phone2;
                                    Update.phone3 = crmvm.phone3;
                                    Update.phone4 = crmvm.phone4;
                                    Update.ptp_amount = crmvm.ptp_amount;
                                    Update.ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null;
                                    Update.ptp_dated_by = crmvm.ptp_dated_by;
                                    Update.recommendation = crmvm.recommendation;
                                    Update.region_primary = crmvm.region_primary;
                                    Update.region_secondary = crmvm.region_secondary;
                                    Update.skip_trace_child = crmvm.skip_trace_child;
                                    Update.service_id_no = crmvm.service_id_no;
                                    Update.phone_extension = crmvm.phone_extension;
                                    Update.service_type = crmvm.service_type;
                                    Update.skip_trace = crmvm.skip_trace;
                                    Update.status = crmvm.status;
                                    Update.supervisor_id = crmvm.supervisor_id;
                                    Update.updatedby = crmvm.createdby;
                                    Update.updatedbyname = crmvm.createdbyname;
                                    Update.campaign_id = crmvm.campaign_id;
                                    Update.remarks = crmvm.remarks == null ? Update.remarks : crmvm.remarks;
                                    Update.list_name = crmvm.list_name;
                                    Update.endorsement_id = crmvm.endorsement_id;
                                    Update.enable_get_lead = crmvm.enable_get_lead;
                                    Update.updatedon = DateTime.Now;
                                    Update.updatedtimes = Update.updatedtimes + 1;
                                    _db.SaveChanges();
                                    if (crmvm.select_callback == 1)
                                    {
                                        CallBackModel _callBackModel = new CallBackModel
                                        {
                                            lead_id = Update.crm_id,
                                            callback_date_time = crmvm.callback_date_time,
                                            isattempted = false
                                        };
                                        _db.callBacks.Add(_callBackModel);
                                        _db.SaveChanges();
                                    }
                                    if (crmvm.isautomate == true)
                                    {
                                        LeadAssignmentModel leadassign = new LeadAssignmentModel
                                        {
                                            lead_id = Update.crm_id,
                                            campaign_id = crmvm.campaign_id,
                                            agent_id = crmvm.agent_id,
                                            createdby = crmvm.agent_id,
                                            createdbyname = crmvm.createdbyname,
                                            createdon = DateTime.Now,
                                            updatedby = crmvm.createdby,
                                            updatedbyname = crmvm.createdbyname,
                                            updatedon = DateTime.Now
                                        };
                                        _db.leadassigns.Add(leadassign);
                                        _db.SaveChanges();
                                    }
                                    if (crmvm.remarks != null)
                                    {
                                        RemarksModel remark = new RemarksModel
                                        {
                                            lead_id = crmvm.crm_id,
                                            remark = crmvm.remarks == null ? "" : crmvm.remarks,
                                            createdbyname = crmvm.createdbyname,
                                            createdby = crmvm.createdby,
                                            createdon = DateTime.Now,
                                            updatedby = crmvm.createdby,
                                            updatedbyname = crmvm.createdbyname,
                                            updatedon = DateTime.Now,
                                            isactive = true
                                        };
                                        _db.remarks.Add(remark);
                                        _db.SaveChanges();
                                    }
                                    scope.Complete();
                                    scope.Dispose();
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                                return false;
                            }
                        }
                    }
                    else
                    {
                        var LoggerResult = false;
                        var _Campaign = _db.campaigns.FirstOrDefault(x => x.campaign_id == crmvm.campaign_id);
                        if (_Campaign != null)
                        {
                            if (_Campaign.campaign_isautomated == true)
                            {
                                LoggerResult = Logger.UploadLeadtoDialer(crmvm.phone1, _Campaign.campaign_name, _Campaign.skills_name, _Campaign.q_name, crmvm.list_name, configuration, crmvm.phone2, crmvm.phone3, crmvm.phone4);
                            }
                            else
                            {
                                LoggerResult = true;
                            }
                        }
                        if (LoggerResult)
                        {
                            // Add
                            var bool_sameNameExists = _db.crms.FirstOrDefault(x => x.phone1.ToLower().Trim() == crmvm.phone1.ToLower().Trim() && x.campaign_id == crmvm.campaign_id);
                            if (bool_sameNameExists != null)
                            {
                                var Update = _db.crms.FirstOrDefault(x => x.crm_id == bool_sameNameExists.crm_id);
                                if (Update != null)
                                {
                                    Update.agent_id = crmvm.agent_id;
                                    Update.account_name = crmvm.account_name;
                                    Update.account_no = crmvm.account_no;
                                    Update.address = crmvm.address;
                                    Update.date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null;
                                    Update.amount_collected = crmvm.amount_collected;
                                    Update.authorized_representatives = crmvm.authorized_representatives;
                                    Update.age_group = crmvm.age_group;
                                    Update.balance_for_collection = crmvm.balance_for_collection;
                                    Update.phone1 = crmvm.phone1;
                                    Update.callout_status_level1 = crmvm.callout_status_level1;
                                    Update.callout_status_level2 = crmvm.callout_status_level2;
                                    Update.callout_status_level3 = crmvm.callout_status_level3;
                                    Update.callout_status_level4 = crmvm.callout_status_level4;
                                    Update.callout_status_level5 = crmvm.callout_status_level5;
                                    Update.reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1;
                                    Update.reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2;
                                    Update.city = crmvm.city;
                                    Update.company_name = crmvm.company_name;
                                    Update.count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers;
                                    Update.createdby = crmvm.createdby;
                                    //      Update.crm_id = crmvm.crm_id;
                                    Update.email = crmvm.email;
                                    Update.financial_contact_person = crmvm.financial_contact_person;
                                    Update.give_new_contact_no = crmvm.give_new_contact_no;
                                    Update.invoice_amount = crmvm.invoice_amount;
                                    Update.invoice_no = crmvm.invoice_no;
                                    Update.mode_of_settlement = crmvm.mode_of_settlement;
                                    Update.number_of_accounts = crmvm.number_of_accounts;
                                    Update.outstanding_balance = crmvm.outstanding_balance;
                                    Update.overdue_balance = crmvm.overdue_balance;
                                    Update.payment_channel = crmvm.payment_channel;
                                    Update.phone2 = crmvm.phone2;
                                    Update.phone3 = crmvm.phone3;
                                    Update.phone4 = crmvm.phone4;
                                    Update.ptp_amount = crmvm.ptp_amount;
                                    Update.ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null;
                                    Update.ptp_dated_by = crmvm.ptp_dated_by;
                                    Update.recommendation = crmvm.recommendation;
                                    Update.region_primary = crmvm.region_primary;
                                    Update.region_secondary = crmvm.region_secondary;
                                    Update.skip_trace_child = crmvm.skip_trace_child;
                                    Update.service_id_no = crmvm.service_id_no;
                                    Update.phone_extension = crmvm.phone_extension;
                                    Update.service_type = crmvm.service_type;
                                    Update.skip_trace = crmvm.skip_trace;
                                    Update.status = crmvm.status;
                                    Update.supervisor_id = crmvm.supervisor_id;
                                    Update.updatedby = crmvm.createdby;
                                    Update.updatedbyname = crmvm.createdbyname;
                                    Update.campaign_id = crmvm.campaign_id;
                                    Update.remarks = crmvm.remarks == null ? Update.remarks : crmvm.remarks;
                                    Update.list_name = crmvm.list_name;
                                    Update.endorsement_id = crmvm.endorsement_id;
                                    Update.enable_get_lead = crmvm.enable_get_lead;
                                    Update.updatedtimes = Update.updatedtimes + 1;
                                    Update.updatedon = DateTime.Now;
                                    _db.SaveChanges();
                                    if (crmvm.select_callback == 1)
                                    {
                                        CallBackModel _callBackModel = new CallBackModel
                                        {
                                            lead_id = Update.crm_id,
                                            callback_date_time = crmvm.callback_date_time,
                                            isattempted = false
                                        };
                                        _db.callBacks.Add(_callBackModel);
                                        _db.SaveChanges();
                                    }
                                    if (crmvm.isautomate == true)
                                    {
                                        LeadAssignmentModel leadassign = new LeadAssignmentModel
                                        {
                                            lead_id = Update.crm_id,
                                            campaign_id = crmvm.campaign_id,
                                            agent_id = crmvm.agent_id,
                                            createdby = crmvm.agent_id,
                                            createdbyname = crmvm.createdbyname,
                                            createdon = DateTime.Now,
                                            updatedby = crmvm.createdby,
                                            updatedbyname = crmvm.createdbyname,
                                            updatedon = DateTime.Now
                                        };
                                        _db.leadassigns.Add(leadassign);
                                        _db.SaveChanges();
                                    }

                                    if (crmvm.remarks != null)
                                    {
                                        RemarksModel remark = new RemarksModel
                                        {
                                            lead_id = crmvm.crm_id,
                                            remark = crmvm.remarks == null ? "" : crmvm.remarks,
                                            createdbyname = crmvm.createdbyname,
                                            createdby = crmvm.createdby,
                                            createdon = DateTime.Now,
                                            updatedby = crmvm.createdby,
                                            updatedbyname = crmvm.createdbyname,
                                            updatedon = DateTime.Now,
                                            isactive = true
                                        };
                                        _db.remarks.Add(remark);
                                        _db.SaveChanges();
                                    }
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {

                                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                                {
                                    try
                                    {
                                        CRMModel crm_obj = new CRMModel()
                                        {
                                            agent_id = crmvm.agent_id,
                                            account_name = crmvm.account_name,
                                            account_no = crmvm.account_no,
                                            address = crmvm.address,
                                            date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null,
                                            amount_collected = crmvm.amount_collected,
                                            authorized_representatives = crmvm.authorized_representatives,
                                            age_group = crmvm.age_group,
                                            balance_for_collection = crmvm.balance_for_collection,
                                            phone1 = crmvm.phone1,
                                            callout_status_level1 = crmvm.callout_status_level1,
                                            callout_status_level2 = crmvm.callout_status_level2,
                                            callout_status_level3 = crmvm.callout_status_level3,
                                            callout_status_level4 = crmvm.callout_status_level4,
                                            callout_status_level5 = crmvm.callout_status_level5,
                                            city = crmvm.city,
                                            company_name = crmvm.company_name,
                                            count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers,
                                            remarks = crmvm.remarks == null ? "" : crmvm.remarks,
                                            email = crmvm.email,
                                            financial_contact_person = crmvm.financial_contact_person,
                                            give_new_contact_no = crmvm.give_new_contact_no,
                                            invoice_amount = crmvm.invoice_amount,
                                            invoice_no = crmvm.invoice_no,
                                            mode_of_settlement = crmvm.mode_of_settlement,
                                            number_of_accounts = crmvm.number_of_accounts,
                                            outstanding_balance = crmvm.outstanding_balance,
                                            overdue_balance = crmvm.overdue_balance,
                                            payment_channel = crmvm.payment_channel,
                                            reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1,
                                            reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2,
                                            phone2 = crmvm.phone2,
                                            phone3 = crmvm.phone3,
                                            phone4 = crmvm.phone4,
                                            phone_extension = crmvm.phone_extension,
                                            ptp_amount = crmvm.ptp_amount,
                                            ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null,
                                            ptp_dated_by = crmvm.ptp_dated_by,
                                            recommendation = crmvm.recommendation,
                                            region_primary = crmvm.region_primary,
                                            region_secondary = crmvm.region_secondary,
                                            skip_trace_child = crmvm.skip_trace_child,
                                            service_id_no = crmvm.service_id_no,
                                            service_type = crmvm.service_type,
                                            skip_trace = crmvm.skip_trace,
                                            status = crmvm.status,
                                            supervisor_id = crmvm.supervisor_id,
                                            campaign_id = crmvm.campaign_id,
                                            updatedby = crmvm.createdby,
                                            createdon = crmvm.createdon,
                                            createdbyname = crmvm.createdbyname,
                                            updatedbyname = crmvm.createdbyname,
                                            updatedon = crmvm.updatedon,
                                            isactive = true,
                                            createdby = crmvm.createdby,
                                            list_name = crmvm.list_name,
                                            endorsement_id = crmvm.endorsement_id,
                                            enable_get_lead = crmvm.enable_get_lead,
                                            updatedtimes = 0
                                        };
                                        _db.crms.Add(crm_obj);
                                        _db.SaveChanges();

                                        if (crmvm.select_callback == 1)
                                        {
                                            CallBackModel _callBackModel = new CallBackModel
                                            {
                                                lead_id = crm_obj.crm_id,
                                                callback_date_time = crmvm.callback_date_time,
                                                isattempted = false,
                                                updatedby = crmvm.createdby,
                                                createdon = crmvm.createdon,
                                                createdbyname = crmvm.createdbyname,
                                                updatedbyname = crmvm.createdbyname,
                                                updatedon = crmvm.updatedon,
                                                isactive = true,
                                                createdby = crmvm.createdby
                                            };
                                            _db.callBacks.Add(_callBackModel);
                                            _db.SaveChanges();
                                        }
                                        if (crmvm.isautomate == true)
                                        {
                                            LeadAssignmentModel leadassign = new LeadAssignmentModel
                                            {
                                                lead_id = crm_obj.crm_id,
                                                campaign_id = crm_obj.campaign_id,
                                                agent_id = crmvm.agent_id,
                                                createdby = crmvm.createdby,
                                                createdbyname = crmvm.createdbyname,
                                                createdon = DateTime.Now,
                                                updatedby = crmvm.createdby,
                                                updatedbyname = crmvm.createdbyname,
                                                updatedon = DateTime.Now
                                            };
                                            _db.leadassigns.Add(leadassign);
                                            _db.SaveChanges();
                                        }
                                        if (crm_obj.remarks != null)
                                        {
                                            RemarksModel remark = new RemarksModel
                                            {
                                                lead_id = crm_obj.crm_id,
                                                remark = crmvm.remarks,
                                                createdbyname = crmvm.createdbyname,
                                                createdby = crmvm.createdby,
                                                createdon = DateTime.Now,
                                                updatedby = crmvm.createdby,
                                                updatedbyname = crmvm.createdbyname,
                                                updatedon = DateTime.Now,
                                                isactive = true
                                            };
                                            _db.remarks.Add(remark);
                                            _db.SaveChanges();
                                        }
                                        scope.Complete();
                                        scope.Dispose();
                                        return true;
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                                        return false;
                                    }
                                }
                            }

                        }
                        else
                        {
                            return false;
                        }
                    }

                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

        public List<ListModel> GetListModels(int CampaignId)
        {
            List<ListModel> lstendorser = new List<ListModel>();
            try
            {
                lstendorser = _db.crms.Where(x => x.campaign_id == CampaignId).Select(x => new ListModel() { listname = x.list_name }).Distinct().OrderBy(x => x.listname).ToList();
                return lstendorser;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return lstendorser;
            }
        }

        public int AddCRM(List<CRMVM> crmvmlst, IConfiguration configuration, int Campaign_id, int endorsername)
        {
            int _Count = 0;
            try
            {
                if (Campaign_id > 0 && endorsername > 0)
                {
                    var _Timeout = new TimeSpan(23, 59, 59);

                    try
                    {
                        foreach (var crmvm in crmvmlst)
                        {
                            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, Timeout = _Timeout }))
                            {
                                var cam = _db.campaigns.FirstOrDefault(x => x.campaign_id == Campaign_id);
                                if (cam != null)
                                {
                                    if (cam.UniqueValue == "Account Number")
                                    {
                                        var LoggerResult = false;
                                        if (cam.campaign_isautomated == true)
                                        {
                                            LoggerResult = Logger.UploadLeadtoDialer(crmvm.phone1, cam.campaign_name, cam.skills_name, cam.q_name, crmvm.list_name, configuration, crmvm.phone2, crmvm.phone3, crmvm.phone4);
                                        }
                                        else
                                        {
                                            LoggerResult = true;
                                        }
                                        if (LoggerResult)
                                        {
                                            var bool_sameNameExist = _db.crms.FirstOrDefault(x => x.account_no.ToLower().Trim() == crmvm.account_no.ToLower().Trim() && x.campaign_id == crmvm.campaign_id);
                                            if (bool_sameNameExist != null)
                                            {
                                                bool_sameNameExist.agent_id = crmvm.agent_id;
                                                bool_sameNameExist.account_name = crmvm.account_name;
                                                bool_sameNameExist.account_no = crmvm.account_no;
                                                bool_sameNameExist.address = crmvm.address;
                                                bool_sameNameExist.date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null;
                                                bool_sameNameExist.amount_collected = crmvm.amount_collected;
                                                bool_sameNameExist.authorized_representatives = crmvm.authorized_representatives;
                                                bool_sameNameExist.age_group = crmvm.age_group;
                                                bool_sameNameExist.balance_for_collection = crmvm.balance_for_collection;
                                                bool_sameNameExist.phone1 = crmvm.phone1;
                                                bool_sameNameExist.callout_status_level1 = crmvm.callout_status_level1;
                                                bool_sameNameExist.callout_status_level2 = crmvm.callout_status_level2;
                                                bool_sameNameExist.callout_status_level3 = crmvm.callout_status_level3;
                                                bool_sameNameExist.callout_status_level4 = crmvm.callout_status_level4;
                                                bool_sameNameExist.callout_status_level5 = crmvm.callout_status_level5;
                                                bool_sameNameExist.reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1;
                                                bool_sameNameExist.reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2;
                                                bool_sameNameExist.city = crmvm.city;
                                                bool_sameNameExist.company_name = crmvm.company_name;
                                                bool_sameNameExist.count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers;
                                                bool_sameNameExist.createdby = crmvm.createdby;
                                                bool_sameNameExist.email = crmvm.email;
                                                bool_sameNameExist.financial_contact_person = crmvm.financial_contact_person;
                                                bool_sameNameExist.give_new_contact_no = crmvm.give_new_contact_no;
                                                bool_sameNameExist.invoice_amount = crmvm.invoice_amount;
                                                bool_sameNameExist.invoice_no = crmvm.invoice_no;
                                                bool_sameNameExist.mode_of_settlement = crmvm.mode_of_settlement;
                                                bool_sameNameExist.number_of_accounts = crmvm.number_of_accounts;
                                                bool_sameNameExist.outstanding_balance = crmvm.outstanding_balance;
                                                bool_sameNameExist.overdue_balance = crmvm.overdue_balance;
                                                bool_sameNameExist.payment_channel = crmvm.payment_channel;
                                                bool_sameNameExist.phone2 = crmvm.phone2;
                                                bool_sameNameExist.phone3 = crmvm.phone3;
                                                bool_sameNameExist.phone4 = crmvm.phone4;
                                                bool_sameNameExist.ptp_amount = crmvm.ptp_amount;
                                                bool_sameNameExist.ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null;
                                                bool_sameNameExist.ptp_dated_by = crmvm.ptp_dated_by;
                                                bool_sameNameExist.recommendation = crmvm.recommendation;
                                                bool_sameNameExist.region_primary = crmvm.region_primary;
                                                bool_sameNameExist.region_secondary = crmvm.region_secondary;
                                                bool_sameNameExist.skip_trace_child = crmvm.skip_trace_child;
                                                bool_sameNameExist.service_id_no = crmvm.service_id_no;
                                                bool_sameNameExist.phone_extension = crmvm.phone_extension;
                                                bool_sameNameExist.service_type = crmvm.service_type;
                                                bool_sameNameExist.skip_trace = crmvm.skip_trace;
                                                bool_sameNameExist.status = crmvm.status;
                                                bool_sameNameExist.supervisor_id = crmvm.supervisor_id;
                                                bool_sameNameExist.updatedby = crmvm.updatedby;
                                                bool_sameNameExist.campaign_id = crmvm.campaign_id;
                                                bool_sameNameExist.remarks = crmvm.remarks == null ? bool_sameNameExist.remarks : crmvm.remarks;
                                                bool_sameNameExist.list_name = crmvm.list_name;
                                                bool_sameNameExist.endorsement_id = crmvm.endorsement_id;
                                                bool_sameNameExist.enable_get_lead = crmvm.enable_get_lead;
                                                bool_sameNameExist.updatedon = DateTime.Now;
                                                bool_sameNameExist.updatedtimes = bool_sameNameExist.updatedtimes + 1;
                                                _db.SaveChanges();
                                            }
                                            else
                                            {
                                                CRMModel crm_obj = new CRMModel()
                                                {
                                                    agent_id = crmvm.agent_id,
                                                    account_name = crmvm.account_name,
                                                    account_no = crmvm.account_no,
                                                    address = crmvm.address,
                                                    date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null,
                                                    amount_collected = crmvm.amount_collected,
                                                    authorized_representatives = crmvm.authorized_representatives,
                                                    age_group = crmvm.age_group,
                                                    balance_for_collection = crmvm.balance_for_collection,
                                                    phone1 = crmvm.phone1,
                                                    callout_status_level1 = crmvm.callout_status_level1,
                                                    callout_status_level2 = crmvm.callout_status_level2,
                                                    callout_status_level3 = crmvm.callout_status_level3,
                                                    callout_status_level4 = crmvm.callout_status_level4,
                                                    callout_status_level5 = crmvm.callout_status_level5,
                                                    city = crmvm.city,
                                                    company_name = crmvm.company_name,
                                                    count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers,
                                                    remarks = crmvm.remarks == null ? "" : crmvm.remarks,
                                                    email = crmvm.email,
                                                    financial_contact_person = crmvm.financial_contact_person,
                                                    give_new_contact_no = crmvm.give_new_contact_no,
                                                    invoice_amount = crmvm.invoice_amount,
                                                    invoice_no = crmvm.invoice_no,
                                                    mode_of_settlement = crmvm.mode_of_settlement,
                                                    number_of_accounts = crmvm.number_of_accounts,
                                                    outstanding_balance = crmvm.outstanding_balance,
                                                    overdue_balance = crmvm.overdue_balance,
                                                    payment_channel = crmvm.payment_channel,
                                                    reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1,
                                                    reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2,
                                                    phone2 = crmvm.phone2,
                                                    phone3 = crmvm.phone3,
                                                    phone4 = crmvm.phone4,
                                                    phone_extension = crmvm.phone_extension,
                                                    ptp_amount = crmvm.ptp_amount,
                                                    ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null,
                                                    ptp_dated_by = crmvm.ptp_dated_by,
                                                    recommendation = crmvm.recommendation,
                                                    region_primary = crmvm.region_primary,
                                                    region_secondary = crmvm.region_secondary,
                                                    skip_trace_child = crmvm.skip_trace_child,
                                                    service_id_no = crmvm.service_id_no,
                                                    service_type = crmvm.service_type,
                                                    skip_trace = crmvm.skip_trace,
                                                    status = crmvm.status,
                                                    supervisor_id = crmvm.supervisor_id,
                                                    campaign_id = crmvm.campaign_id,
                                                    updatedby = crmvm.updatedby,
                                                    createdon = crmvm.createdon,
                                                    createdbyname = crmvm.createdbyname,
                                                    updatedbyname = crmvm.updatedbyname,
                                                    updatedon = crmvm.updatedon,
                                                    isactive = true,
                                                    createdby = crmvm.createdby,
                                                    list_name = crmvm.list_name,
                                                    endorsement_id = crmvm.endorsement_id,
                                                    enable_get_lead = crmvm.enable_get_lead,
                                                    updatedtimes = 0,
                                                    dailer_disposition = crmvm.dailer_disposition,
                                                    last_called_phoneno = crmvm.last_called_phoneno
                                                };
                                                _db.crms.Add(crm_obj);
                                                _db.SaveChanges();

                                                if (crmvm.isautomate == true)
                                                {
                                                    LeadAssignmentModel leadassign = new LeadAssignmentModel
                                                    {
                                                        lead_id = crm_obj.crm_id,
                                                        campaign_id = crm_obj.campaign_id,
                                                        agent_id = crmvm.agent_id,
                                                        createdby = crmvm.agent_id,
                                                        createdbyname = crmvm.createdbyname,
                                                        createdon = DateTime.Now,
                                                        updatedby = crmvm.agent_id,
                                                        updatedbyname = crmvm.updatedbyname,
                                                        updatedon = DateTime.Now
                                                    };
                                                    _db.leadassigns.Add(leadassign);
                                                    _db.SaveChanges();
                                                }
                                                _Count++;

                                            }
                                        }
                                        scope.Complete();
                                        scope.Dispose();
                                    }
                                    else if (cam.UniqueValue == "Account Name")
                                    {
                                        var LoggerResult = false;

                                        if (cam.campaign_isautomated == true)
                                        {
                                            LoggerResult = Logger.UploadLeadtoDialer(crmvm.phone1, cam.campaign_name, cam.skills_name, cam.q_name, crmvm.list_name, configuration, crmvm.phone2, crmvm.phone3, crmvm.phone4);
                                        }
                                        else
                                        {
                                            LoggerResult = true;
                                        }
                                        if (LoggerResult)
                                        {
                                            var bool_sameNameExist = _db.crms.FirstOrDefault(x => x.account_name.ToLower().Trim() == crmvm.account_name.ToLower().Trim() && x.campaign_id == crmvm.campaign_id);
                                            if (bool_sameNameExist != null)
                                            {
                                                bool_sameNameExist.agent_id = crmvm.agent_id;
                                                bool_sameNameExist.account_name = crmvm.account_name;
                                                bool_sameNameExist.account_no = crmvm.account_no;
                                                bool_sameNameExist.address = crmvm.address;
                                                bool_sameNameExist.date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null;
                                                bool_sameNameExist.amount_collected = crmvm.amount_collected;
                                                bool_sameNameExist.authorized_representatives = crmvm.authorized_representatives;
                                                bool_sameNameExist.age_group = crmvm.age_group;
                                                bool_sameNameExist.balance_for_collection = crmvm.balance_for_collection;
                                                bool_sameNameExist.phone1 = crmvm.phone1;
                                                bool_sameNameExist.callout_status_level1 = crmvm.callout_status_level1;
                                                bool_sameNameExist.callout_status_level2 = crmvm.callout_status_level2;
                                                bool_sameNameExist.callout_status_level3 = crmvm.callout_status_level3;
                                                bool_sameNameExist.callout_status_level4 = crmvm.callout_status_level4;
                                                bool_sameNameExist.callout_status_level5 = crmvm.callout_status_level5;
                                                bool_sameNameExist.reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1;
                                                bool_sameNameExist.reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2;
                                                bool_sameNameExist.city = crmvm.city;
                                                bool_sameNameExist.company_name = crmvm.company_name;
                                                bool_sameNameExist.count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers;
                                                bool_sameNameExist.createdby = crmvm.createdby;
                                                bool_sameNameExist.email = crmvm.email;
                                                bool_sameNameExist.financial_contact_person = crmvm.financial_contact_person;
                                                bool_sameNameExist.give_new_contact_no = crmvm.give_new_contact_no;
                                                bool_sameNameExist.invoice_amount = crmvm.invoice_amount;
                                                bool_sameNameExist.invoice_no = crmvm.invoice_no;
                                                bool_sameNameExist.mode_of_settlement = crmvm.mode_of_settlement;
                                                bool_sameNameExist.number_of_accounts = crmvm.number_of_accounts;
                                                bool_sameNameExist.outstanding_balance = crmvm.outstanding_balance;
                                                bool_sameNameExist.overdue_balance = crmvm.overdue_balance;
                                                bool_sameNameExist.payment_channel = crmvm.payment_channel;
                                                bool_sameNameExist.phone2 = crmvm.phone2;
                                                bool_sameNameExist.phone3 = crmvm.phone3;
                                                bool_sameNameExist.phone4 = crmvm.phone4;
                                                bool_sameNameExist.ptp_amount = crmvm.ptp_amount;
                                                bool_sameNameExist.ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null;
                                                bool_sameNameExist.ptp_dated_by = crmvm.ptp_dated_by;
                                                bool_sameNameExist.recommendation = crmvm.recommendation;
                                                bool_sameNameExist.region_primary = crmvm.region_primary;
                                                bool_sameNameExist.region_secondary = crmvm.region_secondary;
                                                bool_sameNameExist.skip_trace_child = crmvm.skip_trace_child;
                                                bool_sameNameExist.service_id_no = crmvm.service_id_no;
                                                bool_sameNameExist.phone_extension = crmvm.phone_extension;
                                                bool_sameNameExist.service_type = crmvm.service_type;
                                                bool_sameNameExist.skip_trace = crmvm.skip_trace;
                                                bool_sameNameExist.status = crmvm.status;
                                                bool_sameNameExist.supervisor_id = crmvm.supervisor_id;
                                                bool_sameNameExist.updatedby = crmvm.updatedby;
                                                bool_sameNameExist.campaign_id = crmvm.campaign_id;
                                                bool_sameNameExist.remarks = crmvm.remarks == null ? bool_sameNameExist.remarks : crmvm.remarks;
                                                bool_sameNameExist.list_name = crmvm.list_name;
                                                bool_sameNameExist.endorsement_id = crmvm.endorsement_id;
                                                bool_sameNameExist.enable_get_lead = crmvm.enable_get_lead;
                                                bool_sameNameExist.updatedon = DateTime.Now;
                                                bool_sameNameExist.updatedtimes = bool_sameNameExist.updatedtimes + 1;
                                                _db.SaveChanges();
                                            }
                                            else
                                            {


                                                CRMModel crm_obj = new CRMModel()
                                                {
                                                    agent_id = crmvm.agent_id,
                                                    account_name = crmvm.account_name,
                                                    account_no = crmvm.account_no,
                                                    address = crmvm.address,
                                                    date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null,
                                                    amount_collected = crmvm.amount_collected,
                                                    authorized_representatives = crmvm.authorized_representatives,
                                                    age_group = crmvm.age_group,
                                                    balance_for_collection = crmvm.balance_for_collection,
                                                    phone1 = crmvm.phone1,
                                                    callout_status_level1 = crmvm.callout_status_level1,
                                                    callout_status_level2 = crmvm.callout_status_level2,
                                                    callout_status_level3 = crmvm.callout_status_level3,
                                                    callout_status_level4 = crmvm.callout_status_level4,
                                                    callout_status_level5 = crmvm.callout_status_level5,
                                                    city = crmvm.city,
                                                    company_name = crmvm.company_name,
                                                    count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers,
                                                    remarks = crmvm.remarks == null ? "" : crmvm.remarks,
                                                    email = crmvm.email,
                                                    financial_contact_person = crmvm.financial_contact_person,
                                                    give_new_contact_no = crmvm.give_new_contact_no,
                                                    invoice_amount = crmvm.invoice_amount,
                                                    invoice_no = crmvm.invoice_no,
                                                    mode_of_settlement = crmvm.mode_of_settlement,
                                                    number_of_accounts = crmvm.number_of_accounts,
                                                    outstanding_balance = crmvm.outstanding_balance,
                                                    overdue_balance = crmvm.overdue_balance,
                                                    payment_channel = crmvm.payment_channel,
                                                    reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1,
                                                    reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2,
                                                    phone2 = crmvm.phone2,
                                                    phone3 = crmvm.phone3,
                                                    phone4 = crmvm.phone4,
                                                    phone_extension = crmvm.phone_extension,
                                                    ptp_amount = crmvm.ptp_amount,
                                                    ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null,
                                                    ptp_dated_by = crmvm.ptp_dated_by,
                                                    recommendation = crmvm.recommendation,
                                                    region_primary = crmvm.region_primary,
                                                    region_secondary = crmvm.region_secondary,
                                                    skip_trace_child = crmvm.skip_trace_child,
                                                    service_id_no = crmvm.service_id_no,
                                                    service_type = crmvm.service_type,
                                                    skip_trace = crmvm.skip_trace,
                                                    status = crmvm.status,
                                                    supervisor_id = crmvm.supervisor_id,
                                                    campaign_id = crmvm.campaign_id,
                                                    updatedby = crmvm.updatedby,
                                                    createdon = crmvm.createdon,
                                                    createdbyname = crmvm.createdbyname,
                                                    updatedbyname = crmvm.updatedbyname,
                                                    updatedon = crmvm.updatedon,
                                                    isactive = true,
                                                    createdby = crmvm.createdby,
                                                    list_name = crmvm.list_name,
                                                    endorsement_id = crmvm.endorsement_id,
                                                    enable_get_lead = crmvm.enable_get_lead,
                                                    updatedtimes = 0,
                                                    dailer_disposition = crmvm.dailer_disposition,
                                                    last_called_phoneno = crmvm.last_called_phoneno

                                                };
                                                _db.crms.Add(crm_obj);
                                                _db.SaveChanges();

                                                if (crmvm.isautomate == true)
                                                {
                                                    LeadAssignmentModel leadassign = new LeadAssignmentModel
                                                    {
                                                        lead_id = crm_obj.crm_id,
                                                        campaign_id = crm_obj.campaign_id,
                                                        agent_id = crmvm.agent_id,
                                                        createdby = crmvm.agent_id,
                                                        createdbyname = crmvm.createdbyname,
                                                        createdon = DateTime.Now,
                                                        updatedby = crmvm.agent_id,
                                                        updatedbyname = crmvm.updatedbyname,
                                                        updatedon = DateTime.Now
                                                    };
                                                    _db.leadassigns.Add(leadassign);
                                                    _db.SaveChanges();
                                                }
                                                _Count++;

                                            }
                                        }
                                        scope.Complete();
                                        scope.Dispose();
                                    }
                                    else if (cam.UniqueValue == "Invoice Number")
                                    {
                                        var LoggerResult = false;

                                        if (cam.campaign_isautomated == true)
                                        {
                                            LoggerResult = Logger.UploadLeadtoDialer(crmvm.phone1, cam.campaign_name, cam.skills_name, cam.q_name, crmvm.list_name, configuration, crmvm.phone2, crmvm.phone3, crmvm.phone4);
                                        }
                                        else
                                        {
                                            LoggerResult = true;
                                        }
                                        if (LoggerResult)
                                        {
                                            var bool_sameNameExist = _db.crms.FirstOrDefault(x => x.invoice_no.ToLower().Trim() == crmvm.invoice_no.ToLower().Trim() && x.campaign_id == crmvm.campaign_id);
                                            if (bool_sameNameExist != null)
                                            {
                                                bool_sameNameExist.agent_id = crmvm.agent_id;
                                                bool_sameNameExist.account_name = crmvm.account_name;
                                                bool_sameNameExist.account_no = crmvm.account_no;
                                                bool_sameNameExist.address = crmvm.address;
                                                bool_sameNameExist.date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null;
                                                bool_sameNameExist.amount_collected = crmvm.amount_collected;
                                                bool_sameNameExist.authorized_representatives = crmvm.authorized_representatives;
                                                bool_sameNameExist.age_group = crmvm.age_group;
                                                bool_sameNameExist.balance_for_collection = crmvm.balance_for_collection;
                                                bool_sameNameExist.phone1 = crmvm.phone1;
                                                bool_sameNameExist.callout_status_level1 = crmvm.callout_status_level1;
                                                bool_sameNameExist.callout_status_level2 = crmvm.callout_status_level2;
                                                bool_sameNameExist.callout_status_level3 = crmvm.callout_status_level3;
                                                bool_sameNameExist.callout_status_level4 = crmvm.callout_status_level4;
                                                bool_sameNameExist.callout_status_level5 = crmvm.callout_status_level5;
                                                bool_sameNameExist.reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1;
                                                bool_sameNameExist.reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2;
                                                bool_sameNameExist.city = crmvm.city;
                                                bool_sameNameExist.company_name = crmvm.company_name;
                                                bool_sameNameExist.count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers;
                                                bool_sameNameExist.createdby = crmvm.createdby;
                                                bool_sameNameExist.email = crmvm.email;
                                                bool_sameNameExist.financial_contact_person = crmvm.financial_contact_person;
                                                bool_sameNameExist.give_new_contact_no = crmvm.give_new_contact_no;
                                                bool_sameNameExist.invoice_amount = crmvm.invoice_amount;
                                                bool_sameNameExist.invoice_no = crmvm.invoice_no;
                                                bool_sameNameExist.mode_of_settlement = crmvm.mode_of_settlement;
                                                bool_sameNameExist.number_of_accounts = crmvm.number_of_accounts;
                                                bool_sameNameExist.outstanding_balance = crmvm.outstanding_balance;
                                                bool_sameNameExist.overdue_balance = crmvm.overdue_balance;
                                                bool_sameNameExist.payment_channel = crmvm.payment_channel;
                                                bool_sameNameExist.phone2 = crmvm.phone2;
                                                bool_sameNameExist.phone3 = crmvm.phone3;
                                                bool_sameNameExist.phone4 = crmvm.phone4;
                                                bool_sameNameExist.ptp_amount = crmvm.ptp_amount;
                                                bool_sameNameExist.ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null;
                                                bool_sameNameExist.ptp_dated_by = crmvm.ptp_dated_by;
                                                bool_sameNameExist.recommendation = crmvm.recommendation;
                                                bool_sameNameExist.region_primary = crmvm.region_primary;
                                                bool_sameNameExist.region_secondary = crmvm.region_secondary;
                                                bool_sameNameExist.skip_trace_child = crmvm.skip_trace_child;
                                                bool_sameNameExist.service_id_no = crmvm.service_id_no;
                                                bool_sameNameExist.phone_extension = crmvm.phone_extension;
                                                bool_sameNameExist.service_type = crmvm.service_type;
                                                bool_sameNameExist.skip_trace = crmvm.skip_trace;
                                                bool_sameNameExist.status = crmvm.status;
                                                bool_sameNameExist.supervisor_id = crmvm.supervisor_id;
                                                bool_sameNameExist.updatedby = crmvm.updatedby;
                                                bool_sameNameExist.campaign_id = crmvm.campaign_id;
                                                bool_sameNameExist.remarks = crmvm.remarks == null ? bool_sameNameExist.remarks : crmvm.remarks;
                                                bool_sameNameExist.list_name = crmvm.list_name;
                                                bool_sameNameExist.endorsement_id = crmvm.endorsement_id;
                                                bool_sameNameExist.enable_get_lead = crmvm.enable_get_lead;
                                                bool_sameNameExist.updatedon = DateTime.Now;
                                                bool_sameNameExist.updatedtimes = bool_sameNameExist.updatedtimes + 1;
                                                _db.SaveChanges();
                                            }
                                            else
                                            {


                                                CRMModel crm_obj = new CRMModel()
                                                {
                                                    agent_id = crmvm.agent_id,
                                                    account_name = crmvm.account_name,
                                                    account_no = crmvm.account_no,
                                                    address = crmvm.address,
                                                    date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null,
                                                    amount_collected = crmvm.amount_collected,
                                                    authorized_representatives = crmvm.authorized_representatives,
                                                    age_group = crmvm.age_group,
                                                    balance_for_collection = crmvm.balance_for_collection,
                                                    phone1 = crmvm.phone1,
                                                    callout_status_level1 = crmvm.callout_status_level1,
                                                    callout_status_level2 = crmvm.callout_status_level2,
                                                    callout_status_level3 = crmvm.callout_status_level3,
                                                    callout_status_level4 = crmvm.callout_status_level4,
                                                    callout_status_level5 = crmvm.callout_status_level5,
                                                    city = crmvm.city,
                                                    company_name = crmvm.company_name,
                                                    count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers,
                                                    remarks = crmvm.remarks == null ? "" : crmvm.remarks,
                                                    email = crmvm.email,
                                                    financial_contact_person = crmvm.financial_contact_person,
                                                    give_new_contact_no = crmvm.give_new_contact_no,
                                                    invoice_amount = crmvm.invoice_amount,
                                                    invoice_no = crmvm.invoice_no,
                                                    mode_of_settlement = crmvm.mode_of_settlement,
                                                    number_of_accounts = crmvm.number_of_accounts,
                                                    outstanding_balance = crmvm.outstanding_balance,
                                                    overdue_balance = crmvm.overdue_balance,
                                                    payment_channel = crmvm.payment_channel,
                                                    reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1,
                                                    reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2,
                                                    phone2 = crmvm.phone2,
                                                    phone3 = crmvm.phone3,
                                                    phone4 = crmvm.phone4,
                                                    phone_extension = crmvm.phone_extension,
                                                    ptp_amount = crmvm.ptp_amount,
                                                    ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null,
                                                    ptp_dated_by = crmvm.ptp_dated_by,
                                                    recommendation = crmvm.recommendation,
                                                    region_primary = crmvm.region_primary,
                                                    region_secondary = crmvm.region_secondary,
                                                    skip_trace_child = crmvm.skip_trace_child,
                                                    service_id_no = crmvm.service_id_no,
                                                    service_type = crmvm.service_type,
                                                    skip_trace = crmvm.skip_trace,
                                                    status = crmvm.status,
                                                    supervisor_id = crmvm.supervisor_id,
                                                    campaign_id = crmvm.campaign_id,
                                                    updatedby = crmvm.updatedby,
                                                    createdon = crmvm.createdon,
                                                    createdbyname = crmvm.createdbyname,
                                                    updatedbyname = crmvm.updatedbyname,
                                                    updatedon = crmvm.updatedon,
                                                    isactive = true,
                                                    createdby = crmvm.createdby,
                                                    list_name = crmvm.list_name,
                                                    endorsement_id = crmvm.endorsement_id,
                                                    enable_get_lead = crmvm.enable_get_lead,
                                                    updatedtimes = 0,
                                                    dailer_disposition = crmvm.dailer_disposition,
                                                    last_called_phoneno = crmvm.last_called_phoneno
                                                };
                                                _db.crms.Add(crm_obj);
                                                _db.SaveChanges();

                                                if (crmvm.isautomate == true)
                                                {
                                                    LeadAssignmentModel leadassign = new LeadAssignmentModel
                                                    {
                                                        lead_id = crm_obj.crm_id,
                                                        campaign_id = crm_obj.campaign_id,
                                                        agent_id = crmvm.agent_id,
                                                        createdby = crmvm.agent_id,
                                                        createdbyname = crmvm.createdbyname,
                                                        createdon = DateTime.Now,
                                                        updatedby = crmvm.agent_id,
                                                        updatedbyname = crmvm.updatedbyname,
                                                        updatedon = DateTime.Now
                                                    };
                                                    _db.leadassigns.Add(leadassign);
                                                    _db.SaveChanges();
                                                }
                                                _Count++;

                                            }
                                        }
                                    }
                                    else if (cam.UniqueValue == "Phone Number")
                                    {
                                        var LoggerResult = false;

                                        if (cam.campaign_isautomated == true)
                                        {
                                            Logger.WriteErrLog("", "");
                                            LoggerResult = Logger.UploadLeadtoDialer(crmvm.phone1, cam.campaign_name, cam.skills_name, cam.q_name, crmvm.list_name, configuration, crmvm.phone2, crmvm.phone3, crmvm.phone4);
                                        }
                                        else
                                        {
                                            LoggerResult = true;
                                        }
                                        if (LoggerResult)
                                        {
                                            var bool_sameNameExist = _db.crms.FirstOrDefault(x => x.phone1.ToLower().Trim() == crmvm.phone1.ToLower().Trim() && x.campaign_id == crmvm.campaign_id);
                                            if (bool_sameNameExist != null)
                                            {
                                                bool_sameNameExist.agent_id = crmvm.agent_id;
                                                bool_sameNameExist.account_name = crmvm.account_name;
                                                bool_sameNameExist.account_no = crmvm.account_no;
                                                bool_sameNameExist.address = crmvm.address;
                                                bool_sameNameExist.date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null;
                                                bool_sameNameExist.amount_collected = crmvm.amount_collected;
                                                bool_sameNameExist.authorized_representatives = crmvm.authorized_representatives;
                                                bool_sameNameExist.age_group = crmvm.age_group;
                                                bool_sameNameExist.balance_for_collection = crmvm.balance_for_collection;
                                                bool_sameNameExist.phone1 = crmvm.phone1;
                                                bool_sameNameExist.callout_status_level1 = crmvm.callout_status_level1;
                                                bool_sameNameExist.callout_status_level2 = crmvm.callout_status_level2;
                                                bool_sameNameExist.callout_status_level3 = crmvm.callout_status_level3;
                                                bool_sameNameExist.callout_status_level4 = crmvm.callout_status_level4;
                                                bool_sameNameExist.callout_status_level5 = crmvm.callout_status_level5;
                                                bool_sameNameExist.reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1;
                                                bool_sameNameExist.reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2;
                                                bool_sameNameExist.city = crmvm.city;
                                                bool_sameNameExist.company_name = crmvm.company_name;
                                                bool_sameNameExist.count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers;
                                                bool_sameNameExist.createdby = crmvm.createdby;
                                                bool_sameNameExist.email = crmvm.email;
                                                bool_sameNameExist.financial_contact_person = crmvm.financial_contact_person;
                                                bool_sameNameExist.give_new_contact_no = crmvm.give_new_contact_no;
                                                bool_sameNameExist.invoice_amount = crmvm.invoice_amount;
                                                bool_sameNameExist.invoice_no = crmvm.invoice_no;
                                                bool_sameNameExist.mode_of_settlement = crmvm.mode_of_settlement;
                                                bool_sameNameExist.number_of_accounts = crmvm.number_of_accounts;
                                                bool_sameNameExist.outstanding_balance = crmvm.outstanding_balance;
                                                bool_sameNameExist.overdue_balance = crmvm.overdue_balance;
                                                bool_sameNameExist.payment_channel = crmvm.payment_channel;
                                                bool_sameNameExist.phone2 = crmvm.phone2;
                                                bool_sameNameExist.phone3 = crmvm.phone3;
                                                bool_sameNameExist.phone4 = crmvm.phone4;
                                                bool_sameNameExist.ptp_amount = crmvm.ptp_amount;
                                                bool_sameNameExist.ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null;
                                                bool_sameNameExist.ptp_dated_by = crmvm.ptp_dated_by;
                                                bool_sameNameExist.recommendation = crmvm.recommendation;
                                                bool_sameNameExist.region_primary = crmvm.region_primary;
                                                bool_sameNameExist.region_secondary = crmvm.region_secondary;
                                                bool_sameNameExist.skip_trace_child = crmvm.skip_trace_child;
                                                bool_sameNameExist.service_id_no = crmvm.service_id_no;
                                                bool_sameNameExist.phone_extension = crmvm.phone_extension;
                                                bool_sameNameExist.service_type = crmvm.service_type;
                                                bool_sameNameExist.skip_trace = crmvm.skip_trace;
                                                bool_sameNameExist.status = crmvm.status;
                                                bool_sameNameExist.supervisor_id = crmvm.supervisor_id;
                                                bool_sameNameExist.updatedby = crmvm.updatedby;
                                                bool_sameNameExist.campaign_id = crmvm.campaign_id;
                                                bool_sameNameExist.remarks = crmvm.remarks == null ? bool_sameNameExist.remarks : crmvm.remarks;
                                                bool_sameNameExist.list_name = crmvm.list_name;
                                                bool_sameNameExist.endorsement_id = crmvm.endorsement_id;
                                                bool_sameNameExist.enable_get_lead = crmvm.enable_get_lead;
                                                bool_sameNameExist.updatedon = DateTime.Now;
                                                bool_sameNameExist.updatedtimes = bool_sameNameExist.updatedtimes + 1;
                                                _db.SaveChanges();
                                            }
                                            else
                                            {


                                                CRMModel crm_obj = new CRMModel()
                                                {
                                                    agent_id = crmvm.agent_id,
                                                    account_name = crmvm.account_name,
                                                    account_no = crmvm.account_no,
                                                    address = crmvm.address,
                                                    date_invoice = crmvm.date_invoice.HasValue == true ? crmvm.date_invoice : null,
                                                    amount_collected = crmvm.amount_collected,
                                                    authorized_representatives = crmvm.authorized_representatives,
                                                    age_group = crmvm.age_group,
                                                    balance_for_collection = crmvm.balance_for_collection,
                                                    phone1 = crmvm.phone1,
                                                    callout_status_level1 = crmvm.callout_status_level1,
                                                    callout_status_level2 = crmvm.callout_status_level2,
                                                    callout_status_level3 = crmvm.callout_status_level3,
                                                    callout_status_level4 = crmvm.callout_status_level4,
                                                    callout_status_level5 = crmvm.callout_status_level5,
                                                    city = crmvm.city,
                                                    company_name = crmvm.company_name,
                                                    count_of_attempted_contact_numbers = crmvm.count_of_attempted_contact_numbers,
                                                    remarks = crmvm.remarks == null ? "" : crmvm.remarks,
                                                    email = crmvm.email,
                                                    financial_contact_person = crmvm.financial_contact_person,
                                                    give_new_contact_no = crmvm.give_new_contact_no,
                                                    invoice_amount = crmvm.invoice_amount,
                                                    invoice_no = crmvm.invoice_no,
                                                    mode_of_settlement = crmvm.mode_of_settlement,
                                                    number_of_accounts = crmvm.number_of_accounts,
                                                    outstanding_balance = crmvm.outstanding_balance,
                                                    overdue_balance = crmvm.overdue_balance,
                                                    payment_channel = crmvm.payment_channel,
                                                    reason_for_non_payment_level1 = crmvm.reason_for_non_payment_level1,
                                                    reason_for_non_payment_level2 = crmvm.reason_for_non_payment_level2,
                                                    phone2 = crmvm.phone2,
                                                    phone3 = crmvm.phone3,
                                                    phone4 = crmvm.phone4,
                                                    phone_extension = crmvm.phone_extension,
                                                    ptp_amount = crmvm.ptp_amount,
                                                    ptp_date = crmvm.ptp_date.HasValue == true ? crmvm.ptp_date : null,
                                                    ptp_dated_by = crmvm.ptp_dated_by,
                                                    recommendation = crmvm.recommendation,
                                                    region_primary = crmvm.region_primary,
                                                    region_secondary = crmvm.region_secondary,
                                                    skip_trace_child = crmvm.skip_trace_child,
                                                    service_id_no = crmvm.service_id_no,
                                                    service_type = crmvm.service_type,
                                                    skip_trace = crmvm.skip_trace,
                                                    status = crmvm.status,
                                                    supervisor_id = crmvm.supervisor_id,
                                                    campaign_id = crmvm.campaign_id,
                                                    updatedby = crmvm.updatedby,
                                                    createdon = crmvm.createdon,
                                                    createdbyname = crmvm.createdbyname,
                                                    updatedbyname = crmvm.updatedbyname,
                                                    updatedon = crmvm.updatedon,
                                                    isactive = true,
                                                    createdby = crmvm.createdby,
                                                    list_name = crmvm.list_name,
                                                    endorsement_id = crmvm.endorsement_id,
                                                    enable_get_lead = crmvm.enable_get_lead,
                                                    updatedtimes = 0,
                                                    dailer_disposition = crmvm.dailer_disposition,
                                                    last_called_phoneno = crmvm.last_called_phoneno

                                                };
                                                _db.crms.Add(crm_obj);
                                                _db.SaveChanges();

                                                if (crmvm.isautomate == true)
                                                {
                                                    LeadAssignmentModel leadassign = new LeadAssignmentModel
                                                    {
                                                        lead_id = crm_obj.crm_id,
                                                        campaign_id = crm_obj.campaign_id,
                                                        agent_id = crmvm.agent_id,
                                                        createdby = crmvm.agent_id,
                                                        createdbyname = crmvm.createdbyname,
                                                        createdon = DateTime.Now,
                                                        updatedby = crmvm.agent_id,
                                                        updatedbyname = crmvm.updatedbyname,
                                                        updatedon = DateTime.Now
                                                    };
                                                    _db.leadassigns.Add(leadassign);
                                                    _db.SaveChanges();
                                                }
                                                _Count++;

                                            }
                                        }
                                    }
                                    scope.Complete();
                                    scope.Dispose();
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        return _Count;
                    }
                    return _Count;
                }
                else
                {
                    return _Count;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return _Count;
            }
        }
        /// <summary>
        /// GET ALL CRM DETRAILS
        /// </summary>
        /// <returns></returns>
        public List<CRMVM> GetAllCrmDetails()
        {
            CRMVM crmVM = new CRMVM();
            try
            {
                crmVM._crmlst = _db.crms.Select(obj => new CRMVM
                {
                    agent_id = obj.agent_id,
                    account_name = obj.account_name,
                    account_no = obj.account_no,
                    address = obj.address,
                    date_invoice = obj.date_invoice.HasValue == true ? obj.date_invoice : null,
                    amount_collected = obj.amount_collected,
                    authorized_representatives = obj.authorized_representatives,
                    age_group = obj.age_group,
                    balance_for_collection = obj.balance_for_collection,
                    phone1 = obj.phone1,
                    callout_status_level1 = obj.callout_status_level1,
                    callout_status_level2 = obj.callout_status_level2,
                    callout_status_level3 = obj.callout_status_level3,
                    callout_status_level4 = obj.callout_status_level4,
                    callout_status_level5 = obj.callout_status_level5,
                    reason_for_non_payment_level1 = obj.reason_for_non_payment_level1,
                    reason_for_non_payment_level2 = obj.reason_for_non_payment_level2,
                    callout_status_level1_Text = "",
                    callout_status_level2_Text = "",
                    callout_status_level3_Text = "",
                    callout_status_level4_Text = "",
                    callout_status_level5_Text = "",
                    Balance_For_Collection_Text = "",
                    Mode_of_settlement_Text = "",
                    Payment_channel_Text = "",
                    region_primary_text = "",
                    region_secondary_text = "",
                    Skip_trace_Text = "",
                    Child_Skip_trace_Text = "",
                    Status_Text = "",
                    city = obj.city,
                    campaign_id = obj.campaign_id,
                    campaign_name = "",
                    company_name = obj.company_name,
                    count_of_attempted_contact_numbers = obj.count_of_attempted_contact_numbers,
                    createdby = obj.createdby,
                    crm_id = obj.crm_id,
                    email = obj.email,
                    financial_contact_person = obj.financial_contact_person,
                    give_new_contact_no = obj.give_new_contact_no,
                    invoice_amount = obj.invoice_amount,
                    invoice_no = obj.invoice_no,
                    mode_of_settlement = obj.mode_of_settlement,
                    number_of_accounts = obj.number_of_accounts,
                    outstanding_balance = obj.outstanding_balance,
                    overdue_balance = obj.overdue_balance,
                    phone_extension = obj.phone_extension,
                    payment_channel = obj.payment_channel,
                    phone2 = obj.phone2,
                    phone3 = obj.phone3,
                    phone4 = obj.phone4,
                    ptp_amount = obj.ptp_amount,
                    ptp_date = obj.ptp_date.HasValue == true ? obj.ptp_date : null,
                    ptp_dated_by = obj.ptp_dated_by,
                    recommendation = obj.recommendation,
                    region_primary = obj.region_primary,
                    region_secondary = obj.region_secondary,
                    skip_trace_child = obj.skip_trace_child,
                    service_id_no = obj.service_id_no,
                    service_type = obj.service_type,
                    skip_trace = obj.skip_trace,
                    status = obj.status,
                    supervisor_id = obj.supervisor_id,
                    createdbyname = obj.createdbyname,
                    createdon = obj.createdon,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    isactive = true,
                    remarks = obj.remarks,
                    list_name = obj.list_name,
                    endorsement_id = obj.endorsement_id,
                    enable_get_lead = obj.enable_get_lead,
                    dailer_disposition = obj.dailer_disposition,
                    last_called_phoneno = obj.last_called_phoneno

                }).ToList();
                var _data_dispositions = _db.dispositions.ToList();
                var _data_balanceForCollections = _db.balanceForCollections.ToList();
                var _data_modeofsettlements = _db.modeofsettlements.ToList();
                var _data_paymentchannels = _db.paymentchannels.ToList();
                var _data_regionprimarys = _db.regionprimarys.ToList();
                var _data_regionsecondarys = _db.regionsecondarys.ToList();
                var _data_skipstrace = _db.skipstrace.ToList();
                var _data_skiptracechilds = _db.skiptracechilds.ToList();
                var _data_statuses = _db.statuses.ToList();
                var _data_campaigns = _db.campaigns.ToList();
                var _data_reasonfornonpayments = _db.reasonfornonpayments.ToList();
                var _data_reasonfornonpaymentchilds = _db.reasonfornonpaymentchilds.ToList();
                foreach (var item in crmVM._crmlst)
                {
                    if (item.callout_status_level1 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1);
                        if (_data != null)
                        {
                            item.callout_status_level1_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level2 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level2);
                        if (_data != null)
                        {
                            item.callout_status_level2_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level3 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level3);
                        if (_data != null)
                        {
                            item.callout_status_level3_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level4 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level4);
                        if (_data != null)
                        {
                            item.callout_status_level4_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level5 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level5);
                        if (_data != null)
                        {
                            item.callout_status_level5_Text = _data.disposition_name;
                        }
                    }
                    if (item.balance_for_collection > 0)
                    {
                        var _data = _data_balanceForCollections.FirstOrDefault(x => x.id == item.balance_for_collection);
                        if (_data != null)
                        {
                            item.Balance_For_Collection_Text = _data.balance_collection_name;
                        }
                    }
                    if (item.mode_of_settlement > 0)
                    {
                        var _data = _data_modeofsettlements.FirstOrDefault(x => x.id == item.mode_of_settlement);
                        if (_data != null)
                        {
                            item.Mode_of_settlement_Text = _data.modeofsettlement_name;
                        }
                    }
                    if (item.payment_channel > 0)
                    {
                        var _data = _data_paymentchannels.FirstOrDefault(x => x.id == item.payment_channel);
                        if (_data != null)
                        {
                            item.Payment_channel_Text = _data.payment_channel_name;
                        }
                    }
                    if (item.region_primary > 0)
                    {
                        var _data = _data_regionprimarys.FirstOrDefault(x => x.id == item.region_primary);
                        if (_data != null)
                        {
                            item.region_primary_text = _data.region_name;
                        }
                    }
                    if (item.region_secondary > 0)
                    {
                        var _data = _data_regionsecondarys.FirstOrDefault(x => x.id == item.region_secondary);
                        if (_data != null)
                        {
                            item.region_secondary_text = _data.region_secondary;
                        }
                    }
                    if (item.skip_trace > 0)
                    {
                        var _data = _data_skipstrace.FirstOrDefault(x => x.id == item.skip_trace);
                        if (_data != null)
                        {
                            item.Skip_trace_Text = _data.skip_trace_name;
                        }
                    }
                    if (item.skip_trace_child > 0)
                    {
                        var _data = _data_skiptracechilds.FirstOrDefault(x => x.id == item.skip_trace_child);
                        if (_data != null)
                        {
                            item.Child_Skip_trace_Text = _data.skip_trace_child;
                        }
                    }
                    if (item.status > 0)
                    {
                        var _data = _data_statuses.FirstOrDefault(x => x.id == item.status);
                        if (_data != null)
                        {
                            item.Status_Text = _data.status_name;
                        }
                    }
                    if (item.campaign_id > 0)
                    {
                        var _data = _data_campaigns.FirstOrDefault(x => x.campaign_id == item.campaign_id);
                        if (_data != null)
                        {
                            item.campaign_name = _data.campaign_name;
                        }
                    }
                    if (item.reason_for_non_payment_level1 > 0)
                    {
                        var _data = _data_reasonfornonpayments.FirstOrDefault(x => x.id == item.reason_for_non_payment_level1);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level1_Text = _data.reason_for_non_payment_name;
                        }
                    }
                    if (item.reason_for_non_payment_level2 > 0)
                    {
                        var _data = _data_reasonfornonpaymentchilds.FirstOrDefault(x => x.id == item.reason_for_non_payment_level2);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level2_Text = _data.reason_for_non_payment_child_name;
                        }
                    }
                    var Agent_name = (from crm in _db.leadassigns.Where(x => x.lead_id == item.crm_id)
                                      join user in _db.users on crm.agent_id equals user.user_id
                                      select new UserModel()
                                      {
                                          username = user.username,
                                          user_fullname = user.user_fullname
                                      }).ToList();

                    foreach (var Agent_nameitem in Agent_name)
                    {
                        item.Agent_name = Agent_nameitem.user_fullname;
                    }
                }

                return crmVM._crmlst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        public List<CRMVM> GetAllCrmDetails(List<int> _ids)
        {
            CRMVM crmVM = new CRMVM();
            try
            {
                crmVM._crmlst = _db.crms.Where(x => _ids.Contains(x.crm_id)).Select(obj => new CRMVM
                {
                    agent_id = obj.agent_id,
                    account_name = obj.account_name,
                    account_no = obj.account_no,
                    address = obj.address,
                    date_invoice = obj.date_invoice.HasValue == true ? obj.date_invoice : null,
                    amount_collected = obj.amount_collected,
                    authorized_representatives = obj.authorized_representatives,
                    age_group = obj.age_group,
                    balance_for_collection = obj.balance_for_collection,
                    phone1 = obj.phone1,
                    callout_status_level1 = obj.callout_status_level1,
                    callout_status_level2 = obj.callout_status_level2,
                    callout_status_level3 = obj.callout_status_level3,
                    callout_status_level4 = obj.callout_status_level4,
                    callout_status_level5 = obj.callout_status_level5,
                    reason_for_non_payment_level1 = obj.reason_for_non_payment_level1,
                    reason_for_non_payment_level2 = obj.reason_for_non_payment_level2,
                    callout_status_level1_Text = "",
                    callout_status_level2_Text = "",
                    callout_status_level3_Text = "",
                    callout_status_level4_Text = "",
                    callout_status_level5_Text = "",
                    Balance_For_Collection_Text = "",
                    Mode_of_settlement_Text = "",
                    Payment_channel_Text = "",
                    region_primary_text = "",
                    region_secondary_text = "",
                    Skip_trace_Text = "",
                    Child_Skip_trace_Text = "",
                    Status_Text = "",
                    city = obj.city,
                    campaign_id = obj.campaign_id,
                    campaign_name = "",
                    company_name = obj.company_name,
                    count_of_attempted_contact_numbers = obj.count_of_attempted_contact_numbers,
                    createdby = obj.createdby,
                    crm_id = obj.crm_id,
                    email = obj.email,
                    financial_contact_person = obj.financial_contact_person,
                    give_new_contact_no = obj.give_new_contact_no,
                    invoice_amount = obj.invoice_amount,
                    invoice_no = obj.invoice_no,
                    mode_of_settlement = obj.mode_of_settlement,
                    number_of_accounts = obj.number_of_accounts,
                    outstanding_balance = obj.outstanding_balance,
                    overdue_balance = obj.overdue_balance,
                    phone_extension = obj.phone_extension,
                    payment_channel = obj.payment_channel,
                    phone2 = obj.phone2,
                    phone3 = obj.phone3,
                    phone4 = obj.phone4,
                    ptp_amount = obj.ptp_amount,
                    ptp_date = obj.ptp_date.HasValue == true ? obj.ptp_date : null,
                    ptp_dated_by = obj.ptp_dated_by,
                    recommendation = obj.recommendation,
                    region_primary = obj.region_primary,
                    region_secondary = obj.region_secondary,
                    skip_trace_child = obj.skip_trace_child,
                    service_id_no = obj.service_id_no,
                    service_type = obj.service_type,
                    skip_trace = obj.skip_trace,
                    status = obj.status,
                    supervisor_id = obj.supervisor_id,
                    createdbyname = obj.createdbyname,
                    createdon = obj.createdon,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    isactive = true,
                    remarks = obj.remarks,
                    list_name = obj.list_name,
                    endorsement_id = obj.endorsement_id,
                    enable_get_lead = obj.enable_get_lead,
                    dailer_disposition = obj.dailer_disposition,
                    last_called_phoneno = obj.last_called_phoneno

                }).ToList();
                var _data_dispositions = _db.dispositions.ToList();
                var _data_balanceForCollections = _db.balanceForCollections.ToList();
                var _data_modeofsettlements = _db.modeofsettlements.ToList();
                var _data_paymentchannels = _db.paymentchannels.ToList();
                var _data_regionprimarys = _db.regionprimarys.ToList();
                var _data_regionsecondarys = _db.regionsecondarys.ToList();
                var _data_skipstrace = _db.skipstrace.ToList();
                var _data_skiptracechilds = _db.skiptracechilds.ToList();
                var _data_statuses = _db.statuses.ToList();
                var _data_campaigns = _db.campaigns.ToList();
                var _data_reasonfornonpayments = _db.reasonfornonpayments.ToList();
                var _data_reasonfornonpaymentchilds = _db.reasonfornonpaymentchilds.ToList();
                foreach (var item in crmVM._crmlst)
                {
                    if (item.callout_status_level1 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1);
                        if (_data != null)
                        {
                            item.callout_status_level1_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level2 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level2);
                        if (_data != null)
                        {
                            item.callout_status_level2_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level3 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level3);
                        if (_data != null)
                        {
                            item.callout_status_level3_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level4 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level4);
                        if (_data != null)
                        {
                            item.callout_status_level4_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level5 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level5);
                        if (_data != null)
                        {
                            item.callout_status_level5_Text = _data.disposition_name;
                        }
                    }
                    if (item.balance_for_collection > 0)
                    {
                        var _data = _data_balanceForCollections.FirstOrDefault(x => x.id == item.balance_for_collection);
                        if (_data != null)
                        {
                            item.Balance_For_Collection_Text = _data.balance_collection_name;
                        }
                    }
                    if (item.mode_of_settlement > 0)
                    {
                        var _data = _data_modeofsettlements.FirstOrDefault(x => x.id == item.mode_of_settlement);
                        if (_data != null)
                        {
                            item.Mode_of_settlement_Text = _data.modeofsettlement_name;
                        }
                    }
                    if (item.payment_channel > 0)
                    {
                        var _data = _data_paymentchannels.FirstOrDefault(x => x.id == item.payment_channel);
                        if (_data != null)
                        {
                            item.Payment_channel_Text = _data.payment_channel_name;
                        }
                    }
                    if (item.region_primary > 0)
                    {
                        var _data = _data_regionprimarys.FirstOrDefault(x => x.id == item.region_primary);
                        if (_data != null)
                        {
                            item.region_primary_text = _data.region_name;
                        }
                    }
                    if (item.region_secondary > 0)
                    {
                        var _data = _data_regionsecondarys.FirstOrDefault(x => x.id == item.region_secondary);
                        if (_data != null)
                        {
                            item.region_secondary_text = _data.region_secondary;
                        }
                    }
                    if (item.skip_trace > 0)
                    {
                        var _data = _data_skipstrace.FirstOrDefault(x => x.id == item.skip_trace);
                        if (_data != null)
                        {
                            item.Skip_trace_Text = _data.skip_trace_name;
                        }
                    }
                    if (item.skip_trace_child > 0)
                    {
                        var _data = _data_skiptracechilds.FirstOrDefault(x => x.id == item.skip_trace_child);
                        if (_data != null)
                        {
                            item.Child_Skip_trace_Text = _data.skip_trace_child;
                        }
                    }
                    if (item.status > 0)
                    {
                        var _data = _data_statuses.FirstOrDefault(x => x.id == item.status);
                        if (_data != null)
                        {
                            item.Status_Text = _data.status_name;
                        }
                    }
                    if (item.campaign_id > 0)
                    {
                        var _data = _data_campaigns.FirstOrDefault(x => x.campaign_id == item.campaign_id);
                        if (_data != null)
                        {
                            item.campaign_name = _data.campaign_name;
                        }
                    }
                    if (item.reason_for_non_payment_level1 > 0)
                    {
                        var _data = _data_reasonfornonpayments.FirstOrDefault(x => x.id == item.reason_for_non_payment_level1);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level1_Text = _data.reason_for_non_payment_name;
                        }
                    }
                    if (item.reason_for_non_payment_level2 > 0)
                    {
                        var _data = _data_reasonfornonpaymentchilds.FirstOrDefault(x => x.id == item.reason_for_non_payment_level2);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level2_Text = _data.reason_for_non_payment_child_name;
                        }
                    }
                    var Agent_name = (from crm in _db.leadassigns.Where(x => x.lead_id == item.crm_id)
                                      join user in _db.users on crm.agent_id equals user.user_id
                                      select new UserModel()
                                      {
                                          username = user.username,
                                          user_fullname = user.user_fullname
                                      }).ToList();

                    foreach (var Agent_nameitem in Agent_name)
                    {
                        item.Agent_name = Agent_nameitem.user_fullname;
                    }
                }

                return crmVM._crmlst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        public List<CRMVM> GetCrmDetails(int campaign_id)
        {
            CRMVM crmVM = new CRMVM();
            try
            {
                crmVM._crmlst = _db.crms.Select(obj => new CRMVM
                {
                    agent_id = obj.agent_id,
                    account_name = obj.account_name,
                    account_no = obj.account_no,
                    address = obj.address,
                    date_invoice = obj.date_invoice.HasValue == true ? obj.date_invoice : null,
                    amount_collected = obj.amount_collected,
                    authorized_representatives = obj.authorized_representatives,
                    age_group = obj.age_group,
                    balance_for_collection = obj.balance_for_collection,
                    phone1 = obj.phone1,
                    callout_status_level1 = obj.callout_status_level1,
                    callout_status_level2 = obj.callout_status_level2,
                    callout_status_level3 = obj.callout_status_level3,
                    callout_status_level4 = obj.callout_status_level4,
                    callout_status_level5 = obj.callout_status_level5,
                    reason_for_non_payment_level1 = obj.reason_for_non_payment_level1,
                    reason_for_non_payment_level2 = obj.reason_for_non_payment_level2,
                    callout_status_level1_Text = "",
                    callout_status_level2_Text = "",
                    callout_status_level3_Text = "",
                    callout_status_level4_Text = "",
                    callout_status_level5_Text = "",
                    Balance_For_Collection_Text = "",
                    Mode_of_settlement_Text = "",
                    Payment_channel_Text = "",
                    region_primary_text = "",
                    region_secondary_text = "",
                    Skip_trace_Text = "",
                    Child_Skip_trace_Text = "",
                    Status_Text = "",
                    city = obj.city,
                    campaign_id = obj.campaign_id,
                    campaign_name = "",
                    company_name = obj.company_name,
                    count_of_attempted_contact_numbers = obj.count_of_attempted_contact_numbers,
                    createdby = obj.createdby,
                    crm_id = obj.crm_id,
                    email = obj.email,
                    financial_contact_person = obj.financial_contact_person,
                    give_new_contact_no = obj.give_new_contact_no,
                    invoice_amount = obj.invoice_amount,
                    invoice_no = obj.invoice_no,
                    mode_of_settlement = obj.mode_of_settlement,
                    number_of_accounts = obj.number_of_accounts,
                    outstanding_balance = obj.outstanding_balance,
                    overdue_balance = obj.overdue_balance,
                    phone_extension = obj.phone_extension,
                    payment_channel = obj.payment_channel,
                    phone2 = obj.phone2,
                    phone3 = obj.phone3,
                    phone4 = obj.phone4,
                    ptp_amount = obj.ptp_amount,
                    ptp_date = obj.ptp_date.HasValue == true ? obj.ptp_date : null,
                    ptp_dated_by = obj.ptp_dated_by,
                    recommendation = obj.recommendation,
                    region_primary = obj.region_primary,
                    region_secondary = obj.region_secondary,
                    skip_trace_child = obj.skip_trace_child,
                    service_id_no = obj.service_id_no,
                    service_type = obj.service_type,
                    skip_trace = obj.skip_trace,
                    status = obj.status,
                    supervisor_id = obj.supervisor_id,
                    createdbyname = obj.createdbyname,
                    createdon = obj.createdon,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    isactive = true,
                    remarks = obj.remarks,
                    list_name = obj.list_name,
                    endorsement_id = obj.endorsement_id,
                    enable_get_lead = obj.enable_get_lead,
                    dailer_disposition = obj.dailer_disposition,
                    last_called_phoneno = obj.last_called_phoneno

                }).Where(x => x.campaign_id == campaign_id).ToList();

                var _data_dispositions = _db.dispositions.ToList();
                var _data_balanceForCollections = _db.balanceForCollections.ToList();
                var _data_modeofsettlements = _db.modeofsettlements.ToList();
                var _data_paymentchannels = _db.paymentchannels.ToList();
                var _data_regionprimarys = _db.regionprimarys.ToList();
                var _data_regionsecondarys = _db.regionsecondarys.ToList();
                var _data_skipstrace = _db.skipstrace.ToList();
                var _data_skiptracechilds = _db.skiptracechilds.ToList();
                var _data_statuses = _db.statuses.ToList();
                var _data_campaigns = _db.campaigns.ToList();
                var _data_reasonfornonpayments = _db.reasonfornonpayments.ToList();
                var _data_reasonfornonpaymentchilds = _db.reasonfornonpaymentchilds.ToList();

                foreach (var item in crmVM._crmlst)
                {
                    if (item.callout_status_level1 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1);
                        if (_data != null)
                        {
                            item.callout_status_level1_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level2 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level2);
                        if (_data != null)
                        {
                            item.callout_status_level2_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level3 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level3);
                        if (_data != null)
                        {
                            item.callout_status_level3_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level4 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level4);
                        if (_data != null)
                        {
                            item.callout_status_level4_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level5 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level5);
                        if (_data != null)
                        {
                            item.callout_status_level5_Text = _data.disposition_name;
                        }
                    }
                    if (item.balance_for_collection > 0)
                    {
                        var _data = _data_balanceForCollections.FirstOrDefault(x => x.id == item.balance_for_collection);
                        if (_data != null)
                        {
                            item.Balance_For_Collection_Text = _data.balance_collection_name;
                        }
                    }
                    if (item.mode_of_settlement > 0)
                    {
                        var _data = _data_modeofsettlements.FirstOrDefault(x => x.id == item.mode_of_settlement);
                        if (_data != null)
                        {
                            item.Mode_of_settlement_Text = _data.modeofsettlement_name;
                        }
                    }
                    if (item.payment_channel > 0)
                    {
                        var _data = _data_paymentchannels.FirstOrDefault(x => x.id == item.payment_channel);
                        if (_data != null)
                        {
                            item.Payment_channel_Text = _data.payment_channel_name;
                        }
                    }
                    if (item.region_primary > 0)
                    {
                        var _data = _data_regionprimarys.FirstOrDefault(x => x.id == item.region_primary);
                        if (_data != null)
                        {
                            item.region_primary_text = _data.region_name;
                        }
                    }
                    if (item.region_secondary > 0)
                    {
                        var _data = _data_regionsecondarys.FirstOrDefault(x => x.id == item.region_secondary);
                        if (_data != null)
                        {
                            item.region_secondary_text = _data.region_secondary;
                        }
                    }
                    if (item.skip_trace > 0)
                    {
                        var _data = _data_skipstrace.FirstOrDefault(x => x.id == item.skip_trace);
                        if (_data != null)
                        {
                            item.Skip_trace_Text = _data.skip_trace_name;
                        }
                    }
                    if (item.skip_trace_child > 0)
                    {
                        var _data = _data_skiptracechilds.FirstOrDefault(x => x.id == item.skip_trace_child);
                        if (_data != null)
                        {
                            item.Child_Skip_trace_Text = _data.skip_trace_child;
                        }
                    }
                    if (item.status > 0)
                    {
                        var _data = _data_statuses.FirstOrDefault(x => x.id == item.status);
                        if (_data != null)
                        {
                            item.Status_Text = _data.status_name;
                        }
                    }
                    if (item.campaign_id > 0)
                    {
                        var _data = _data_campaigns.FirstOrDefault(x => x.campaign_id == item.campaign_id);
                        if (_data != null)
                        {
                            item.campaign_name = _data.campaign_name;
                        }
                    }
                    if (item.reason_for_non_payment_level1 > 0)
                    {
                        var _data = _data_reasonfornonpayments.FirstOrDefault(x => x.id == item.reason_for_non_payment_level1);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level1_Text = _data.reason_for_non_payment_name;
                        }
                    }
                    if (item.reason_for_non_payment_level2 > 0)
                    {
                        var _data = _data_reasonfornonpaymentchilds.FirstOrDefault(x => x.id == item.reason_for_non_payment_level2);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level2_Text = _data.reason_for_non_payment_child_name;
                        }
                    }
                    var Agent_name = (from crm in _db.leadassigns.Where(x => x.lead_id == item.crm_id)
                                      join user in _db.users on crm.agent_id equals user.user_id
                                      select new UserModel()
                                      {
                                          username = user.username,
                                          user_fullname = user.user_fullname
                                      }).ToList();

                    foreach (var Agent_nameitem in Agent_name)
                    {
                        item.Agent_name = Agent_nameitem.user_fullname;
                    }
                }


                return crmVM._crmlst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        public List<CRMDATAVM> DownloadCrmDetails(int campaign_id, string ListNameValue, DateTime FromDate, DateTime TODate)
        {
            CRMVM crmVM = new CRMVM();
            try
            {
                crmVM._crmlst = _db.crms.Select(obj => new CRMVM
                {
                    agent_id = obj.agent_id,
                    account_name = obj.account_name,
                    account_no = obj.account_no,
                    address = obj.address,
                    date_invoice = obj.date_invoice.HasValue == true ? obj.date_invoice : null,
                    amount_collected = obj.amount_collected,
                    authorized_representatives = obj.authorized_representatives,
                    age_group = obj.age_group,
                    balance_for_collection = obj.balance_for_collection,
                    phone1 = obj.phone1,
                    callout_status_level1 = obj.callout_status_level1,
                    callout_status_level2 = obj.callout_status_level2,
                    callout_status_level3 = obj.callout_status_level3,
                    callout_status_level4 = obj.callout_status_level4,
                    callout_status_level5 = obj.callout_status_level5,
                    reason_for_non_payment_level1 = obj.reason_for_non_payment_level1,
                    reason_for_non_payment_level2 = obj.reason_for_non_payment_level2,
                    callout_status_level1_Text = "",
                    callout_status_level2_Text = "",
                    callout_status_level3_Text = "",
                    callout_status_level4_Text = "",
                    callout_status_level5_Text = "",
                    Balance_For_Collection_Text = "",
                    Mode_of_settlement_Text = "",
                    Payment_channel_Text = "",
                    region_primary_text = "",
                    region_secondary_text = "",
                    Skip_trace_Text = "",
                    Child_Skip_trace_Text = "",
                    Status_Text = "",
                    city = obj.city,
                    campaign_id = obj.campaign_id,
                    campaign_name = "",
                    company_name = obj.company_name,
                    count_of_attempted_contact_numbers = obj.count_of_attempted_contact_numbers,
                    createdby = obj.createdby,
                    crm_id = obj.crm_id,
                    email = obj.email,
                    financial_contact_person = obj.financial_contact_person,
                    give_new_contact_no = obj.give_new_contact_no,
                    invoice_amount = obj.invoice_amount,
                    invoice_no = obj.invoice_no,
                    mode_of_settlement = obj.mode_of_settlement,
                    number_of_accounts = obj.number_of_accounts,
                    outstanding_balance = obj.outstanding_balance,
                    overdue_balance = obj.overdue_balance,
                    phone_extension = obj.phone_extension,
                    payment_channel = obj.payment_channel,
                    phone2 = obj.phone2,
                    phone3 = obj.phone3,
                    phone4 = obj.phone4,
                    ptp_amount = obj.ptp_amount,
                    ptp_date = obj.ptp_date.HasValue == true ? obj.ptp_date : null,
                    ptp_dated_by = obj.ptp_dated_by,
                    recommendation = obj.recommendation,
                    region_primary = obj.region_primary,
                    region_secondary = obj.region_secondary,
                    skip_trace_child = obj.skip_trace_child,
                    service_id_no = obj.service_id_no,
                    service_type = obj.service_type,
                    skip_trace = obj.skip_trace,
                    status = obj.status,
                    supervisor_id = obj.supervisor_id,
                    createdbyname = obj.createdbyname,
                    createdon = obj.createdon,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    isactive = true,
                    remarks = obj.remarks,
                    list_name = obj.list_name,
                    endorsement_id = obj.endorsement_id,
                    enable_get_lead = obj.enable_get_lead,
                    dailer_disposition = obj.dailer_disposition,
                    last_called_phoneno = obj.last_called_phoneno
                }).Where(x => x.campaign_id == campaign_id && x.list_name == ListNameValue && x.updatedon.Date >= FromDate.Date && x.updatedon.Date <= TODate.Date).ToList();

                var _data_dispositions = _db.dispositions.ToList();
                var _data_balanceForCollections = _db.balanceForCollections.ToList();
                var _data_modeofsettlements = _db.modeofsettlements.ToList();
                var _data_paymentchannels = _db.paymentchannels.ToList();
                var _data_regionprimarys = _db.regionprimarys.ToList();
                var _data_regionsecondarys = _db.regionsecondarys.ToList();
                var _data_skipstrace = _db.skipstrace.ToList();
                var _data_skiptracechilds = _db.skiptracechilds.ToList();
                var _data_statuses = _db.statuses.ToList();
                var _data_campaigns = _db.campaigns.ToList();
                var _data_reasonfornonpayments = _db.reasonfornonpayments.ToList();
                var _data_reasonfornonpaymentchilds = _db.reasonfornonpaymentchilds.ToList();

                foreach (var item in crmVM._crmlst)
                {
                    if (item.callout_status_level1 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1);
                        if (_data != null)
                        {
                            item.callout_status_level1_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level2 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level2);
                        if (_data != null)
                        {
                            item.callout_status_level2_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level3 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level3);
                        if (_data != null)
                        {
                            item.callout_status_level3_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level4 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level4);
                        if (_data != null)
                        {
                            item.callout_status_level4_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level5 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level5);
                        if (_data != null)
                        {
                            item.callout_status_level5_Text = _data.disposition_name;
                        }
                    }
                    if (item.balance_for_collection > 0)
                    {
                        var _data = _data_balanceForCollections.FirstOrDefault(x => x.id == item.balance_for_collection);
                        if (_data != null)
                        {
                            item.Balance_For_Collection_Text = _data.balance_collection_name;
                        }
                    }
                    if (item.mode_of_settlement > 0)
                    {
                        var _data = _data_modeofsettlements.FirstOrDefault(x => x.id == item.mode_of_settlement);
                        if (_data != null)
                        {
                            item.Mode_of_settlement_Text = _data.modeofsettlement_name;
                        }
                    }
                    if (item.payment_channel > 0)
                    {
                        var _data = _data_paymentchannels.FirstOrDefault(x => x.id == item.payment_channel);
                        if (_data != null)
                        {
                            item.Payment_channel_Text = _data.payment_channel_name;
                        }
                    }
                    if (item.region_primary > 0)
                    {
                        var _data = _data_regionprimarys.FirstOrDefault(x => x.id == item.region_primary);
                        if (_data != null)
                        {
                            item.region_primary_text = _data.region_name;
                        }
                    }
                    if (item.region_secondary > 0)
                    {
                        var _data = _data_regionsecondarys.FirstOrDefault(x => x.id == item.region_secondary);
                        if (_data != null)
                        {
                            item.region_secondary_text = _data.region_secondary;
                        }
                    }
                    if (item.skip_trace > 0)
                    {
                        var _data = _data_skipstrace.FirstOrDefault(x => x.id == item.skip_trace);
                        if (_data != null)
                        {
                            item.Skip_trace_Text = _data.skip_trace_name;
                        }
                    }
                    if (item.skip_trace_child > 0)
                    {
                        var _data = _data_skiptracechilds.FirstOrDefault(x => x.id == item.skip_trace_child);
                        if (_data != null)
                        {
                            item.Child_Skip_trace_Text = _data.skip_trace_child;
                        }
                    }
                    if (item.status > 0)
                    {
                        var _data = _data_statuses.FirstOrDefault(x => x.id == item.status);
                        if (_data != null)
                        {
                            item.Status_Text = _data.status_name;
                        }
                    }
                    if (item.campaign_id > 0)
                    {
                        var _data = _data_campaigns.FirstOrDefault(x => x.campaign_id == item.campaign_id);
                        if (_data != null)
                        {
                            item.campaign_name = _data.campaign_name;
                        }
                    }
                    if (item.reason_for_non_payment_level1 > 0)
                    {
                        var _data = _data_reasonfornonpayments.FirstOrDefault(x => x.id == item.reason_for_non_payment_level1);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level1_Text = _data.reason_for_non_payment_name;
                        }
                    }
                    if (item.reason_for_non_payment_level2 > 0)
                    {
                        var _data = _data_reasonfornonpaymentchilds.FirstOrDefault(x => x.id == item.reason_for_non_payment_level2);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level2_Text = _data.reason_for_non_payment_child_name;
                        }
                    }
                    var Agent_name = (from crm in _db.remarks.Where(x => x.lead_id == item.crm_id && x.remark == item.remarks)
                                      join user in _db.users on crm.createdby equals user.user_id
                                      select new UserModel()
                                      {
                                          username = user.username,
                                          user_fullname = user.user_fullname
                                      }).ToList();

                    foreach (var Agent_nameitem in Agent_name)
                    {
                        item.Agent_name = Agent_nameitem.user_fullname;
                    }
                }
                foreach (var item in crmVM._crmlst)
                {
                    crmVM.cRMDATAVMs.Add(new CRMDATAVM()
                    {
                        Account_Name = item.account_name,
                        Account_No = item.account_no,
                        Address = item.address,
                        Date_Invoice = item.date_invoice.HasValue == true ? item.date_invoice : null,
                        Amount_Collected = item.amount_collected,
                        Authorized_Representatives = item.authorized_representatives,
                        Age_Group = item.age_group,
                        Phone1 = item.phone1,
                        Callout_Status_Level1 = item.callout_status_level1_Text,
                        Callout_Status_Level2 = item.callout_status_level2_Text,
                        Callout_Status_Level3 = item.callout_status_level3_Text,
                        Callout_Status_Level4 = item.callout_status_level4_Text,
                        Callout_Status_Level5 = item.callout_status_level5_Text,
                        Balance_For_Collection = item.Balance_For_Collection_Text,
                        Mode_of_Settlement = item.Mode_of_settlement_Text,
                        Payment_Channel = item.Payment_channel_Text,
                        Region_Primary = item.region_primary_text,
                        Region_Secondary = item.region_secondary_text,
                        Skip_Trace_Level1 = item.Skip_trace_Text,
                        Skip_Trace_Level2 = item.Child_Skip_trace_Text,
                        Status_Text = item.Status_Text,
                        City = item.city,
                        Campaign_Name = item.campaign_name,
                        Company_Name = item.company_name,
                        Count_of_Attempted = item.count_of_attempted_contact_numbers,
                        Onet_Id = "ONET-" + Convert.ToString(item.crm_id),
                        Email = item.email,
                        Financial_Contact_Person = item.financial_contact_person,
                        New_Contact_No = item.give_new_contact_no,
                        Invoice_Amount = item.invoice_amount,
                        Invoice_No = item.invoice_no,
                        Number_of_Accounts = item.number_of_accounts,
                        Outstanding_Balance = item.outstanding_balance,
                        Overdue_Balance = item.overdue_balance,
                        Phone_Extension = item.phone_extension,
                        Phone2 = item.phone2,
                        Phone3 = item.phone3,
                        Phone4 = item.phone4,
                        PTP_Amount = item.ptp_amount,
                        PTP_Date = item.ptp_date.HasValue == true ? item.ptp_date : null,
                        PTP_Dated_By = item.ptp_dated_by,
                        Recommendation = item.recommendation,
                        Service_Id = item.service_id_no,
                        Service_Type = item.service_type,
                        Created_On = item.createdon,
                        Updated_On = item.updatedon,
                        Remarks = item.remarks,
                        List_Name = item.list_name,
                        Activity_Name = item.activity_name,
                        Activity_Status = item.activity_status,
                        Activity_Summary = item.activity_summary,
                        Agent_Name = item.Agent_name,
                        Reason_for_Non_Payment_Level1 = item.reason_for_non_payment_level1_Text,
                        Reason_for_Non_Payment_Level2 = item.reason_for_non_payment_level2_Text,
                        Updated_Count = item.updatedtimes,
                        dailer_disposition = item.dailer_disposition,
                        last_called_phoneno = item.last_called_phoneno

                    });
                }

                return crmVM.cRMDATAVMs;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get CallBacks Details List
        /// </summary>
        /// <returns></returns>
        public List<CRMVM> GetCallBacksDetails()
        {
            CRMVM crmModel = new CRMVM();
            try
            {
                crmModel._crmlst = (from crm in _db.crms
                                    join callback in _db.callBacks.Where(x => x.isattempted == false && x.callback_date_time.Date == DateTime.Now.Date) on crm.crm_id equals callback.lead_id
                                    select new CRMVM()
                                    {
                                        account_name = crm.account_name,
                                        account_no = crm.account_no,
                                        address = crm.address,
                                        agent_id = crm.agent_id,
                                        age_group = crm.age_group,
                                        amount_collected = crm.amount_collected,
                                        authorized_representatives = crm.authorized_representatives,
                                        balance_for_collection = crm.balance_for_collection,
                                        callout_status_level1 = crm.callout_status_level1,
                                        callout_status_level2 = crm.callout_status_level2,
                                        callout_status_level3 = crm.callout_status_level3,
                                        city = crm.city,
                                        company_name = crm.company_name,
                                        phone1 = crm.phone1,
                                        count_of_attempted_contact_numbers = crm.count_of_attempted_contact_numbers,
                                        createdby = crm.createdby,
                                        createdbyname = crm.createdbyname,
                                        createdon = crm.createdon,
                                        crm_id = crm.crm_id,
                                        date_invoice = crm.date_invoice.HasValue == true ? crm.date_invoice : null,
                                        email = crm.email,
                                        financial_contact_person = crm.financial_contact_person,
                                        give_new_contact_no = crm.give_new_contact_no,
                                        invoice_amount = crm.invoice_amount,
                                        invoice_no = crm.invoice_no,
                                        isactive = true,
                                        reason_for_non_payment_level1 = crm.reason_for_non_payment_level1,
                                        reason_for_non_payment_level2 = crm.reason_for_non_payment_level2,
                                        mode_of_settlement = crm.mode_of_settlement,
                                        number_of_accounts = crm.number_of_accounts,
                                        outstanding_balance = crm.outstanding_balance,
                                        overdue_balance = crm.overdue_balance,
                                        payment_channel = crm.payment_channel,
                                        phone2 = crm.phone2,
                                        phone3 = crm.phone3,
                                        phone4 = crm.phone4,
                                        phone_extension = crm.phone_extension,
                                        ptp_amount = crm.ptp_amount,
                                        ptp_date = crm.ptp_date.HasValue == true ? crm.ptp_date : null,
                                        ptp_dated_by = crm.ptp_dated_by,
                                        recommendation = crm.recommendation,
                                        region_primary = crm.region_primary,
                                        region_secondary = crm.region_secondary,
                                        skip_trace_child = crm.skip_trace_child,
                                        service_id_no = crm.service_id_no,
                                        service_type = crm.service_type,
                                        skip_trace = crm.skip_trace,
                                        status = crm.status,
                                        supervisor_id = crm.supervisor_id,
                                        updatedby = crm.updatedby,
                                        updatedbyname = crm.updatedbyname,
                                        updatedon = crm.updatedon,
                                        select_callback = 1,
                                        callback_date_time = callback.callback_date_time

                                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return crmModel._crmlst;
        }

        /// <summary>
        /// GET CRM BY MOBILE NO
        /// </summary>
        /// <param name="MobileNo"></param>
        /// <returns></returns>

        public CRMModel GetCrmByMobileNo(string MobileNo)
        {
            return _db.crms.FirstOrDefault(x => (x.phone1 == MobileNo) || (x.phone2 == MobileNo) || (x.phone3 == MobileNo) || (x.phone4 == MobileNo) || (x.give_new_contact_no == MobileNo));
        }
        /// <summary>
        /// get city
        /// </summary>
        /// <returns></returns>
        public List<CityModel> GetAllCity()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.cityModels = _db.cities.ToList();
                return model.cityModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get All Skip Trace
        /// </summary>
        /// <returns></returns>
        public List<SkipTraceModel> GetAllSkipTrace()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.skipTraceModels = _db.skipstrace.ToList();
                return model.skipTraceModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        ///  Get All Status
        /// </summary>
        /// <returns></returns>
        public List<StatusModel> GetAllStatus()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.statusModels = _db.statuses.ToList();
                return model.statusModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get All Region
        /// </summary>
        /// <returns></returns>
        public List<RegionModel> GetAllRegion()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.regionModel = _db.regionprimarys.ToList();
                return model.regionModel;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get All ModeOfSettlement
        /// </summary>
        /// <returns></returns>
        public List<ModeOfSettlementModel> GetAllModeOfSettlement()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.modeOfSettlementModels = _db.modeofsettlements.ToList();
                return model.modeOfSettlementModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get All PayementChannel
        /// </summary>
        /// <returns></returns>
        public List<PaymentChannelModel> GetAllPayementChannel()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.paymentChannelModels = _db.paymentchannels.ToList();
                return model.paymentChannelModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get All BalanceForCollection
        /// </summary>
        /// <returns></returns>
        public List<BalanceForCollectionModel> GetAllBalanceForCollection()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.balanceForCollectionModels = _db.balanceForCollections.ToList();
                return model.balanceForCollectionModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }

        /// <summary>
        /// Get All ReasonForNonPayment
        /// </summary>
        /// <returns></returns>
        public List<ReasonForNonPaymentModel> GetAllReasonForNonPayment()
        {
            CRMVM model = new CRMVM();
            try
            {
                model.reasonForNonPaymentModels = _db.reasonfornonpayments.ToList();
                return model.reasonForNonPaymentModels;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get RegionSecondary
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<RegionSecondaryModel> GetRegionSecondary(int id)
        {
            try
            {

                return _db.regionsecondarys.Where(x => x.parent_region_id == id).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get ReasonForNonPayementChild
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<ReasonForNonPayementChildModel> GetReasonForNonPayementChildModel(int id)
        {
            try
            {

                return _db.reasonfornonpaymentchilds.Where(x => x.reason_for_non_payment_parent == id).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get SkipTraceChild
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<SkipTraceChildModel> GetSkipTraceChildModel(int id)
        {
            try
            {

                return _db.skiptracechilds.Where(x => x.parent_skip_trace == id).ToList(); ;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// AddActivityDetails
        /// </summary>
        /// <param name="activityModel"></param>
        /// <returns></returns>
        public bool AddActivityDetails(CRMVM activityModel)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    ActivityHistoryModel Activity_obj = new ActivityHistoryModel();
                    {
                        Activity_obj.activity_name = activityModel.activity_name;
                        Activity_obj.time_stamp = DateTime.Now;
                        Activity_obj.status = activityModel.activity_status;
                        Activity_obj.summary = activityModel.activity_summary;
                        Activity_obj.lead_id = activityModel.crm_id;
                        Activity_obj.isactive = true;
                        Activity_obj.createdon = DateTime.Now;
                        Activity_obj.createdby = activityModel.createdby;
                        Activity_obj.updatedon = DateTime.Now;
                        Activity_obj.updatedby = activityModel.updatedby;
                        Activity_obj.createdbyname = activityModel.createdbyname;
                        Activity_obj.updatedbyname = activityModel.updatedbyname;

                    };
                    _db.activityhistories.Add(Activity_obj);
                    _db.SaveChanges();
                    scope.Complete();
                    scope.Dispose();
                    return true;
                }
                catch (Exception ex)
                {
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return false;
                }
            }
        }

        public bool EditPaymentField(CRMVM crmpayment)
        {
            try
            {
                CRMModel _data = new CRMModel();
                if (_data == null)
                {
                    _data = new CRMModel();
                }

                if (_data.crm_id == 0 && !String.IsNullOrWhiteSpace(crmpayment.invoice_no))
                {
                    _data = _db.crms.FirstOrDefault(x => (x.invoice_no == crmpayment.invoice_no) && x.campaign_id == crmpayment.campaign_id);
                }
                if (_data == null)
                {
                    _data = new CRMModel();
                }

                if (_data.crm_id == 0 && !String.IsNullOrWhiteSpace(crmpayment.account_no))
                {
                    _data = _db.crms.FirstOrDefault(x => (x.account_no == crmpayment.account_no) && x.campaign_id == crmpayment.campaign_id);
                }
                if (_data == null)
                {
                    _data = new CRMModel();
                }

                if (_data.crm_id == 0 && !String.IsNullOrWhiteSpace(crmpayment.account_name))
                {
                    _data = _db.crms.FirstOrDefault(x => (x.account_name == crmpayment.account_name) && x.campaign_id == crmpayment.campaign_id);
                }

                if (_data != null && _data.crm_id != 0)
                {
                    _data.amount_collected = crmpayment.amount_collected;
                    _data.updatedon = DateTime.Now;
                    _data.updatedby = crmpayment.updatedby;
                    _data.updatedbyname = crmpayment.updatedbyname;
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

        public int SaveModeofSettlement(string str)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    ModeOfSettlementModel mode = new ModeOfSettlementModel();
                    mode.modeofsettlement_name = str;
                    mode.createdby = 1;
                    mode.createdbyname = "admin";
                    mode.createdon = DateTime.Now;
                    mode.isactive = true;
                    _db.modeofsettlements.Add(mode);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = mode.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int SaveStatus(string str)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    StatusModel status = new StatusModel();
                    status.status_name = str;
                    status.createdby = 1;
                    status.createdbyname = "admin";
                    status.createdon = DateTime.Now;
                    status.isactive = true;
                    _db.statuses.Add(status);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = status.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int SaveRegionPrimary(string str)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    RegionModel primary = new RegionModel();
                    primary.region_name = str;
                    primary.createdby = 1;
                    primary.createdbyname = "admin";
                    primary.createdon = DateTime.Now;
                    primary.isactive = true;
                    _db.regionprimarys.Add(primary);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = primary.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int SaveRegionSecondary(string primary, string secondary)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var primary_id = _db.regionprimarys.FirstOrDefault(x => x.region_name.ToLower().Trim() == primary.ToLower().Trim()).id;
                    RegionSecondaryModel model = new RegionSecondaryModel();
                    model.region_secondary = secondary;
                    model.parent_region_id = primary_id;
                    model.createdby = 1;
                    model.createdbyname = "admin";
                    model.createdon = DateTime.Now;
                    model.isactive = true;
                    _db.regionsecondarys.Add(model);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = model.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int SaveSkipTracer(string str)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    SkipTraceModel skip = new SkipTraceModel();
                    skip.skip_trace_name = str;
                    skip.createdby = 1;
                    skip.createdbyname = "admin";
                    skip.createdon = DateTime.Now;
                    skip.isactive = true;
                    _db.skipstrace.Add(skip);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = skip.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int SaveSkipTracerChild(string primary, string secondary)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var primary_id = _db.skipstrace.FirstOrDefault(x => x.skip_trace_name.ToLower().Trim() == primary.ToLower().Trim()).id;
                    SkipTraceChildModel model = new SkipTraceChildModel();
                    model.skip_trace_child = secondary;
                    model.parent_skip_trace = primary_id;
                    model.createdby = 1;
                    model.createdbyname = "admin";
                    model.createdon = DateTime.Now;
                    model.isactive = true;
                    _db.skiptracechilds.Add(model);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = model.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0; ;
                }

            }
        }

        public int SaveCallOutStatus(string str)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    DispositionModel reason = new DispositionModel();
                    reason.disposition_name = str;
                    reason.short_name = str;
                    reason.parent_disposition = 0;
                    reason.createdby = 1;
                    reason.createdbyname = "admin";
                    reason.createdon = DateTime.Now;
                    reason.isactive = true;
                    _db.dispositions.Add(reason);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = reason.disposition_id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0; ;
                }

            }
        }

        public int SaveCallOutStatusChild(string primary, string secondary)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var primary_id = _db.dispositions.FirstOrDefault(x => x.disposition_name.ToLower().Trim() == primary.ToLower().Trim()).disposition_id;
                    DispositionModel reason = new DispositionModel();
                    reason.disposition_name = secondary;
                    reason.short_name = secondary;
                    reason.parent_disposition = primary_id;
                    reason.createdby = 1;
                    reason.createdbyname = "admin";
                    reason.createdon = DateTime.Now;
                    reason.isactive = true;
                    _db.dispositions.Add(reason);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = reason.disposition_id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int SaveBalanceForCollections(string str)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                try
                {
                    BalanceForCollectionModel skip = new BalanceForCollectionModel();
                    skip.balance_collection_name = str;
                    skip.createdby = 1;
                    skip.createdbyname = "admin";
                    skip.createdon = DateTime.Now;
                    skip.isactive = true;
                    _db.balanceForCollections.Add(skip);
                    _db.SaveChanges();
                    scope.Complete();
                    int id = skip.id;
                    return id;
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                    return 0;
                }

            }
        }

        public int GetModeOfSettlementId(string str)
        {
            var data = _db.modeofsettlements.FirstOrDefault(x => x.modeofsettlement_name.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public int GetStatusId(string str)
        {

            var data = _db.statuses.FirstOrDefault(x => x.status_name.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public int GetRegionPrimaryId(string str)
        {
            var data = _db.regionprimarys.FirstOrDefault(x => x.region_name.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public int GetRegionSecondaryId(string str)
        {
            var data = _db.regionsecondarys.FirstOrDefault(x => x.region_secondary.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public int GetSkipTracerId(string str)
        {
            var data = _db.skipstrace.FirstOrDefault(x => x.skip_trace_name.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public int GetSkipTracerChildId(string str)
        {
            var data = _db.skiptracechilds.FirstOrDefault(x => x.skip_trace_child.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public int GetCalloutstatusId(string str)
        {
            var data = _db.dispositions.FirstOrDefault(x => x.disposition_name.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.disposition_id;
            }
            else
            {
                return 0;
            }
        }
        public int GetBalanceForCollectionsId(string str)
        {
            var data = _db.balanceForCollections.FirstOrDefault(x => x.balance_collection_name.ToLower().Trim() == str.ToLower().Trim());
            if (data != null)
            {
                return data.id;
            }
            else
            {
                return 0;
            }
        }
        public CRMModel GetCrmByMobileNoAndCamp(string MobileNo, int campaign)
        {
            return _db.crms.FirstOrDefault(x => ((x.phone1 == MobileNo) || (x.phone2 == MobileNo) || (x.phone3 == MobileNo) || (x.phone4 == MobileNo) || (x.give_new_contact_no == MobileNo)) && x.campaign_id == campaign);
        }
    }
}

