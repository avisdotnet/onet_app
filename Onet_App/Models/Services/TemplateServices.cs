﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;


namespace Onet_App.Models.Services
{

    public class TemplateServices : ITemplate
    {
        private readonly ApplicationDbContext _db;

        public TemplateServices(ApplicationDbContext db)
        {
            _db = db;
        }
        /// <summary>
        /// Function for template creation
        /// </summary>
        /// <param name="templateVM"></param>
        /// <returns></returns>
       public bool SaveTemplateCreation(TemplateVM templateVM)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        TemplateModel temp_obj = new TemplateModel
                        {
                            template_name = templateVM.template_name,
                            template_areaname = templateVM.template_areaname,
                            template_controller_name = templateVM.template_controller_name,
                            template_action_name = templateVM.template_action_name,
                            template_query_string = templateVM.template_query_string,
                            isactive = templateVM.isactive,
                            createdon = DateTime.Now,
                            createdbyname = templateVM.createdbyname,
                            createdby = templateVM.createdby,
                            updatedby = templateVM.updatedby,
                            updatedbyname = templateVM.updatedbyname,
                            updatedon = DateTime.Now
                        };
                        temp_obj.isactive = true;
                        _db.templates.Add(temp_obj);
                        _db.SaveChanges();
                        scope.Complete();
                        scope.Dispose();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
        /// <summary>
        /// Function to get Template Details
        /// </summary>
        /// <param name="TemplateID"></param>
        /// <returns></returns>
       public TemplateVM GetTemplateID(int id)
        {
            TemplateVM model = new TemplateVM();
            try
            {
                var obj = _db.templates.FirstOrDefault(t => t.id ==model.id);
                if (obj == null)
                    throw new NotImplementedException();
                else
                {
                    return model;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Function for allactive template
        /// </summary>
        /// <returns></returns>
       public List<TemplateVM> AllActiveTemplate()
        {
            TemplateVM model = new TemplateVM();
            try
            {
                model._tempList = _db.templates
                    .Select(obj => new TemplateVM
                    {
                        template_name = obj.template_name,
                        template_areaname = obj.template_areaname,
                        template_controller_name = obj.template_controller_name,
                        template_action_name = obj.template_action_name,
                        template_query_string = obj.template_query_string,
                        updatedby = obj.updatedby,
                        createdby = obj.createdby,
                        updatedbyname = obj.template_controller_name,
                        createdbyname = obj.createdbyname,
                        updatedon = DateTime.Now,
                        createdon = obj.createdon,
                        isactive = true
                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return model._tempList;
        }
    }
}
