﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class EmailTemplateServices : IEmailTemplateServices
    {
        private readonly ApplicationDbContext _db;
        public EmailTemplateServices(ApplicationDbContext db) => _db = db;
        /// <summary>
        /// GetSMSTemplateList
        /// </summary>
        /// <returns></returns>
        public List<EmailTemplateVM> GetEmailTemplateList()
        {
            EmailTemplateVM email_temp = new EmailTemplateVM();
            try
            {
                email_temp._emailtemplatelst = _db.email_template.Where(x => x.isactive == true).Select(
                x => new EmailTemplateVM
                {
                    id = x.id,
                    email_template_definition = x.email_template_definition,
                    email_template_name = x.email_template_name,
                    email_template_description = x.email_template_description,
                    isactive = x.isactive,
                    createdby = x.createdby,
                    createdbyname = x.createdbyname,
                    createdon = x.createdon,
                    updatedby = x.updatedby,
                    updatedbyname = x.updatedbyname,
                    updatedon = x.updatedon
                }).ToList();

                return email_temp._emailtemplatelst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Get EmailTemplateById
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public EmailTemplateVM GetEmailTemplateById(int ID)
        {
            EmailTemplateVM emailtemp_obj = new EmailTemplateVM();
            try
            {
                var Data = _db.email_template.FirstOrDefault(x => x.id == ID);
                if (Data != null)
                {
                    emailtemp_obj.id = Data.id;
                    emailtemp_obj.email_template_description = Data.email_template_description;
                    emailtemp_obj.email_template_definition = Data.email_template_definition;
                    emailtemp_obj.email_template_name = Data.email_template_name;
                    emailtemp_obj.isactive = Data.isactive;
                    emailtemp_obj.createdby = Data.createdby;
                    emailtemp_obj.createdbyname = Data.createdbyname;
                    emailtemp_obj.createdon = Data.createdon;
                    emailtemp_obj.updatedby = Data.updatedby;
                    emailtemp_obj.updatedbyname = Data.updatedbyname;
                    emailtemp_obj.updatedon = Data.updatedon;
                }
                return emailtemp_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// Add EmailTemplate
        /// </summary>
        /// <param name="emailVm"></param>
        /// <returns></returns>
        public bool AddEmailTemplate(EmailTemplateVM emailVm)
        {
            try
            {
                bool bool_sameNameExists = _db.email_template.Any(x => x.email_template_definition.ToLower().Replace('#', ' ').Trim() == emailVm.email_template_definition.ToLower().Replace('#', ' ').Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            EmailTemplateCreationModel emailtemp_obj = new EmailTemplateCreationModel();
                            {

                                emailtemp_obj.email_template_name = emailVm.email_template_name;
                                emailtemp_obj.email_template_description = emailVm.email_template_description;
                                emailtemp_obj.email_template_definition = emailVm.email_template_definition;
                                emailtemp_obj.id = emailVm.id;
                                emailtemp_obj.isactive = true;
                                emailtemp_obj.createdby = emailVm.createdby;
                                emailtemp_obj.createdbyname = emailVm.createdbyname;
                                emailtemp_obj.createdon = DateTime.Now;
                                emailtemp_obj.updatedby = emailVm.updatedby;
                                emailtemp_obj.updatedbyname = emailVm.updatedbyname;
                                emailtemp_obj.updatedon = DateTime.Now;
                            };

                            _db.email_template.Add(emailtemp_obj);
                            _db.SaveChanges();
                            scope.Complete();

                            return true;
                        }

                        catch (Exception ex)
                        {
                            scope.Dispose();
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
        /// <summary>
        /// Delete EmailTemplate
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteEmailTemplate(int Id)
        {
            try
            {
                EmailTemplateVM email_obj = new EmailTemplateVM();
                var Data = _db.email_template.FirstOrDefault(x => x.id == Id);
                Data.isactive = false;
                Data.createdby = email_obj.createdby;
                Data.createdbyname = email_obj.createdbyname;
                Data.createdon = email_obj.createdon;
                Data.updatedby = email_obj.updatedby;
                Data.updatedbyname = email_obj.updatedbyname;
                Data.updatedon = DateTime.Now;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                return false;
            }
        }
        /// <summary>
        /// Edit EmailTemplate
        /// </summary>
        /// <param name="emailVm"></param>
        /// <returns></returns>
        public bool EditEmailTemplate(EmailTemplateVM emailVm)
        {
            try
            {
                bool bool_sameNameExists = _db.email_template.Any(x => x.email_template_definition.ToLower().Replace('#', ' ').Trim() == emailVm.email_template_definition.ToLower().Replace('#', ' ').Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }

                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        var Data = _db.email_template.FirstOrDefault(x => x.id == emailVm.id);
                        if (Data != null)
                        {
                            Data.email_template_name = emailVm.email_template_name;
                            Data.email_template_description = emailVm.email_template_description;
                            Data.email_template_definition = emailVm.email_template_definition;
                            Data.id = emailVm.id;
                            Data.isactive = true;
                            Data.createdby = emailVm.createdby;
                            Data.createdbyname = emailVm.createdbyname;
                            Data.createdon = DateTime.Now;
                            Data.updatedon = DateTime.Now;
                            Data.updatedby = emailVm.updatedby;
                            Data.updatedbyname = emailVm.updatedbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

    }
}

           
    

