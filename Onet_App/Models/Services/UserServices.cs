﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class UserServices : IUser
    {
        private readonly ApplicationDbContext _db;
        public UserServices(ApplicationDbContext db) => _db = db;

        /// <summary>
        /// Get All Users,
        /// Created By : Kritika 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public List<UserVM> GetUsers()
        {
            UserVM model = new UserVM();
            try
            {
                model.UserVMList = _db.users.Where(x => x.is_superadmin == false)
                    .Select(x => new UserVM
                    {
                        user_id = x.user_id,
                        username = x.username,
                        user_fullname = x.user_fullname,
                        user_password = x.user_password,
                        isactive = x.isactive,
                        is_superadmin = x.is_superadmin,
                        role_name = x.role_name,
                        campaign_id = x.campaign_id,
                        usergroup_id = x.usergroup_id,
                        createdby = x.createdby,
                        createdbyname = x.createdbyname,
                        createdon = x.createdon,
                        updatedby = x.updatedby,
                        updatedbyname = x.updatedbyname,
                        updatedon = x.updatedon

                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return model.UserVMList;
        }

        /// <summary>
        /// Get the User Data,
        /// Created By : Kritika 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public UserVM GetUser(int id)
        {
            UserVM model = new UserVM();
            try
            {
                //var data = (from user in _db.users.Where(x => x.user_id == id)
                //            join role in _db.roleMasters on user.role_name equals role.role_id
                //            select new UserVM
                //            {
                //                user_id = user.user_id,
                //                username = user.username,
                //                user_fullname = user.user_fullname,
                //                user_password = user.user_password,
                //                role_name = user.role_name,
                //                role_name_text = role.role_name
                //            }).FirstOrDefault();
                model.UserVMList = _db.users.Where(x => x.user_id == id)
                    .Select(x => new UserVM
                    {
                        user_id = x.user_id,
                        username = x.username,
                        user_fullname = x.user_fullname,
                        user_password = x.user_password,
                        isactive = x.isactive,
                        is_superadmin = x.is_superadmin,
                        role_name = x.role_name,
                        campaign_id = x.campaign_id,
                        usergroup_id = x.usergroup_id,
                        createdby = x.createdby,
                        createdbyname = x.createdbyname,
                        createdon = x.createdon,
                        updatedby = x.updatedby,
                        updatedbyname = x.updatedbyname,
                        updatedon = x.updatedon

                    }).ToList();
                model = model.UserVMList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return model;
        }

        /// <summary>
        /// Save and Update the User Data,
        /// Created By : Kritika 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public string SaveUpdateUser(UserVM userModel)
        {
            UserModel user = new UserModel();
            try
            {
                //Check for Duplicate Record


                if (userModel.user_id == 0)
                {
                    bool bool_sameNameExists = _db.users.Any(x => x.username.ToLower().Trim() == userModel.username.ToLower().Trim());
                    if (bool_sameNameExists)
                    {
                        return "Duplicate User";
                    }
                    else
                    {
                        if (userModel.user_password == userModel.confirm_password)
                        {
                            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                            {

                                user.username = userModel.username;
                                user.user_fullname = userModel.user_fullname;
                                user.user_password = UtilityServices.MD5Convertor(userModel.confirm_password);
                                user.role_name = userModel.role_name;
                                user.createdby = 1;
                                user.createdon = DateTime.Now;
                                user.updatedby = userModel.updatedby;
                                user.createdbyname = userModel.createdbyname;
                                user.updatedon = DateTime.Now;
                                user.updatedbyname = userModel.updatedbyname;
                                user.createdbyname = "admin";
                                user.isactive = true;
                                user.campaign_id = userModel.campaign_id;
                                user.usergroup_id = userModel.usergroup_id;
                                _db.users.Add(user);
                                _db.SaveChanges();
                                var data = _db.users.ToList().LastOrDefault();

                                if (userModel.UserGroupList != null && userModel.UserGroupList.Count != 0)
                                {
                                    foreach (var item in userModel.UserGroupList)
                                    {
                                        UserGroupAssignModel userGroupAssignModel = new UserGroupAssignModel
                                        {
                                            group_id = item.group_id,
                                            user_id = data.user_id,
                                            is_selected = item.is_selected,
                                            createdon = DateTime.Now,
                                            updatedby = userModel.updatedby,
                                            createdbyname = userModel.createdbyname,
                                            updatedon = DateTime.Now,
                                            updatedbyname = userModel.updatedbyname
                                        };
                                        UserGroupAssignModel usergroupassign = userGroupAssignModel;
                                        usergroupassign.createdbyname = "admin";
                                        usergroupassign.isactive = true;
                                        _db.usergroupassigns.Add(usergroupassign);
                                        _db.SaveChanges();
                                    }

                                }
                                scope.Complete();
                                scope.Dispose();
                                return "User Added Successfully";
                            }
                        }
                        else
                        {
                            return "Password and Confirm Password does not match.";
                        }
                    }
                }
                else
                {
                    var data = _db.users.FirstOrDefault(x => x.user_id == userModel.user_id);
                    if (data.username != userModel.username)
                    {
                        bool bool_sameNameExists = _db.users.Any(x => x.username.ToLower().Trim() == userModel.username.ToLower().Trim());
                        if (bool_sameNameExists)
                        {
                            return "Duplicate User";
                        }
                    }
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        data.username = userModel.username;
                        data.user_fullname = userModel.user_fullname;
                        data.role_name = userModel.role_name;
                        data.campaign_id = userModel.campaign_id;
                        data.updatedon = DateTime.Now;
                        data.updatedby = 1;
                        data.updatedbyname = "admin";
                        data.createdon = userModel.createdon;
                        data.createdby = userModel.createdby;
                        data.createdbyname = userModel.createdbyname;
                        _db.SaveChanges();
                        var _Assignment = _db.usergroupassigns.Where(x => x.user_id == data.user_id).ToList();
                        if (_Assignment != null && _Assignment.Count != 0)
                        {
                            foreach (var item in _Assignment)
                            {
                                _db.usergroupassigns.Remove(item);
                                _db.SaveChanges();
                            }

                        }
                        if (userModel.UserGroupList != null && userModel.UserGroupList.Count != 0)
                        {
                            foreach (var item in userModel.UserGroupList)
                            {
                                UserGroupAssignModel usergroupassign = new UserGroupAssignModel
                                {
                                    group_id = item.group_id,
                                    user_id = data.user_id,
                                    is_selected = item.is_selected,
                                    createdon = DateTime.Now,
                                    updatedby = userModel.updatedby,
                                    createdbyname = userModel.createdbyname,
                                    updatedon = DateTime.Now,
                                    updatedbyname = userModel.updatedbyname
                                };
                                usergroupassign.createdbyname = "admin";
                                usergroupassign.isactive = true;
                                _db.usergroupassigns.Add(usergroupassign);
                                _db.SaveChanges();
                            }

                        }
                        scope.Complete();
                        scope.Dispose();
                        return "User Updated Successfully";
                    }

                }



            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return "Error";
            }

        }

        /// <summary>
        /// Active and Inactive User,
        /// Created By : Kritika 
        ///On Date: 31/05/2018
        /// </summary>
        /// <returns></returns>
        public bool ActiveInactiveUser(UserVM userModel)
        {
            UserVM model = new UserVM();
            try
            {
                var data = _db.users.FirstOrDefault(x => x.user_id == userModel.user_id);
                if (data != null)
                {
                    if (data.isactive == true)
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.updatedby = 1;
                            data.updatedbyname = "admin";
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                        }
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = 1;
                            data.updatedbyname = "admin";
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }

        }

        /// <summary>
        /// Verify User,
        /// </summary>
        /// <returns></returns>
        public UserVM VerifyUser(UserVM userModel)
        {
            UserVM model = new UserVM();

            try
            {
                var data = _db.users.ToList();
                if (data == null || data.Count == 0)
                {

                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        _db.roleMasters.Add(new RoleMasterModel() { createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, description = "Administrator Role", role_name = RoleType.Admin, isactive = true });
                        _db.roleMasters.Add(new RoleMasterModel() { createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, description = "QA Role", role_name = RoleType.QA, isactive = true });
                        _db.roleMasters.Add(new RoleMasterModel() { createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, description = "Supervisor Role", role_name = RoleType.Supervisor, isactive = true });
                        _db.roleMasters.Add(new RoleMasterModel() { createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, description = "Agent Role", role_name = RoleType.Agent, isactive = true });
                        _db.SaveChanges();

                        _db.balanceForCollections.Add(new BalanceForCollectionModel() { balance_collection_name = "Overdue Balance", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.balanceForCollections.Add(new BalanceForCollectionModel() { balance_collection_name = "Outstanding Balance", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.SaveChanges();

                        _db.statuses.Add(new StatusModel() { status_name = "Active", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.statuses.Add(new StatusModel() { status_name = "Historic", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.SaveChanges();

                        _db.modeofsettlements.Add(new ModeOfSettlementModel() { modeofsettlement_name = "Individual Settlement", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.modeofsettlements.Add(new ModeOfSettlementModel() { modeofsettlement_name = "Company Settlement", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.SaveChanges();

                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "Bayad Center", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "Cebuana Lhuiller", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "LBC", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "7 Eleven", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "Banks", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "ECPay", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "SM Payment Center", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "Mlhuillier", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "Online Payment", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.paymentchannels.Add(new PaymentChannelModel() { payment_channel_name = "Credit Card", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.SaveChanges();

                        var region1 = new RegionModel() { region_name = "VizMin", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true }; _db.regionprimarys.Add(region1);
                        var region2 = new RegionModel() { region_name = "SouthLuzon", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true }; _db.regionprimarys.Add(region2);
                        var region3 = new RegionModel() { region_name = "NorthLuzon", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true }; _db.regionprimarys.Add(region3);
                        var region4 = new RegionModel() { region_name = "NCR", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true }; _db.regionprimarys.Add(region4);
                        _db.SaveChanges();


                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "Visayas", parent_region_id = region1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "Mindanao", parent_region_id = region1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "South Luzon", parent_region_id = region2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "SGMA", parent_region_id = region2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "North Luzon", parent_region_id = region3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "NGMA", parent_region_id = region3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.regionsecondarys.Add(new RegionSecondaryModel() { region_secondary = "NCR", parent_region_id = region4.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.SaveChanges();


                        var SkipTraceYes = new SkipTraceModel() { skip_trace_name = "Yes", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.skipstrace.Add(SkipTraceYes);
                        var SkipTraceNo = new SkipTraceModel() { skip_trace_name = "No", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.skipstrace.Add(SkipTraceNo);
                        _db.SaveChanges();

                        _db.skiptracechilds.Add(new SkipTraceChildModel() { skip_trace_child = "Iccbs", parent_skip_trace = SkipTraceYes.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.skiptracechilds.Add(new SkipTraceChildModel() { skip_trace_child = "Ics", parent_skip_trace = SkipTraceYes.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.skiptracechilds.Add(new SkipTraceChildModel() { skip_trace_child = "Mybss", parent_skip_trace = SkipTraceYes.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.skiptracechilds.Add(new SkipTraceChildModel() { skip_trace_child = "Web", parent_skip_trace = SkipTraceYes.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.skiptracechilds.Add(new SkipTraceChildModel() { skip_trace_child = "NO", parent_skip_trace = SkipTraceNo.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true });
                        _db.SaveChanges();

                        var Reason1 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "Bill Dispute", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason1);
                        var Reason2 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "Financial Difficulty", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason2);
                        var Reason3 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "Lack Of Awareness", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason3);
                        var Reason4 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "No Action", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason4);
                        var Reason5 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "Non Receipt Of Bill", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason5);
                        var Reason6 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "Others", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason6);
                        var Reason7 = new ReasonForNonPaymentModel() { reason_for_non_payment_name = "Uncontacted", createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true };
                        _db.reasonfornonpayments.Add(Reason7);
                        _db.SaveChanges();

                        #region Bill Dispute Child
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Additional Charges - Gadget Care", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Local Data", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Local Voice / Text", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "One Time Charge - Handset, Admin Fee", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Roaming Charges", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Service Complaints", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Unposted Payment", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Unposted Payment (EWT)", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "VAS - Subscription, Infotext, Gaming", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Waiting For The Bill Adjustment", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "With Open Termination Order", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "With Unprocess Termination Request", reason_for_non_payment_parent = Reason1.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        #region Financial Difficulty
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Business Closed", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Customer Hospitalized-Critical", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Customer Hospitalized-Not Critical", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Death In The Family", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Family Member Hospitalized", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Loss Job", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Paid Tuition", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Waiting For Salary / Remittance", reason_for_non_payment_parent = Reason2.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        #region Lack Of Awareness
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Does Not Know When To Pay", reason_for_non_payment_parent = Reason3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Does Not Know Where To Locate Nearest Payment Center", reason_for_non_payment_parent = Reason3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Does Not Know Where To Pay", reason_for_non_payment_parent = Reason3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Forgets Payment Due Date", reason_for_non_payment_parent = Reason3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Not Aware Of The Type Of Plan Acquired", reason_for_non_payment_parent = Reason3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Not Aware That Sub Acquired A Postpiad Plan", reason_for_non_payment_parent = Reason3.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        #region Non Receipt Of Bill
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Late Receipt Of Paperbill", reason_for_non_payment_parent = Reason5.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Late Receipt Of Paperless", reason_for_non_payment_parent = Reason5.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Non-Recipt of Paperbill", reason_for_non_payment_parent = Reason5.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Non-Recipt of Paperless", reason_for_non_payment_parent = Reason5.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        #region Others
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Additional Plan Booster", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Calamity", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Cannot Provide Details", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Cannot Provide Reason", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Company Pays The Account", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Enrolled In Auto Debit", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "For Termination", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Incharge Not Available", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Listed Sub Is Not The One Using And Paying the Account", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "On Process Payment", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Out Of The Country / Out Of Town", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Reclassification of Payment", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Subscriber Is Busy", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Transfer Of Location", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Transfer Of Ownership", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Priority Bills - Recurring", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Company Pays The Account", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Forgot to Settle Account", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Waiting For Check Pick-up", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Waiting For Signatory", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "With Payment Arrangement", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "With Termination Request", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Late Check Pick Up", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Late Posting of Payment", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Long Process of Payment", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Request For Onsite Visit", reason_for_non_payment_parent = Reason6.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        #region No Action
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "No Action", reason_for_non_payment_parent = Reason4.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        #region Uncontacted
                        _db.reasonfornonpaymentchilds.Add(new ReasonForNonPayementChildModel() { reason_for_non_payment_child_name = "Uncontacted", reason_for_non_payment_parent = Reason7.id, createdby = 0, createdbyname = "SuperAdmin", createdon = DateTime.Now, isactive = true, updatedby = 0, updatedbyname = "", updatedon = DateTime.Now });
                        #endregion

                        _db.SaveChanges();

                        var _Role = _db.roleMasters.FirstOrDefault(x => x.role_name == RoleType.Admin);
                        if (_Role != null)
                        {
                            UserModel user = new UserModel
                            {
                                username = userModel.username,
                                user_fullname = userModel.username,
                                user_password = UtilityServices.MD5Convertor(userModel.user_password),
                                is_superadmin = true,
                                role_name = _Role.role_id,
                                createdby = 0,
                                createdon = DateTime.Now,
                                createdbyname = "admin",
                                isactive = true
                            };
                            user.is_superadmin = true;
                            _db.users.Add(user);
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                        }
                        else
                        {
                            _db.roleMasters.RemoveRange(_db.roleMasters.ToList());
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                        }
                    }
                }
                string userpass = UtilityServices.MD5Convertor(userModel.user_password);
                model = _db.users.Where(x => x.username == userModel.username)
                    .Select(x => new UserVM
                    {
                        user_id = x.user_id,
                        username = x.username,
                        user_fullname = x.user_fullname,
                        user_password = x.user_password,
                        isactive = x.isactive,
                        is_superadmin = x.is_superadmin,
                        role_name = x.role_name,
                        campaign_id = x.campaign_id

                    }).FirstOrDefault();

                var _role = _db.roleMasters.FirstOrDefault(x => x.role_id == model.role_name);
                if (_role != null)
                    model.role_name_text = _role.role_name;
                if (model.role_name_text == RoleType.Supervisor)
                {
                    var UserGroup = _db.usergroupassigns.Where(x => x.user_id == model.user_id).ToList();
                    if (UserGroup != null)
                    {
                        foreach (var item in UserGroup)
                        {
                            var _Group = _db.usergroups.Where(x => x.group_id == item.group_id).ToList();
                            if (_Group != null)
                            {
                                foreach (var istem in _Group)
                                {
                                    var _DD = _db.campaigns.FirstOrDefault(x => x.campaign_id == istem.campaign_id);
                                    if (_DD != null)
                                    {
                                        model.CampaignList.Add(_DD);
                                    }

                                }
                            }
                        }
                    }
                }
                else if (model.role_name_text == RoleType.Agent)
                {
                    model.CampaignList = (from cam in _db.campaigns.Where(x => x.campaign_id == model.campaign_id)
                                          select new CampaignModel()
                                          {
                                              campaign_id = cam.campaign_id,
                                              campaign_isautomated = cam.campaign_isautomated,
                                              campaign_name = cam.campaign_name,
                                              createdby = cam.createdby,
                                              createdbyname = cam.createdbyname,
                                              createdon = cam.createdon,
                                              isactive = cam.isactive,
                                              list_name = cam.list_name,
                                              q_name = cam.q_name,
                                              skills_name = cam.skills_name,
                                              updatedby = cam.updatedby,
                                              updatedbyname = cam.updatedbyname,
                                              updatedon = cam.updatedon
                                          }).ToList();

                }

                string dbpass = model.user_password;
                if (dbpass == userpass && model.isactive == true)
                {
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }

        /// <summary>
        /// Change Password,
        /// </summary>
        /// <returns></returns>
        public bool ChangePassword(UserVM userModel)
        {
            UserVM model = new UserVM();
            try
            {
                var data = _db.users.FirstOrDefault(x => x.user_id == userModel.user_id);
                if (data != null)
                {

                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        data.user_password = UtilityServices.MD5Convertor(userModel.user_password);
                        data.updatedon = DateTime.Now;
                        data.updatedby = 1;
                        data.updatedbyname = "admin";
                        _db.SaveChanges();
                        scope.Complete();
                        scope.Dispose();
                    }


                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }

        }
    }
}

