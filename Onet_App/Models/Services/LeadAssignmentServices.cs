﻿using Microsoft.AspNetCore.Http;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class LeadAssignmentServices : ILeadAssignmentServices
    {
        private readonly ApplicationDbContext _db;

        public LeadAssignmentServices(ApplicationDbContext db) => _db = db;

        public LeadAssignmentVM GetLeadAssignment()
        {
            LeadAssignmentVM leadAssign = new LeadAssignmentVM();
            try
            {
                leadAssign.LeadAssignmentVMList = _db.leadassigns
                    .Select(x => new LeadAssignmentVM
                    {
                        id = x.id,
                        lead_id = x.lead_id,
                        agent_id = x.agent_id,
                        campaign_id = x.campaign_id
                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return leadAssign;
        }

        public bool LeadAssigment(CRMVM crmVM)
        {
            try
            {
                int i = 0;
                string calloutname = string.Empty;
                LeadAssignmentVM leadAssignVM = new LeadAssignmentVM();
                var data = _db.crms.Where(x => x.campaign_id == crmVM.userVM.campaign_id && x.enable_get_lead == true).ToList();
                leadAssignVM = GetLeadAssignment();
                List<int> _Ids = new List<int>();
                foreach (var item in leadAssignVM.LeadAssignmentVMList)
                {
                    if (item.agent_id == crmVM.userVM.user_id)
                    {
                        i++;
                    }
                    _Ids.Add(item.lead_id);
                }
                if (i < 50)
                {
                    int remaining_lead = 50 - i;
                    int j = 0;

                    var _Data = data.Where(x => !_Ids.Contains(x.crm_id)).ToList();
                    if (_Data != null)
                    {
                        foreach (var item in _Data)
                        {
                            if (item.callout_status_level1 > 0)
                            {
                                calloutname = _db.dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1).disposition_name;
                            }
                            if (item.updatedtimes == 0 || (item.updatedtimes == 1 && calloutname.ToLower().Trim() == "uncontacted"))
                            {
                                if (j != remaining_lead)
                                {
                                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                                    {
                                        LeadAssignmentModel leadassign = new LeadAssignmentModel
                                        {
                                            lead_id = item.crm_id,
                                            agent_id = crmVM.userVM.user_id,
                                            campaign_id = crmVM.userVM.campaign_id,
                                            createdby = crmVM.userVM.user_id,
                                            createdbyname = crmVM.userVM.user_fullname,
                                            createdon = DateTime.Now,
                                            updatedby = crmVM.userVM.updatedby,
                                            updatedbyname = crmVM.userVM.user_fullname
                                        };
                                        leadassign.createdon = DateTime.Now;


                                        _db.leadassigns.Add(leadassign);
                                        _db.SaveChanges();
                                        scope.Complete();
                                        scope.Dispose();
                                    }
                                    j++;

                                }
                            }
                        }
                    }

                }
                if (i == 50)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }

        }

        public List<LeadAssignmentVM> GetLeadByAssignedAgentId(int userId)
        {

            return GetLeadAssignment().LeadAssignmentVMList.Where(x => x.agent_id == userId).ToList();
        }

        public bool LeadReassignment(LeadAssignmentVM LeadAssginVM)
        {
            try
            {

                foreach (var item in LeadAssginVM.LeadList)
                {
                    if (item.is_selected)
                    {
                        var data = _db.leadassigns.FirstOrDefault(x => x.lead_id == item.crm_id);
                        if (data != null)
                        {
                            data.agent_id = LeadAssginVM.agent_id_2;
                            _db.SaveChanges();
                        }
                        else
                        {
                            Logger.WriteErrLog("Lead already Deleted by other User.", String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

        public List<LeadAssignmentVM> GetLeadByAssignedAgentClass(LeadAssignmentVM leadAssignmentVM)
        {
            List<LeadAssignmentVM> leadAssignmentVMs = new List<LeadAssignmentVM>();
            var _Data = _db.leadassigns.Where(x => x.agent_id == leadAssignmentVM.agent_id_1 && x.campaign_id == leadAssignmentVM.campaign_id).ToList();
            if (!String.IsNullOrWhiteSpace(leadAssignmentVM.phone1))
            {
                leadAssignmentVMs = (from crm in _db.crms.Where(x => x.account_name.ToLower().Contains(leadAssignmentVM.phone1.ToLower()))
                                     join dd in _Data on crm.crm_id equals dd.lead_id
                                     select new LeadAssignmentVM()
                                     {
                                         lead_id = crm.crm_id
                                     }).ToList();
            }
            else
            {
                leadAssignmentVMs = (from crm in _db.crms
                                     join dd in _Data on crm.crm_id equals dd.lead_id
                                     select new LeadAssignmentVM()
                                     {
                                         lead_id = crm.crm_id
                                     }).ToList();

            }
            return leadAssignmentVMs;
        }
    }
}
