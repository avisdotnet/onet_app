﻿using Microsoft.AspNetCore.Http;
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class CampaignServices : ICampaignServices
    {
        private readonly ApplicationDbContext _db;

        public CampaignServices(ApplicationDbContext db) => _db = db;
        /// <summary>
        /// ADD
        /// </summary>
        /// <param name="campaignVm"></param>
        /// <returns></returns>

        public bool AddCampaign(CampaignVM campaignVm)
        {
            try
            {
                bool bool_sameNameExists = _db.campaigns.Any(x => x.campaign_name.ToLower().Trim() == campaignVm.campaign_name.ToLower().Trim() && x.q_name.ToLower().Trim() == campaignVm.q_name.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            CampaignModel campaign_obj = new CampaignModel();
                            {

                                campaign_obj.campaign_name = campaignVm.campaign_name;
                                campaign_obj.skills_name = campaignVm.skills_name;
                                campaign_obj.list_name = campaignVm.list_name;
                                campaign_obj.campaign_isautomated = campaignVm.campaign_isautomated;
                                campaign_obj.q_name = campaignVm.q_name;
                                campaign_obj.isactive = true;
                                campaign_obj.createdby = campaignVm.createdby;
                                campaign_obj.UniqueValue = campaignVm.UniqueValue;
                                campaign_obj.createdbyname = campaignVm.createdbyname;
                                campaign_obj.createdon = DateTime.Now;
                                campaign_obj.updatedby = campaignVm.updatedby;
                                campaign_obj.updatedbyname = campaignVm.updatedbyname;
                                campaign_obj.updatedon = DateTime.Now;
                            };

                            _db.campaigns.Add(campaign_obj);
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                            return true;
                        }

                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;

            }
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteCampaign(int Id)
        {
            try
            {
                CampaignVM campaign_obj = new CampaignVM();
                var Data = _db.campaigns.FirstOrDefault(x => x.campaign_id == Id);
                Data.isactive = false;
                Data.createdby = campaign_obj.createdby;
                Data.createdbyname = campaign_obj.createdbyname;
                Data.createdon = campaign_obj.createdon;
                Data.UniqueValue = campaign_obj.UniqueValue;
                Data.updatedby = campaign_obj.updatedby;
                Data.updatedbyname = campaign_obj.updatedbyname;
                Data.updatedon = DateTime.Now;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                return false;
            }
        }
        /// <summary>
        /// EDIT
        /// </summary>
        /// <param name="campaignVm"></param>
        /// <returns></returns>
        public bool EditCampaign(CampaignVM campaignVm)
        {
            try
            {
                bool bool_sameNameExists = _db.campaigns.Any(x => x.campaign_name.ToLower().Trim() == campaignVm.campaign_name.ToLower().Trim() && x.q_name.ToLower().Trim() == campaignVm.q_name.ToLower().Trim() && x.campaign_id != campaignVm.campaign_id);
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            var Data = _db.campaigns.FirstOrDefault(x => x.campaign_id == campaignVm.campaign_id);
                            if (Data != null)
                            {
                                Data.campaign_id = campaignVm.campaign_id;
                                Data.campaign_name = campaignVm.campaign_name;
                                Data.skills_name = campaignVm.skills_name;
                                Data.isactive = true;
                                Data.list_name = campaignVm.list_name;
                                Data.UniqueValue = campaignVm.UniqueValue;
                                Data.campaign_isautomated = campaignVm.campaign_isautomated;
                                Data.q_name = campaignVm.q_name;
                                Data.createdby = campaignVm.createdby;
                                Data.createdbyname = campaignVm.createdbyname;
                                Data.createdon = campaignVm.createdon;
                                Data.updatedby = campaignVm.updatedby;
                                Data.updatedbyname = campaignVm.updatedbyname;
                                Data.updatedon = DateTime.Now;
                                _db.SaveChanges();
                                scope.Complete();
                                scope.Dispose();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
        /// <summary>
        /// GET ALL Campaign
        /// </summary>
        /// <returns></returns>
        public List<CampaignVM> GetAllCampaign(UserVM userVM)
        {
            CampaignVM campaignVM = new CampaignVM();
            try
            {
                var _campaignslst = _db.campaigns.Where(x => x.isactive == true)
                   .Select(obj => new CampaignVM
                   {
                       campaign_id = obj.campaign_id,
                       campaign_name = obj.campaign_name,
                       skills_name = obj.skills_name,
                       q_name = obj.q_name,
                       list_name = obj.list_name,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedbyname = obj.updatedbyname,
                       updatedon = DateTime.Now
                   }).ToList();
                var _campaignVM = new List<CampaignVM>();

                foreach (var item in userVM.CampaignList)
                {
                    var _remove = _campaignslst.FirstOrDefault(x => x.campaign_id == item.campaign_id);
                    if (_remove != null)
                    {
                        _campaignVM.Add(_remove);
                    }

                }

                campaignVM._campaignslst = _campaignVM;

                if (userVM.role_name_text == RoleType.Admin)
                {
                    campaignVM._campaignslst = _campaignslst;
                }


                return campaignVM._campaignslst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CampaignID"></param>
        /// <returns></returns>
        public CampaignVM GetCampaignById(int CampaignID)
        {
            CampaignVM campaign_obj = new CampaignVM();
            try
            {
                var Data = _db.campaigns.FirstOrDefault(x => x.campaign_id == CampaignID);
                if (Data != null)
                {
                    campaign_obj.campaign_id = Data.campaign_id;
                    campaign_obj.campaign_name = Data.campaign_name;
                    campaign_obj.skills_name = Data.skills_name;
                    campaign_obj.list_name = Data.list_name;
                    campaign_obj.campaign_isautomated = Data.campaign_isautomated;
                    campaign_obj.q_name = Data.q_name;
                    campaign_obj.isactive = Data.isactive;
                    campaign_obj.createdby = Data.createdby;
                    campaign_obj.UniqueValue = Data.UniqueValue;
                    campaign_obj.createdbyname = Data.createdbyname;
                    campaign_obj.createdon = Data.createdon;
                    campaign_obj.updatedby = Data.updatedby;
                    campaign_obj.updatedbyname = Data.updatedbyname;
                    campaign_obj.updatedon = DateTime.Now;
                }
                return campaign_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
    }
}
