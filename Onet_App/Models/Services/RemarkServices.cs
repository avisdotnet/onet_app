﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Onet_App.Models.Services
{
    public class RemarkServices : IRemarkServices
    {
        private readonly ApplicationDbContext _db;
        public RemarkServices(ApplicationDbContext db) => _db = db;
       
        /// <summary>
        /// Get All RemarkDetails
        /// </summary>
        /// <returns></returns>
        public List<RemarkVM> GetAllRemarkDetails()
        {
            RemarkVM remakrVM = new RemarkVM();
            try
            {
                remakrVM.RemarkVMList = _db.remarks
                   .Select(obj => new RemarkVM
                   {
                       remark = obj.remark,
                       lead_id = obj.lead_id,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedbyname = obj.updatedbyname,
                       updatedon = DateTime.Now
                   }).ToList();

                return remakrVM.RemarkVMList;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get Remark ByLeadId
        /// </summary>
        /// <param name="leadId"></param>
        /// <returns></returns>
        public List<RemarkVM> GetRemarkByLeadId(int leadId)
        {
            RemarkVM remark_obj = new RemarkVM();
            try
            {
                remark_obj.RemarkVMList = _db.remarks.Where(x=>x.lead_id == leadId)
                    .Select(obj => new RemarkVM
                    {
                        remark = obj.remark,
                        lead_id = obj.lead_id,
                        createdby = obj.createdby,
                        createdbyname = obj.createdbyname,
                        createdon = obj.createdon,
                        updatedby = obj.updatedby,
                        updatedbyname = obj.updatedbyname,
                        updatedon = DateTime.Now
                    }).ToList();
                return remark_obj.RemarkVMList;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
    }
    
}
