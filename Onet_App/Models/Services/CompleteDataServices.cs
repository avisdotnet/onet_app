﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Onet_App.Models.Services
{
    public class CompleteDataServices : ICompleteData
    {
        private readonly ApplicationDbContext _db;
        public CompleteDataServices(ApplicationDbContext _Db) => _db = _Db;
        public List<CRMDATAVM> DownloadCrmDetails(DateTime fromDate, DateTime toDate)
        {
            DateTime FromDate = fromDate;
            DateTime TODate = toDate;
            CRMVM crmVM = new CRMVM();
           // var camp = _db.campaigns.ToList();
            var _data_dispositions = _db.dispositions.ToList();
            var _data_balanceForCollections = _db.balanceForCollections.ToList();
            var _data_modeofsettlements = _db.modeofsettlements.ToList();
            var _data_paymentchannels = _db.paymentchannels.ToList();
            var _data_regionprimarys = _db.regionprimarys.ToList();
            var _data_regionsecondarys = _db.regionsecondarys.ToList();
            var _data_skipstrace = _db.skipstrace.ToList();
            var _data_skiptracechilds = _db.skiptracechilds.ToList();
            var _data_statuses = _db.statuses.ToList();
            var _data_campaigns = _db.campaigns.ToList();
            var _data_reasonfornonpayments = _db.reasonfornonpayments.ToList();
            var _data_reasonfornonpaymentchilds = _db.reasonfornonpaymentchilds.ToList();
            try
            {
                crmVM._crmlst = _db.crms.Where(x => x.updatedon >= FromDate && x.updatedon.Date <= TODate.Date).Select(obj => new CRMVM
                {
                    agent_id = obj.agent_id,
                    account_name = obj.account_name,
                    account_no = obj.account_no,
                    address = obj.address,
                    date_invoice = obj.date_invoice.HasValue == true ? obj.date_invoice : (DateTime?)null,
                    amount_collected = obj.amount_collected,
                    authorized_representatives = obj.authorized_representatives,
                    age_group = obj.age_group,
                    balance_for_collection = obj.balance_for_collection,
                    phone1 = obj.phone1,
                    callout_status_level1 = obj.callout_status_level1,
                    callout_status_level2 = obj.callout_status_level2,
                    callout_status_level3 = obj.callout_status_level3,
                    callout_status_level4 = obj.callout_status_level4,
                    callout_status_level5 = obj.callout_status_level5,
                    reason_for_non_payment_level1 = obj.reason_for_non_payment_level1,
                    reason_for_non_payment_level2 = obj.reason_for_non_payment_level2,
                    callout_status_level1_Text = "",
                    callout_status_level2_Text = "",
                    callout_status_level3_Text = "",
                    callout_status_level4_Text = "",
                    callout_status_level5_Text = "",
                    Balance_For_Collection_Text = "",
                    Mode_of_settlement_Text = "",
                    Payment_channel_Text = "",
                    region_primary_text = "",
                    region_secondary_text = "",
                    Skip_trace_Text = "",
                    Child_Skip_trace_Text = "",
                    Status_Text = "",
                    city = obj.city,
                    campaign_id = obj.campaign_id,
                    campaign_name = "",
                    company_name = obj.company_name,
                    count_of_attempted_contact_numbers = obj.count_of_attempted_contact_numbers,
                    createdby = obj.createdby,
                    crm_id = obj.crm_id,
                    email = obj.email,
                    financial_contact_person = obj.financial_contact_person,
                    give_new_contact_no = obj.give_new_contact_no,
                    invoice_amount = obj.invoice_amount,
                    invoice_no = obj.invoice_no,
                    mode_of_settlement = obj.mode_of_settlement,
                    number_of_accounts = obj.number_of_accounts,
                    outstanding_balance = obj.outstanding_balance,
                    overdue_balance = obj.overdue_balance,
                    phone_extension = obj.phone_extension,
                    payment_channel = obj.payment_channel,
                    phone2 = obj.phone2,
                    phone3 = obj.phone3,
                    phone4 = obj.phone4,
                    ptp_amount = obj.ptp_amount,
                    ptp_date = obj.ptp_date.HasValue == true ? obj.ptp_date : (DateTime?)null,
                    ptp_dated_by = obj.ptp_dated_by,
                    recommendation = obj.recommendation,
                    region_primary = obj.region_primary,
                    region_secondary = obj.region_secondary,
                    skip_trace_child = obj.skip_trace_child,
                    service_id_no = obj.service_id_no,
                    service_type = obj.service_type,
                    skip_trace = obj.skip_trace,
                    status = obj.status,
                    supervisor_id = obj.supervisor_id,
                    createdbyname = obj.createdbyname,
                    createdon = obj.createdon,
                    updatedby = obj.updatedby,
                    updatedbyname = obj.updatedbyname,
                    updatedon = obj.updatedon,
                    isactive = true,
                    remarks = obj.remarks,
                    list_name = obj.list_name,
                    endorsement_id = obj.endorsement_id,
                    enable_get_lead = obj.enable_get_lead
                }).OrderBy(x => x.campaign_name).OrderBy(x => x.list_name).ToList();

                foreach (var item in crmVM._crmlst)
                {
                    if (item.callout_status_level1 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level1);
                        if (_data != null)
                        {
                            item.callout_status_level1_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level2 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level2);
                        if (_data != null)
                        {
                            item.callout_status_level2_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level3 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level3);
                        if (_data != null)
                        {
                            item.callout_status_level3_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level4 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level4);
                        if (_data != null)
                        {
                            item.callout_status_level4_Text = _data.disposition_name;
                        }
                    }
                    if (item.callout_status_level5 > 0)
                    {
                        var _data = _data_dispositions.FirstOrDefault(x => x.disposition_id == item.callout_status_level5);
                        if (_data != null)
                        {
                            item.callout_status_level5_Text = _data.disposition_name;
                        }
                    }
                    if (item.balance_for_collection > 0)
                    {
                        var _data = _data_balanceForCollections.FirstOrDefault(x => x.id == item.balance_for_collection);
                        if (_data != null)
                        {
                            item.Balance_For_Collection_Text = _data.balance_collection_name;
                        }
                    }
                    if (item.mode_of_settlement > 0)
                    {
                        var _data = _data_modeofsettlements.FirstOrDefault(x => x.id == item.mode_of_settlement);
                        if (_data != null)
                        {
                            item.Mode_of_settlement_Text = _data.modeofsettlement_name;
                        }
                    }
                    if (item.payment_channel > 0)
                    {
                        var _data = _data_paymentchannels.FirstOrDefault(x => x.id == item.payment_channel);
                        if (_data != null)
                        {
                            item.Payment_channel_Text = _data.payment_channel_name;
                        }
                    }
                    if (item.region_primary > 0)
                    {
                        var _data = _data_regionprimarys.FirstOrDefault(x => x.id == item.region_primary);
                        if (_data != null)
                        {
                            item.region_primary_text = _data.region_name;
                        }
                    }
                    if (item.region_secondary > 0)
                    {
                        var _data = _data_regionsecondarys.FirstOrDefault(x => x.id == item.region_secondary);
                        if (_data != null)
                        {
                            item.region_secondary_text = _data.region_secondary;
                        }
                    }
                    if (item.skip_trace > 0)
                    {
                        var _data = _data_skipstrace.FirstOrDefault(x => x.id == item.skip_trace);
                        if (_data != null)
                        {
                            item.Skip_trace_Text = _data.skip_trace_name;
                        }
                    }
                    if (item.skip_trace_child > 0)
                    {
                        var _data = _data_skiptracechilds.FirstOrDefault(x => x.id == item.skip_trace_child);
                        if (_data != null)
                        {
                            item.Child_Skip_trace_Text = _data.skip_trace_child;
                        }
                    }
                    if (item.status > 0)
                    {
                        var _data = _data_statuses.FirstOrDefault(x => x.id == item.status);
                        if (_data != null)
                        {
                            item.Status_Text = _data.status_name;
                        }
                    }
                    if (item.campaign_id > 0)
                    {
                        var _data = _data_campaigns.FirstOrDefault(x => x.campaign_id == item.campaign_id);
                        if (_data != null)
                        {
                            item.campaign_name = _data.campaign_name;
                        }
                    }
                    if (item.reason_for_non_payment_level1 > 0)
                    {
                        var _data = _data_reasonfornonpayments.FirstOrDefault(x => x.id == item.reason_for_non_payment_level1);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level1_Text = _data.reason_for_non_payment_name;
                        }
                    }
                    if (item.reason_for_non_payment_level2 > 0)
                    {
                        var _data = _data_reasonfornonpaymentchilds.FirstOrDefault(x => x.id == item.reason_for_non_payment_level2);
                        if (_data != null)
                        {
                            item.reason_for_non_payment_level2_Text = _data.reason_for_non_payment_child_name;
                        }
                    }
                    var Agent_name = (from crm in _db.leadassigns.Where(x => x.lead_id == item.crm_id)
                                      join user in _db.users on crm.agent_id equals user.user_id
                                      select new UserModel()
                                      {
                                          username = user.username,
                                          user_fullname = user.user_fullname
                                      }).FirstOrDefault();

                    item.Agent_name = Agent_name != null ? Agent_name.user_fullname : "";
                    crmVM.cRMDATAVMs.Add(new CRMDATAVM()
                    {
                        Account_Name = item.account_name,
                        Account_No = item.account_no,
                        Address = item.address,
                        Date_Invoice = item.date_invoice.HasValue == true ? item.date_invoice : (DateTime?)null,
                        Amount_Collected = item.amount_collected,
                        Authorized_Representatives = item.authorized_representatives,
                        Age_Group = item.age_group,
                        Phone1 = item.phone1,
                        Callout_Status_Level1 = item.callout_status_level1_Text,
                        Callout_Status_Level2 = item.callout_status_level2_Text,
                        Callout_Status_Level3 = item.callout_status_level3_Text,
                        Callout_Status_Level4 = item.callout_status_level4_Text,
                        Callout_Status_Level5 = item.callout_status_level5_Text,
                        Balance_For_Collection = item.Balance_For_Collection_Text,
                        Mode_of_Settlement = item.Mode_of_settlement_Text,
                        Payment_Channel = item.Payment_channel_Text,
                        Region_Primary = item.region_primary_text,
                        Region_Secondary = item.region_secondary_text,
                        Skip_Trace_Level1 = item.Skip_trace_Text,
                        Skip_Trace_Level2 = item.Child_Skip_trace_Text,
                        Status_Text = item.Status_Text,
                        City = item.city,
                        Campaign_Name = item.campaign_name,
                        Company_Name = item.company_name,
                        Count_of_Attempted = item.count_of_attempted_contact_numbers,
                        Onet_Id = "ONET-" + Convert.ToString(item.crm_id),
                        Email = item.email,
                        Financial_Contact_Person = item.financial_contact_person,
                        New_Contact_No = item.give_new_contact_no,
                        Invoice_Amount = item.invoice_amount,
                        Invoice_No = item.invoice_no,
                        Number_of_Accounts = item.number_of_accounts,
                        Outstanding_Balance = item.outstanding_balance,
                        Overdue_Balance = item.overdue_balance,
                        Phone_Extension = item.phone_extension,
                        Phone2 = item.phone2,
                        Phone3 = item.phone3,
                        Phone4 = item.phone4,
                        PTP_Amount = item.ptp_amount,
                        PTP_Date = item.ptp_date.HasValue == true ? item.ptp_date : (DateTime?)null,
                        PTP_Dated_By = item.ptp_dated_by,
                        Recommendation = item.recommendation,
                        Service_Id = item.service_id_no,
                        Service_Type = item.service_type,
                        Created_On = item.createdon,
                        Updated_On = item.updatedon,
                        Remarks = item.remarks,
                        List_Name = item.list_name,
                        Activity_Name = item.activity_name,
                        Activity_Status = item.activity_status,
                        Activity_Summary = item.activity_summary,
                        Agent_Name = item.Agent_name,
                        Reason_for_Non_Payment_Level1 = item.reason_for_non_payment_level1_Text,
                        Reason_for_Non_Payment_Level2 = item.reason_for_non_payment_level2_Text,
                        Updated_Count = item.updatedtimes
                    });

                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
            //foreach (var campaign in camp)
            //{
            //    int campaign_id = campaign.campaign_id;
            //    var lists = GetListModels(campaign_id);
            //    foreach (var List in lists)
            //    {
            //        string ListNameValue = List.listname;

            //    }
            //}
            return crmVM.cRMDATAVMs;
        }
        public List<ListModel> GetListModels(int CampaignId)
        {
            List<ListModel> lstendorser = new List<ListModel>();
            try
            {
                lstendorser = _db.crms.Where(x => x.campaign_id == CampaignId).Select(x => new ListModel() { listname = x.list_name }).Distinct().ToList();
                return lstendorser;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return lstendorser;
            }
        }
    }
}
