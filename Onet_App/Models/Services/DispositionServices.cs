﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class DispositionServices : IDispositionServices
    {
        private readonly ApplicationDbContext _db;
        public DispositionServices(ApplicationDbContext db) => _db = db;

        /// <summary>
        /// Add Disposition
        /// </summary>
        /// <param name="dispositionVm"></param>
        /// <returns></returns>

        public bool AddDisposition(DispositionVM dispositionVm)
        {
            try
            {
                bool bool_sameNameExists = _db.dispositions.Any(x => x.disposition_name.ToLower().Trim() == dispositionVm.disposition_name.ToLower().Trim() && x.parent_disposition == dispositionVm.parent_disposition && x.isactive == true);
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            DispositionModel disposition_obj = new DispositionModel();
                            {
                                disposition_obj.disposition_name = dispositionVm.disposition_name;
                                disposition_obj.parent_disposition = dispositionVm.parent_disposition;
                                disposition_obj.short_name = dispositionVm.short_name;
                                disposition_obj.isactive = true;
                                disposition_obj.createdby = dispositionVm.createdby;
                                disposition_obj.createdbyname = dispositionVm.createdbyname;
                                disposition_obj.createdon = DateTime.Now;
                                disposition_obj.updatedby = dispositionVm.updatedby;
                                disposition_obj.updatedbyname = dispositionVm.updatedbyname;
                                disposition_obj.updatedon = DateTime.Now;
                            };
                            _db.dispositions.Add(disposition_obj);
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;

            }
        }

        /// <summary>
        /// Delete Disposition
        /// </summary>
        /// <param name="dispositionVM"></param>
        /// <returns></returns>
        public bool DeleteDisposition(int id)
        {
            DispositionVM dispositionVM = new DispositionVM();
            try
            {
                var Data = _db.dispositions.FirstOrDefault(x => x.disposition_id == id);
                Data.isactive = false;
                Data.createdby = dispositionVM.createdby;
                Data.createdbyname = dispositionVM.createdbyname;
                Data.createdon = dispositionVM.createdon;
                Data.updatedby = dispositionVM.updatedby;
                Data.updatedbyname = dispositionVM.updatedbyname;
                Data.updatedon = DateTime.Now;
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                return false;
            }
        }

        /// <summary>
        /// Edit Disposition
        /// </summary>
        /// <param name="dispositionVm"></param>
        /// <returns></returns>

        public bool EditDisposition(DispositionVM dispositionVm)
        {
            try
            {
                bool bool_sameNameExists = _db.dispositions.Any(x => x.disposition_name.ToLower().Trim() == dispositionVm.disposition_name.ToLower().Trim() && x.disposition_id != dispositionVm.disposition_id);
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            var Data = _db.dispositions.FirstOrDefault(x => x.disposition_id == dispositionVm.disposition_id);
                            if (Data != null)
                            {
                                Data.disposition_id = dispositionVm.disposition_id;
                                Data.disposition_name = dispositionVm.disposition_name;
                                Data.short_name = dispositionVm.short_name;
                                Data.parent_disposition = dispositionVm.parent_disposition;
                                Data.isactive = true;
                                Data.createdby = dispositionVm.createdby;
                                Data.createdbyname = dispositionVm.createdbyname;
                                Data.createdon = dispositionVm.createdon;
                                Data.updatedby = dispositionVm.updatedby;
                                Data.updatedbyname = dispositionVm.updatedbyname;
                                Data.updatedon = DateTime.Now;
                                _db.SaveChanges();
                                scope.Complete();
                                scope.Dispose();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }

                }
            }

            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }

        }

        public List<DispositionVM> GetAllChildDisposition(int ParentID)
        {
            DispositionVM dispositionVM = new DispositionVM();
            try
            {
                dispositionVM._dispositionlst = _db.dispositions.Where(x => x.isactive == true && x.parent_disposition == ParentID)
                   .Select(obj => new DispositionVM
                   {
                       disposition_name = obj.disposition_name,
                       parent_disposition = obj.parent_disposition,
                       short_name = obj.short_name,
                       disposition_id = obj.disposition_id,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedbyname = obj.updatedbyname,
                       updatedon = DateTime.Now,
                       // parent_disposition_name = _db.dispositions.FirstOrDefault(x => x.parent_disposition == obj.disposition_id).disposition_name
                   }).ToList();

                dispositionVM._dispositionlst.Insert(0, new DispositionVM() { disposition_id = 0, disposition_name = "--Select--" });
                return dispositionVM._dispositionlst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }

        /// <summary>
        /// Get All Disposition
        /// </summary>
        /// <returns></returns>
        public List<DispositionVM> GetAllDisposition()
        {
            DispositionVM dispositionVM = new DispositionVM();
            try
            {
                dispositionVM._dispositionlst = _db.dispositions.Where(x => x.isactive == true)
                   .Select(obj => new DispositionVM
                   {
                       disposition_name = obj.disposition_name,
                       parent_disposition = obj.parent_disposition,
                       short_name = obj.short_name,
                       disposition_id = obj.disposition_id,
                       parent_disposition_name = _db.dispositions.FirstOrDefault(x => x.parent_disposition == obj.disposition_id).disposition_name,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedbyname = obj.updatedbyname,
                       updatedon = DateTime.Now
                   }).ToList();

                return dispositionVM._dispositionlst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }

        public List<DispositionVM> GetAllParentDisposition()
        {
            DispositionVM dispositionVM = new DispositionVM();
            try
            {
                dispositionVM._dispositionlst = _db.dispositions.Where(x => x.isactive == true && x.parent_disposition == 0)
                   .Select(obj => new DispositionVM
                   {
                       disposition_name = obj.disposition_name,
                       parent_disposition = obj.parent_disposition,
                       short_name = obj.short_name,
                       disposition_id = obj.disposition_id,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedbyname = obj.updatedbyname,
                       updatedon = DateTime.Now,
                       // parent_disposition_name = _db.dispositions.FirstOrDefault(x => x.parent_disposition == obj.disposition_id).disposition_name
                   }).ToList();
              //  dispositionVM._dispositionlst.Insert(0, new DispositionVM() { disposition_id = 0, disposition_name = "--Select--" });
                return dispositionVM._dispositionlst;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }

        /// <summary>
        /// Get Disposition by Id
        /// </summary>
        /// <param name="dispositionID"></param>
        /// <returns></returns>
        public DispositionVM GetDispositionById(int dispositionID)
        {
            DispositionVM disposition_obj = new DispositionVM();
            try
            {
                var Data = _db.dispositions.FirstOrDefault(x => x.disposition_id == dispositionID);
                if (Data != null)
                {
                    disposition_obj.disposition_id = Data.disposition_id;
                    disposition_obj.disposition_name = Data.disposition_name;
                    disposition_obj.parent_disposition = Data.parent_disposition;
                    disposition_obj.short_name = Data.short_name;
                    disposition_obj.isactive = Data.isactive;
                    disposition_obj.createdby = Data.createdby;
                    disposition_obj.createdbyname = Data.createdbyname;
                    disposition_obj.createdon = Data.createdon;
                    disposition_obj.updatedby = Data.updatedby;
                    disposition_obj.updatedbyname = Data.updatedbyname;
                    disposition_obj.updatedon = DateTime.Now;
                }
                return disposition_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }
        }
    }
}
