﻿
using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class UserGroupServices : IUserGroupServices
    {
        private readonly ApplicationDbContext _db;
        public UserGroupServices(ApplicationDbContext db) => _db = db;

        /// <summary>
        /// Enable Disable
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public bool ActiveInactiveUserGroup(UserGroupVM userModel)
        {
            UserGroupVM model = new UserGroupVM();
            try
            {
                var data = _db.usergroups.FirstOrDefault(x => x.group_id == userModel.group_id);
                if (data != null)
                {
                    if (data.isactive == true)
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            data.isactive = false;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            data.createdon = DateTime.Now;
                            data.createdby = model.createdby;
                            data.createdbyname = model.createdbyname;
                            data.createdbyname = model.createdbyname;

                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                        }
                    }
                    else
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            data.isactive = true;
                            data.updatedon = DateTime.Now;
                            data.updatedby = model.updatedby;
                            data.createdon = model.createdon;
                            data.createdby = model.createdby;
                            data.createdbyname = model.createdbyname;
                            data.createdbyname = model.createdbyname;
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }

        }
        /// <summary>
        /// GET USERGROUP BY ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserGroupVM GetUserGroupById(int id)
        {
            UserGroupVM user_obj = new UserGroupVM();
            try
            {
                var Data = _db.usergroups.FirstOrDefault(x => x.group_id == id);
                if (Data != null)
                {
                    user_obj.group_id = Data.group_id;
                    user_obj.group_name = Data.group_name;
                    user_obj.campaign_id = Data.campaign_id;
                    user_obj.isactive = Data.isactive;
                    user_obj.createdby = Data.createdby;
                    user_obj.createdbyname = Data.createdbyname;
                    user_obj.createdon = Data.createdon;
                    user_obj.updatedby = Data.updatedby;
                    user_obj.updatedon = Data.updatedon;
                    user_obj.updatedbyname = Data.updatedbyname;
                }
                return user_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }
        /// <summary>
        /// Get Users Group
        /// </summary>
        /// <returns></returns>
        public List<UserGroupVM> GetUserGroup()
        {
            UserGroupVM userGroup = new UserGroupVM();
            try
            {
                userGroup.UserGroupList = _db.usergroups.Where(x => x.isactive == true)
                   .Select(obj => new UserGroupVM
                   {
                       group_name = obj.group_name,
                       campaign_id = obj.campaign_id,
                       group_id = obj.group_id,
                       campaign_name = _db.campaigns.FirstOrDefault(x => x.campaign_id == obj.campaign_id).campaign_name,
                       isactive = obj.isactive,
                       createdby = obj.createdby,
                       createdbyname = obj.createdbyname,
                       createdon = obj.createdon,
                       updatedby = obj.updatedby,
                       updatedon = obj.updatedon,
                       updatedbyname = obj.updatedbyname
            }).ToList();

                return userGroup.UserGroupList;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }

        /// <summary>
        /// ADD user group
        /// </summary>
        /// <param name="usergroupModel"></param>
        /// <returns></returns>
        public bool AddUserGroup(UserGroupVM userGroupVM)
        {
            try
            {
                bool bool_sameNameExists = _db.usergroups.Any(x => x.group_name.ToLower().Trim() == userGroupVM.group_name.ToLower().Trim());
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            UserGroupModel group_obj = new UserGroupModel();
                            {
                                group_obj.group_name = userGroupVM.group_name;
                                group_obj.campaign_id = userGroupVM.campaign_id;
                                group_obj.isactive = true;
                                group_obj.createdon = DateTime.Now;
                                group_obj.createdbyname = userGroupVM.createdbyname;
                                group_obj.updatedby = userGroupVM.updatedby;
                                group_obj.updatedbyname = userGroupVM.updatedbyname;
                                group_obj.updatedon = DateTime.Now;
                                group_obj.createdby = userGroupVM.createdby;
                            };
                            _db.usergroups.Add(group_obj);
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            scope.Complete();
                            return false;
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }
        /// <summary>
        /// Edit UserGroup
        /// </summary>
        /// <param name="roleMasterVM"></param>
        /// <returns></returns>
        public bool EditUserGroup(UserGroupVM userGroupVM)
        {
            try
            {
                bool bool_sameNameExists = _db.usergroups.Any(x => x.group_name.ToLower().Trim() == userGroupVM.group_name.ToLower().Trim() && x.group_id != userGroupVM.group_id);
                if (bool_sameNameExists)
                {
                    return false;
                }
                else
                {
                    UserGroupVM group_obj = new UserGroupVM();
                   
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            var Data = _db.usergroups.FirstOrDefault(x => x.group_id == userGroupVM.group_id);
                            if (Data != null)
                            {
                              
                                Data.group_name = userGroupVM.group_name;
                                Data.campaign_id = userGroupVM.campaign_id;
                                Data.isactive = true;
                                Data.createdon = DateTime.Now;
                                Data.createdbyname = userGroupVM.createdbyname;
                                Data.updatedby = userGroupVM.updatedby;
                                Data.updatedbyname = userGroupVM.updatedbyname;
                                Data.updatedon = DateTime.Now;
                                Data.createdby = userGroupVM.createdby;
                                _db.SaveChanges();
                                var data = _db.groupsuserassigns.Where(x => x.group_id == Data.group_id);
                                if (data != null)
                                {
                                    foreach (var item in data)
                                    {
                                        _db.groupsuserassigns.Remove(item);
                                    }

                                }
                                _db.SaveChanges();
                                scope.Complete();
                                scope.Dispose();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

        /// <summary>
        /// Delete 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteUserGroup(int id)
        {
            UserGroupVM role = new UserGroupVM();
            try
            {
                var Data = _db.usergroups.FirstOrDefault(x => x.group_id == id);
                Data.isactive = false;
                Data.createdon = role.createdon;
                Data.createdby = role.createdby;
                Data.updatedon = DateTime.Now;
                Data.updatedby = role.updatedby;
                Data.createdbyname = role.createdbyname;
                Data.updatedbyname = role.updatedbyname;
                _db.SaveChanges();

                var _Users = _db.groupsuserassigns.Where(x => x.group_id == id).ToList();
                _db.groupsuserassigns.RemoveRange(_Users);
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));

                return false;
            }
        }

        public  bool AddEditGroupUserAssign(UserGroupVM usergroup)
        {
            try
            {
                var Data = _db.usergroups.FirstOrDefault(x => x.group_id == usergroup.group_id);
                List<GroupUserAssignModel> data = _db.groupsuserassigns.Where(x => x.group_id == Data.group_id).ToList();
                if(data!=null)
                {
                    foreach (var item in data)
                    {
                        _db.groupsuserassigns.Remove(item);
                    }
                   
                }
                _db.SaveChanges();
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    try
                    {
                        foreach (var item in usergroup.NewAgentList)
                        {
                            if (item.is_selected == true)
                            {
                                GroupUserAssignModel group_obj = new GroupUserAssignModel();
                                {
                                    group_obj.group_id = usergroup.group_id;
                                    group_obj.agent_id = item.user_id;
                                    group_obj.is_selected = item.is_selected;
                                    group_obj.isactive = true;
                                    group_obj.createdon = DateTime.Now;
                                    group_obj.createdbyname = usergroup.createdbyname;
                                    group_obj.updatedby = usergroup.updatedby;
                                    group_obj.updatedbyname = usergroup.updatedbyname;
                                    group_obj.updatedon = DateTime.Now;
                                    group_obj.createdby = usergroup.createdby;
                                };

                                _db.groupsuserassigns.Add(group_obj);
                                _db.SaveChanges();
                            }
                        }
                        foreach (var item in usergroup.ExistingAgentList)
                        {
                            if (item.is_selected == true)
                            {
                                GroupUserAssignModel group_obj = new GroupUserAssignModel();
                                {
                                    group_obj.group_id = usergroup.group_id;
                                    group_obj.agent_id = item.user_id;
                                    group_obj.is_selected = item.is_selected;
                                    group_obj.isactive = true;
                                    group_obj.createdon = DateTime.Now;
                                    group_obj.createdbyname = usergroup.createdbyname;
                                    group_obj.updatedby = usergroup.updatedby;
                                    group_obj.updatedbyname = usergroup.updatedbyname;
                                    group_obj.updatedon = DateTime.Now;
                                    group_obj.createdby = usergroup.createdby;
                                };

                                _db.groupsuserassigns.Add(group_obj);
                                _db.SaveChanges();
                            }
                        }
                        scope.Complete();
                        scope.Dispose();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                        return false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

       
    }
}
