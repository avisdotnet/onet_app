﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface ILeadAssignmentServices
    {
        bool LeadAssigment(CRMVM crmVM);
        LeadAssignmentVM GetLeadAssignment();
        List<LeadAssignmentVM> GetLeadByAssignedAgentId(int Userid);
        List<LeadAssignmentVM> GetLeadByAssignedAgentClass(LeadAssignmentVM leadAssignmentVM);
        bool LeadReassignment(LeadAssignmentVM LeadAssginVM);
    }
}
