﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IEmailTemplateServices
    {
        List<EmailTemplateVM> GetEmailTemplateList();
        EmailTemplateVM GetEmailTemplateById(int ID);
        bool AddEmailTemplate(EmailTemplateVM emailVm);
        bool DeleteEmailTemplate(int Id);
        bool EditEmailTemplate(EmailTemplateVM emailVm);
      

    }
}
