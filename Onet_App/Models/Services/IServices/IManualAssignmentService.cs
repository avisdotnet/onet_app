﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IManualAssignmentService
    {
        bool ManualAssign();
        List<EndorserModel> GetEndorserList(int CampaignId);
        List<ListModel> GetListModels(int EndorserId);
        List<CRMVM> GetDataForManualAssignment(ManualAssignmentVM manualAssignmentVM);
    }
}
