﻿using Microsoft.Extensions.Configuration;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface ICRMServices
    {
        bool AddEditCRM(CRMVM crmvm, IConfiguration configuration);
        int AddCRM(List<CRMVM> crmvmlst, IConfiguration configuration, int Campaign_id, int endorsername);
        CRMModel GetCrmByMobileNo(string MobileNo);
        CRMModel GetCrmByMobileNoAndCamp(string MobileNo, int campaign);
        List<CRMVM> GetAllCrmDetails();
        List<CRMVM> GetCrmDetails(int campaign_id);
        List<CRMDATAVM> DownloadCrmDetails(int campaign_id, string ListNameValue, DateTime FromDate, DateTime TODate);
        List<CRMVM> GetCallBacksDetails();
        List<CityModel> GetAllCity();
        List<SkipTraceModel> GetAllSkipTrace();
        List<StatusModel> GetAllStatus();
        List<RegionModel> GetAllRegion();
        List<ModeOfSettlementModel> GetAllModeOfSettlement();
        List<PaymentChannelModel> GetAllPayementChannel();
        List<BalanceForCollectionModel> GetAllBalanceForCollection();
        List<ReasonForNonPaymentModel> GetAllReasonForNonPayment();
        List<RegionSecondaryModel> GetRegionSecondary(int id);
        List<ReasonForNonPayementChildModel> GetReasonForNonPayementChildModel(int id);
        List<SkipTraceChildModel> GetSkipTraceChildModel(int id);
        bool AddActivityDetails(CRMVM activityModel);
        bool EditPaymentField(CRMVM crmpayment);
        int SaveModeofSettlement(string str);
        int SaveStatus(string str);
        int SaveRegionPrimary(string str);
        int SaveRegionSecondary(string primary, string secondary);
        int SaveSkipTracer(string str);
        int SaveSkipTracerChild(string primary, string secondary);
        int SaveCallOutStatus(string str);
        int SaveCallOutStatusChild(string primary, string secondary);
        int GetModeOfSettlementId(string str);
        int GetStatusId(string str);
        int GetRegionPrimaryId(string str);
        int GetRegionSecondaryId(string str);
        int GetSkipTracerId(string str);
        int GetSkipTracerChildId(string str);
        int GetCalloutstatusId(string str);
        int SaveBalanceForCollections(string str);
        int GetBalanceForCollectionsId(string str);
        List<ListModel> GetListModels(int CampaignId);
        List<CRMVM> GetAllCrmDetails(List<int> _ids);
    }
}
