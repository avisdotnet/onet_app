﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
   public interface IUserGroupServices
    {
      
        UserGroupVM GetUserGroupById(int id);
        List<UserGroupVM> GetUserGroup();
        bool AddUserGroup(UserGroupVM userGroupVM);
        bool EditUserGroup(UserGroupVM userGroupVM);
        bool DeleteUserGroup(int id);
        bool ActiveInactiveUserGroup(UserGroupVM userModel);
        bool AddEditGroupUserAssign(UserGroupVM usergroup);

    }
}
