﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IDispositionServices
    {
        bool AddDisposition(DispositionVM dispositionVm);
        bool EditDisposition(DispositionVM dispositionVm);
        List<DispositionVM> GetAllDisposition();
        List<DispositionVM> GetAllParentDisposition();
        List<DispositionVM> GetAllChildDisposition(int ParentID);
        DispositionVM GetDispositionById(int dispositionID);
        bool DeleteDisposition(int id);
    }
}
