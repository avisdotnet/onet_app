﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IRemarkServices
    {
        List<RemarkVM> GetRemarkByLeadId(int leadId);
        List<RemarkVM> GetAllRemarkDetails();
    }
}
