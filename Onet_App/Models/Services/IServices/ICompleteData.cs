﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    interface ICompleteData
    {
       List<CRMDATAVM> DownloadCrmDetails(DateTime fromDate,DateTime toDate);
        List<ListModel> GetListModels(int CampaignId);

    }
}
