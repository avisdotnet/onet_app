﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public  interface ICampaignServices
    {
        bool AddCampaign(CampaignVM campaignVm);
        bool EditCampaign(CampaignVM campaignVm);
        bool DeleteCampaign(int Id);
        List<CampaignVM> GetAllCampaign(UserVM userVM);
        CampaignVM GetCampaignById(int CampaignID);
    }
}
