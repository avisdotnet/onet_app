﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    interface ISMSTemplateServices
    {
        List<SMSTemplateVM> GetSMSTemplateList();
        SMSTemplateVM GetSMSTemplateById(int id);
        string SaveUpdateSMSTemplate(SMSTemplateVM sms);
        bool DeleteTemplate(SMSTemplateVM sms);
    }
}
