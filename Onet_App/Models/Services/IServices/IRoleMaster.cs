﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IRoleMaster
    {
        bool AddRole(RoleMasterVM roleMasterModel);
        bool EditRole(RoleMasterVM roleMasterModel);
        bool DeleteRole(int id);
        List<RoleMasterModel> MenuCheckList();
        List<RoleMasterVM> GetAllRoles();
        RoleMasterVM GetRolesById(int RoleID);
    }
}
