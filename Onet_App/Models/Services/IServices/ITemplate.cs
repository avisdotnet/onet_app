﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface ITemplate
    {
        bool SaveTemplateCreation(TemplateVM templateVM);
         TemplateVM GetTemplateID(int Id);
        List<TemplateVM> AllActiveTemplate();
      

    }
}
