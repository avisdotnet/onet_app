﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IActivityServices
    {
        List<ActivityHistoryVM> GetActivityByLeadId(int leadId);
        List<ActivityHistoryVM> GetAllActivityDetails();
    }
}
