﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IUser
    {
        UserVM VerifyUser(UserVM userModel);
        UserVM GetUser(int id);
        List<UserVM> GetUsers();
        string SaveUpdateUser(UserVM userModel);
        bool ActiveInactiveUser(UserVM userModel);
        bool ChangePassword(UserVM userModel);
    }
}
