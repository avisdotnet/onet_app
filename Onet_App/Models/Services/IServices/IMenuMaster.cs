﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.Services.IServices
{
    public interface IMenuMaster
    {
        bool SaveUpdateMenu(MenuMasterVM menuMasterModel);
        MenuMasterVM GetMenuByID(int MenuID);
        MenuMasterVM GetAllMenus();
        MenuMasterVM GetMenuByStatus(bool IsActive);

        /// <summary>
        /// Function for Get All Menus Including InAcive
        /// Created By : Anamika on 30/05/2018
        /// </summary>
        /// <returns></returns>
        MenuMasterVM GetAllMenusByRoleId(int Id);
    }
}
