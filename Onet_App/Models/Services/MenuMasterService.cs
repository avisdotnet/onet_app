﻿using Onet_App.Models.Services.IServices;
using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;

namespace Onet_App.Models.Services
{
    public class MenuMasterService : IMenuMaster
    {
        private readonly ApplicationDbContext _db;

        public MenuMasterService(ApplicationDbContext db) => _db = db;

        /// <summary>
        /// Function for Get All Menus Including InAcive
        /// </summary>
        /// <returns></returns>
        public MenuMasterVM GetAllMenus()
        {
            MenuMasterVM menuModel = new MenuMasterVM();
            try
            {
                menuModel.MenuMasterVMList = _db.menumaster
                    .Select(x => new MenuMasterVM
                    {
                        action_name = x.action_name,
                        area_name = x.area_name,
                        controller_name = x.controller_name,
                        createdon = x.createdon,
                        isactive = x.isactive,
                        is_selected = x.isactive,
                        updatedby = x.updatedby,
                        updatedon = x.updatedon,
                        createdby = x.createdby,
                        createdbyname = x.createdbyname,
                        updatedbyname=x.updatedbyname,
                        menu_id = x.menu_id,
                        menu_name = x.menu_name,
                        query_string = x.query_string
                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return menuModel;
        }

        /// <summary>
        /// Function for Get Menus by Id
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        public MenuMasterVM GetMenuByID(int MenuID)
        {
            MenuMasterVM menu_obj = new MenuMasterVM();
            try
            {
                var Data = _db.menumaster.FirstOrDefault(x => x.menu_id == MenuID);
                if (Data != null)
                {
                    menu_obj.menu_name = Data.menu_name;
                    menu_obj.action_name = Data.action_name;
                    menu_obj.area_name = Data.area_name;
                    menu_obj.controller_name = Data.controller_name;
                    menu_obj.createdon = DateTime.Now;
                    menu_obj.isactive = Data.isactive;
                    menu_obj.menu_id = Data.menu_id;
                    menu_obj.query_string = Data.query_string;
                }
                return menu_obj;
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return null;
            }

        }

        /// <summary>
        /// Function for Get Menus by status
        /// </summary>
        /// <param name="IsActive"></param>
        /// <returns></returns>
        public MenuMasterVM GetMenuByStatus(bool IsActive)
        {
            MenuMasterVM MenuVm = new MenuMasterVM();
            try
            {
                MenuVm.MenuMasterVMList = _db.menumaster
                    .Where(m => m.isactive == IsActive).Select(x => new MenuMasterVM
                    {
                        action_name = x.action_name,
                        area_name = x.area_name,
                        controller_name = x.controller_name,
                        createdon = x.createdon,
                        isactive = x.isactive,
                        is_selected = x.isactive,
                        updatedon=DateTime.Now,
                        createdby=x.createdby,
                        updatedbyname=x.updatedbyname,
                        updatedby=x.updatedby,
                        createdbyname=x.createdbyname,
                        menu_id = x.menu_id,
                        menu_name = x.menu_name,
                        query_string = x.query_string
                    }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return MenuVm;
        }

        /// <summary>
        /// Function for save update menu
        /// </summary>
        /// <param name="menuMasterModel"></param>
        /// <returns></returns>
        public bool SaveUpdateMenu(MenuMasterVM menuMasterModel)
        {
            try
            {
                if (menuMasterModel != null)
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            MenuMasterModel menu_obj = new MenuMasterModel()
                            {
                                menu_name = menuMasterModel.menu_name,
                                area_name = menuMasterModel.area_name,
                                controller_name = menuMasterModel.controller_name,
                                query_string = menuMasterModel.query_string,
                                isactive = true,
                                createdon = DateTime.Now,
                                createdbyname = menuMasterModel.createdbyname,
                                updatedon = DateTime.Now,
                                updatedby = menuMasterModel.updatedby,
                                createdby = menuMasterModel.createdby,
                                updatedbyname = menuMasterModel.updatedbyname,
                                action_name = menuMasterModel.action_name,
                            };
                            _db.menumaster.Add(menu_obj);
                            _db.SaveChanges();
                            scope.Complete();
                            scope.Dispose();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }
                else
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        try
                        {
                            var Update = _db.menumaster.FirstOrDefault(x => x.menu_id == menuMasterModel.menu_id);
                            if (Update != null)
                            {
                                Update.menu_name = menuMasterModel.menu_name;
                                Update.area_name = menuMasterModel.area_name;
                                Update.controller_name = menuMasterModel.controller_name;
                                Update.query_string = menuMasterModel.query_string;
                                Update.isactive = true;
                                Update.menu_id = menuMasterModel.menu_id;
                                Update.createdon = menuMasterModel.createdon;
                                Update.updatedon = DateTime.Now;
                                Update.updatedby = menuMasterModel.updatedby;
                                Update.updatedbyname = menuMasterModel.updatedbyname;
                                Update.createdby = menuMasterModel.createdby;
                                Update.createdbyname = menuMasterModel.createdbyname;
                                Update.action_name = menuMasterModel.action_name;
                                _db.SaveChanges();
                                scope.Complete();
                                scope.Dispose();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                            return false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
                return false;
            }
        }

        /// <summary>
        /// Function for Get All Menus Including InAcive
        /// Created By : Anamika on 30/05/2018
        /// </summary>
        /// <returns></returns>
        public MenuMasterVM GetAllMenusByRoleId(int Id)
        {
            MenuMasterVM menuModel = new MenuMasterVM();
            try
            {
                menuModel.MenuMasterVMList = (from Menu in _db.menumaster
                                             from roleDetils in _db.rolemenudetails.Where(x => x.menu_id == Menu.menu_id && x.role_id==Id).DefaultIfEmpty()
                                             select new MenuMasterVM
                                             {
                                                 action_name = Menu.action_name,
                                                 area_name = Menu.area_name,
                                                 controller_name = Menu.controller_name,
                                                 createdon = Menu.createdon,
                                                 isactive = Menu.isactive,
                                                 updatedon=DateTime.Now,
                                                 createdby=Menu.createdby,
                                                 createdbyname=Menu.createdbyname,
                                                 updatedby=Menu.updatedby,
                                                 updatedbyname=Menu.updatedbyname,
                                                 is_selected = roleDetils.is_selected,
                                                 menu_id = Menu.menu_id,
                                                 menu_name = Menu.menu_name,
                                                 query_string = Menu.query_string
                                             }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteErrLog(ex, String.Format("{0} ==> {1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name));
            }
            return menuModel;
        }
    }
}
