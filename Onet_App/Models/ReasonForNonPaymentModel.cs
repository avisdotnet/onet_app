﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class ReasonForNonPaymentModel:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string reason_for_non_payment_name { get; set; }
    }
}
