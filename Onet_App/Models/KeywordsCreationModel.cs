﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class KeywordsCreationModel:CommonAttributeModel
    {
        [Key]
        public int key_id { get; set; }

        [Display(Name = "Keyword Name")]
        [Required(ErrorMessage = "Keyword Name is Required")]
        [RegularExpression(@"^\w+$")]
        public string keyword_name { get; set; }
       
        public string keyword_description { get; set; }

    }
}
