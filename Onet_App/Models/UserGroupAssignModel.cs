﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class UserGroupAssignModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public int group_id { get; set; }
        [ForeignKey("user_id")]
        public int user_id { get; set; }
        public bool is_selected { get; set; }
      
    }
}
