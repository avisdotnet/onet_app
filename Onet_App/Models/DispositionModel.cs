﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class DispositionModel:CommonAttributeModel
    {
        [Key]
        public int disposition_id { get; set; }

        [Display(Name = "Disposition Name")]
        [Required(ErrorMessage = "Disposition Name  is Required")]
        public string disposition_name { get; set; }

        [Display(Name = "Short Name")]
        [Required(ErrorMessage = "Short Name  is Required")]
        public string short_name { get; set; }
      
        public int parent_disposition { get; set; }
    }
}
