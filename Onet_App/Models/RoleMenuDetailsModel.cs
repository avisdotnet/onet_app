﻿using Onet_App.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class RoleMenuDetailsModel : CommonAttributeModel
    {
        [Key]
        public int Id { get; set; }

        public int menu_id { get; set; }

        public int role_id { get; set; }

        public bool is_selected { get; set; }
    }
}
