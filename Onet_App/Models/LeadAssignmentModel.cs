﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class LeadAssignmentModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("crm_id")]
        public int lead_id { get; set; }
        public int agent_id { get; set; }
        public int campaign_id { get; set; }

    }
}
