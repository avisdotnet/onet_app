﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class SkipTraceChildModel:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string skip_trace_child { get; set; }
        public int parent_skip_trace { get; set; }
    }
}
