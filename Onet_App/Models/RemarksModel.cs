﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class RemarksModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public int lead_id { get; set; }
        public string remark { get; set; }
    }
}
