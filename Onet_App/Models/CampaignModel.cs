﻿using System.ComponentModel.DataAnnotations;

namespace Onet_App.Models
{
    public class CampaignModel : CommonAttributeModel
    {
        [Key]
        public int campaign_id { get; set; }

        [Display(Name = "Campaign Name")]
        [Required(ErrorMessage = "Campaign Name is Required")]
        public string campaign_name { get; set; }

        [Display(Name = "Skill Name")]
        public string skills_name { get; set; }

        [Display(Name = "List Name")]
        public string list_name { get; set; }


        [Required(ErrorMessage = "This field is required")]
        public string UniqueValue { get; set; }

        [Display(Name = "Campaign Automated")]
        public bool campaign_isautomated { get; set; }

        [Display(Name = "Q Name")]
        [Required(ErrorMessage = "Q Name is Required")]
        public string q_name { get; set; }
    }
}
