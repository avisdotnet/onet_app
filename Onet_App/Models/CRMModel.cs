﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class CRMModel : CommonAttributeModel
    {
        [Key]
        public int crm_id { get; set; }
        public int agent_id { get; set; }
        public int supervisor_id { get; set; }
        public int endorsement_id { get; set; }
        public int campaign_id { get; set; }
        public string dailer_disposition { get; set; }
        public string last_called_phoneno { get; set; }


        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Only Alphanumeric Characters allowed.")]
        public String list_name { get; set; }

        [Display(Name = "Date Invoice")]
        //[Required(ErrorMessage = "Date Invoice is Required")]
        public DateTime? date_invoice { get; set; }

        [Display(Name = "PTP Date")]
        //[Required(ErrorMessage = "PTP Date is Required")]
        public DateTime? ptp_date { get; set; }

        [Display(Name = "Balance For Collection")]
        //[Required(ErrorMessage = "Balance For Collection is Required")]
        public int balance_for_collection { get; set; }

        [Display(Name = "Callout Status Level2")]
        //[Required(ErrorMessage = "Callout Status Level2 is Required")]
        public int callout_status_level2 { get; set; }

        [Display(Name = "Callout Status Level3")]
        // [Required(ErrorMessage = "Callout Status Level3 is Required")]
        public int callout_status_level3 { get; set; }

        [Display(Name = "Callout Status Level4")]
        //[Required(ErrorMessage = "Callout Status Level4 is Required")]
        public int callout_status_level4 { get; set; }

        [Display(Name = "Callout Status Level5")]
        // [Required(ErrorMessage = "Callout Status Level5 is Required")]
        public int callout_status_level5 { get; set; }


        [Display(Name = "Callout Status Level1")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Callout Status Level1 is Required")]
        public int callout_status_level1 { get; set; }

        [Display(Name = "City")]
        //[Required(ErrorMessage = "City is Required")]
        public String city { get; set; }

        [Display(Name = "Mode of Settlement")]
        //[Required(ErrorMessage  = "Mode of Settlement is Required")]
        public int mode_of_settlement { get; set; }

        [Display(Name = "Payment Channel")]
        //[Required(ErrorMessage = "Payment Channel is Required")]
        public int payment_channel { get; set; }

        [Display(Name = "Reason for Non Payment Level1")]
        //[Required(ErrorMessage = "Reason for Non Payment")]
        public int reason_for_non_payment_level1 { get; set; }

        [Display(Name = "Reason For Non Payment Level2")]
        //[Required(ErrorMessage = "Reason for Non Payment Level2 is Required")]
        public int reason_for_non_payment_level2 { get; set; }

        [Display(Name = "Region Primary")]
        //[Required(ErrorMessage  = "Region is Required")]
        public int region_primary { get; set; }

        [Display(Name = "Region Secondary")]
        //[Required(ErrorMessage  = "Region is Required")]
        public int region_secondary { get; set; }

        [Display(Name = "Skip Trace")]
        [Required(ErrorMessage = "Skip Trace is Required")]
        public int skip_trace { get; set; }

        [Display(Name = "Skip Trace Child")]
        //[Required(ErrorMessage = "Skip Trace is Required")]
        public int skip_trace_child { get; set; }

        [Display(Name = "Status")]
        //[Required(ErrorMessage  = "Status is Required")]
        public int status { get; set; }

        [Display(Name = "Recommendation")]
        //[Required(ErrorMessage  = "Recommendation is Required")]
        public string recommendation { get; set; }

        [Display(Name = "Remarks")]
        [Required(ErrorMessage = "Remarks is Required")]
        public string remarks { get; set; }

        [Display(Name = "Account_Name")]
        //[Required(ErrorMessage  = "Account_Name is Required")]
        public string account_name { get; set; }

        [Display(Name = "Account_No")]
        //[Required(ErrorMessage  = "Account_No is Required")]
        public string account_no { get; set; }

        [Display(Name = "Address")]
        //[Required(ErrorMessage  = "Address is Required")]
        public string address { get; set; }

        [Display(Name = "Age Group")]
        //[Required(ErrorMessage  = "Age Group is Required")]
        public string age_group { get; set; }

        [Display(Name = "Amount Collected")]
        //[Required(ErrorMessage  = "Amount Collected is Required")]
        public string amount_collected { get; set; }

        [Display(Name = "Authorized Representatives")]
        //[Required(ErrorMessage  = "Authorized Representatives is Required")]
        public string authorized_representatives { get; set; }

        [Display(Name = "Company Name")]
        //[Required(ErrorMessage  = "Company Name is Required")]
        public string company_name { get; set; }

        [Display(Name = "Phone1")]
        [Required(ErrorMessage = "Phone1 is Required")]
        public string phone1 { get; set; }

        [Display(Name = "Count Of Attempted Contact Numbers")]
        //[Required(ErrorMessage = "Count Of Attempted Contact Numbers is Required")]
        public string count_of_attempted_contact_numbers { get; set; }

        [Display(Name = "Email")]
        //[Required(ErrorMessage  = "Email is Required")]
        public string email { get; set; }

        [Display(Name = "Financial Contact Person")]
        //[Required(ErrorMessage = "Financial Contact Person is Required")]
        public string financial_contact_person { get; set; }

        [Display(Name = "Give New Contact No")]
        //[Required(ErrorMessage = "Give New Contact No is Required")]
        public string give_new_contact_no { get; set; }

        [Display(Name = "Invoice Amount")]
        //[Required(ErrorMessage  = "Invoice Amount is Required")]
        public string invoice_amount { get; set; }

        [Display(Name = "Invoice No")]
        //[Required(ErrorMessage  = "Invoice No is Required")]
        public string invoice_no { get; set; }

        [Display(Name = "Number of Accounts")]
        //[Required(ErrorMessage  = "Number of Accounts is Required")]
        public string number_of_accounts { get; set; }

        [Display(Name = "Outstanding Balance")]
        //[Required(ErrorMessage = "Outstanding Balance is Required")]
        public string outstanding_balance { get; set; }

        [Display(Name = "Overdue Balance")]
        //[Required(ErrorMessage  = "Overdue Balance is Required")]
        public string overdue_balance { get; set; }

        [Display(Name = "Phone2")]
        //[Required(ErrorMessage = "Phone1 is Required")]
        public string phone2 { get; set; }

        [Display(Name = "phone3")]
        //[Required(ErrorMessage = "phone2 is Required")]
        public string phone3 { get; set; }

        [Display(Name = "phone4")]
        //[Required(ErrorMessage  = "phone3 is Required")]
        public string phone4 { get; set; }

        [Display(Name = "PTP Amount")]
        //[Required(ErrorMessage  = "PTP Amount is Required")]
        public string ptp_amount { get; set; }

        [Display(Name = "Service Id No")]
        //[Required(ErrorMessage  = "Service Id No is Required")]
        public string service_id_no { get; set; }

        [Display(Name = "Service Type")]
        //[Required(ErrorMessage  = "Service Type is Required")]
        public string service_type { get; set; }

        [Display(Name = "PTP Dated By")]
        //[Required(ErrorMessage  = "PTP Dated By is Required")]
        public string ptp_dated_by { get; set; }

        public string phone_extension { get; set; }
        public bool enable_get_lead { get; set; }

        public int updatedtimes { get; set; }

        [ForeignKey("lead_id")]
        public virtual ICollection<LeadAssignmentModel> LeadAssignmentDetails { get; set; }

    }
}
