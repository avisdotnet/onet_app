﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class ModeOfSettlementModel:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string modeofsettlement_name { get; set; }
    }
}

