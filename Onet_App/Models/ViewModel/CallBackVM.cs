﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class CallBackVM:CallBackModel
    {
        public List<CallBackVM> _callbacklist { get; set; }
        public string agent_name_text { get; set; }
        public List<UserVM> UserList { get; set; }

        public CallBackVM()
        {
            _callbacklist = new List<CallBackVM>();
            UserList = new List<UserVM>();
        }
    }
}
