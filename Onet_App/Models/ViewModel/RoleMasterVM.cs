﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Onet_App.Models.ViewModel;

namespace Onet_App.Models.ViewModel
{
    public class RoleMasterVM:RoleMasterModel
    {
        public List<MenuMasterVM> MenuCheckList { get; set; }
        public List<RoleMasterVM> ParentRoleList { get; set; }
        public List<RoleMasterVM> RoleList { get; set; }
        public string parent_role_name { get; set; }

        public RoleMasterVM()
        {
            MenuCheckList = new List<MenuMasterVM>();
            RoleList = new List<RoleMasterVM>();
            ParentRoleList = new List<RoleMasterVM>();
        }
      
    }
}
