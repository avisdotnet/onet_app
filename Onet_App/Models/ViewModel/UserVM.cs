﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class UserVM : UserModel
    {
        [Required(ErrorMessage = "Password Required")]
        [Display(Name = "Password")]
        [CompareAttribute("user_password", ErrorMessage ="Password not Matched.")]
        public string confirm_password { get; set; }
        public string role_name_text { get; set; }
        
        public List<RoleMasterModel> RoleList { get; set; }
        public List<UserVM> UserVMList { get; set; }
        public List<UserModel> UserList { get; set; }
        public List<CampaignModel> CampaignList { get; set; }
        public List<UserGroupVM> UserGroupList { get; set; }
        public List<UserGroupAssignModel> UserGroupAssignList { get; set; }

        public bool is_selected { get; set; }
        public UserVM()
        {
            UserVMList = new List<UserVM>();
            RoleList = new List<RoleMasterModel>();
            UserList = new List<UserModel>();
            UserGroupList = new List<UserGroupVM>();
            CampaignList = new List<CampaignModel>();
        }

    }
}
