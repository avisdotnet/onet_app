﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class GroupUserAssignVM : GroupUserAssignModel
    {
        public List<GroupUserAssignVM> GroupUserAssignList { get; set; }
        public GroupUserAssignVM()
        {
            GroupUserAssignList = new List<GroupUserAssignVM>();
        }
    }
}
