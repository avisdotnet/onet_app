﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class PaymentFileUpdateVM: PaymentFileUpdateModel
    {
        public List<PaymentFileUpdateVM> paymentFileUpdateslist { get; set; }
        public PaymentFileUpdateVM()
        {
            paymentFileUpdateslist = new List<PaymentFileUpdateVM>();
        }
    }
}
