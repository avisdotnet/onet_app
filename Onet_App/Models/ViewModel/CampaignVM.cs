﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class CampaignVM : CampaignModel
    {
        public List<CampaignVM> _campaignslst { get; set; }
        public CampaignVM()
        {
            _campaignslst = new List<CampaignVM>();
        }
    }
}
