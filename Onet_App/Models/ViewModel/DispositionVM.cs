﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class DispositionVM:DispositionModel
    {
        public List<DispositionVM> _dispositionlst { get; set; }
        public List<DispositionVM> _parentDispositionList { get; set; }//For ParentDisposition
        public string parent_disposition_name { get; set; }
        public DispositionVM()
        {
            _dispositionlst = new List<DispositionVM>();
            _parentDispositionList = new List<DispositionVM>();
        }
    }
}
