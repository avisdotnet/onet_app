﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class RemarkDownloadVM
    {
       
        public string account_name { get; set; }
        public string account_no { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string phone3 { get; set; }
        public string email { get; set; }
        public int lead_id { get; set; }
        public int campaign_id { get; set; }
        public string remark { get; set; }
        public string createdby { get; set; }
        public DateTime createdon { get; set; }
    }
}
