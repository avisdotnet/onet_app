﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Onet_App.Models;
using Onet_App.Models.ViewModel;

namespace Onet_App.Models.ViewModel
{
    public class TemplateVM:TemplateModel
    {
      public List<TemplateVM> _tempList { get; set; }

        public TemplateVM()
        {
            _tempList = new List<TemplateVM>();
        }
    }
}
