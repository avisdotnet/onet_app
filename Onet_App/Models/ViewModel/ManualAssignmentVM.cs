﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class ManualAssignmentVM
    {
        [Display(Name = "Campaign Name")]
        public List<CampaignModel> campaignModels { get; set; }

        [Display(Name = "Endorser Name")]
        public List<EndorserModel> endorserModels { get; set; }

        [Display(Name = "List Name")]
        public List<ListModel> listModels { get; set; }

        [Display(Name = "Agent List")]
        public List<UserModel> agentList { get; set; }

        public int Campaign_id { get; set; }
        public int Endorser_id { get; set; }
        public string Listname { get; set; }
        public int Agent_id { get; set; }
        public String AccountName { get; set; }
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Only Numeric allowed.")]
        public String RowCount { get; set; }
        public List<CRMVM> LeadList { get; set; }
        public ManualAssignmentVM()
        {
            campaignModels = new List<CampaignModel>();
            endorserModels = new List<EndorserModel>();
            listModels = new List<ListModel>();
            LeadList = new List<CRMVM>();
            agentList = new List<UserModel>();
        }
    }
}
