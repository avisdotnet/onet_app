﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel 
{
    public class RemarkVM : RemarksModel
    {
      public List<RemarksModel> RemarkModelList { get; set; }
        public List<RemarkVM> RemarkVMList { get; set; }

        public RemarkVM()
        {
            RemarkModelList = new List<RemarksModel>();
            RemarkVMList = new List<RemarkVM>();
        }
    }
}
