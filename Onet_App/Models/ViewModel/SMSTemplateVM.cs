﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class SMSTemplateVM : SMSTemplateModel
    {
        public List<SMSTemplateModel> SMSTemplateList { get; set; }
        public List<SMSTemplateVM> SMSTemplateVMList { get; set; }
        public List<KeywordsCreationVM> KeywordList { get; set; }

        public string keyword { get; set; }

        public SMSTemplateVM()
        {
            SMSTemplateVMList = new List<SMSTemplateVM>();
            SMSTemplateList = new List<SMSTemplateModel>();
            KeywordList = new List<KeywordsCreationVM>();
        }
    }
}
