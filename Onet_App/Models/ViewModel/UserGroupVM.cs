﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class UserGroupVM : UserGroupModel
    {
        public List<UserGroupVM> UserGroupList { get; set; }
        public List<CampaignVM> campaignlist { get; set; }
        public string campaign_name { get; set; }
        public bool is_selected { get; set; }
        public List<UserVM> NewAgentList { get; set; }
        public List<UserVM> ExistingAgentList { get; set; }
        public List<GroupUserAssignModel> _agentlist { get; set; }
        public UserVM user_obj { get; set; }
        public UserGroupVM()
        {
            UserGroupList = new List<UserGroupVM>();
            campaignlist = new List<CampaignVM>();
            NewAgentList = new List<UserVM>();
            ExistingAgentList = new List<UserVM>();
            _agentlist = new List<GroupUserAssignModel>();
            user_obj = new UserVM();
        }
    }
}
