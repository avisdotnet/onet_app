﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class CrmActivityVM
    {

        public int id { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string phone3 { get; set; }
        public string email { get; set; }
        public int lead_id { get; set; }
        public string activity_name { get; set; }
        public DateTime time_stamp { get; set; }
        public string status { get; set; }
        public string summary { get; set; }
        public int campaign_id { get; set; }
        public DateTime createdon { get; set; }
    }
}
