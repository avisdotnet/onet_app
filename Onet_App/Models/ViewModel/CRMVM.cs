﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Onet_App.Models.ViewModel
{
    public class CRMVM : CRMModel
    {

        public string OnetId { get; set; }
        public bool isautomate { get; set; }
        public List<CRMVM> _crmlst { get; set; }
        public List<CRMDATAVM> cRMDATAVMs { get; set; }
        public int select_callback { get; set; }
        public List<DispositionVM> dispositionModels { get; set; }
        public List<CityModel> cityModels { get; set; }
        public List<StatusModel> statusModels { get; set; }
        public List<SkipTraceModel> skipTraceModels { get; set; }
        public List<ReasonForNonPaymentModel> reasonForNonPaymentModels { get; set; }
        public List<ModeOfSettlementModel> modeOfSettlementModels { get; set; }
        public List<RegionModel> regionModel { get; set; }
        public List<PaymentChannelModel> paymentChannelModels { get; set; }
        public List<BalanceForCollectionModel> balanceForCollectionModels { get; set; }
        public List<CampaignVM> campaignlist { get; set; }
        public string campaign_name { get; set; }
        public DateTime callback_date_time { get; set; }
        public UserVM userVM { get; set; }
        public List<CRMModel> CRMList { get; set; }
        public List<CRMModel> CRMModelListAll { get; set; }
        public List<CRMVM> CallBackList { get; set; }
        public List<LeadAssignmentVM> LeadAssignmentVMList { get; set; }
        public List<RemarkVM> remarklist { get; set; }
        public bool is_selected { get; set; }
        public string activity_status { get; set; }
        public string activity_summary { get; set; }
        public List<ActivityHistoryVM> activityHistoryList { get; set; }
        public string activity_name { get; set; }
        public DateTime time_stamp { get; set; }
        public string filter_text { get; set; }
        public DateTime filter_date { get; set; }
        public string role_name { get; set; }
        public string callout_status_level1_Text { get; set; }
        public string callout_status_level2_Text { get; set; }
        public string callout_status_level3_Text { get; set; }
        public string callout_status_level4_Text { get; set; }
        public string callout_status_level5_Text { get; set; }
        public string reason_for_non_payment_level1_Text { get; set; }
        public string reason_for_non_payment_level2_Text { get; set; }
        public string Balance_For_Collection_Text { get; set; }
        public String Mode_of_settlement_Text { get; set; }
        public String Payment_channel_Text { get; set; }
        public String region_primary_text { get; set; }
        public String region_secondary_text { get; set; }
        public String Skip_trace_Text { get; set; }
        public String Child_Skip_trace_Text { get; set; }
        public String Status_Text { get; set; }
        public List<RemarkDownloadVM> RemarkDownloadList { get; set; }
        public List<CrmActivityVM> CrmActivitylst { get; set; }
        public List<EndorserModel> EndorserModelList { get; set; }
        public List<ListModel> listModels { get; set; }
        public string endorsement_name { get; set; }
        public string Agent_name { get; set; }
        public CRMVM()
        {
            _crmlst = new List<CRMVM>();
            dispositionModels = new List<DispositionVM>();
            campaignlist = new List<CampaignVM>();
            CRMList = new List<CRMModel>();
            CRMModelListAll = new List<CRMModel>();
            CallBackList = new List<CRMVM>();
            cityModels = new List<CityModel>();
            statusModels = new List<StatusModel>();
            paymentChannelModels = new List<PaymentChannelModel>();
            regionModel = new List<RegionModel>();
            modeOfSettlementModels = new List<ModeOfSettlementModel>();
            reasonForNonPaymentModels = new List<ReasonForNonPaymentModel>();
            skipTraceModels = new List<SkipTraceModel>();
            balanceForCollectionModels = new List<BalanceForCollectionModel>();
            remarklist = new List<RemarkVM>();
            activityHistoryList = new List<ActivityHistoryVM>();
            RemarkDownloadList = new List<RemarkDownloadVM>();
            CrmActivitylst = new List<CrmActivityVM>();
            EndorserModelList = new List<EndorserModel>();
            listModels = new List<ListModel>();
            cRMDATAVMs = new List<CRMDATAVM>();
        }
    }
}
