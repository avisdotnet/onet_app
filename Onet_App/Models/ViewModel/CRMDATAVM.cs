﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class CRMDATAVM
    {
      

        public string Onet_Id { get; set; }
        public string Account_Name { get; set; }
        public string Account_No { get; set; }
        public string Activity_Name { get; set; }
        public string Activity_Status { get; set; }
        public string Activity_Summary { get; set; }
        public string Address { get; set; }
        public string Age_Group { get; set; }
        public string Agent_Name { get; set; }
        public string Amount_Collected { get; set; }
        public string Authorized_Representatives { get; set; }
        public String Balance_For_Collection { get; set; }
        public string Callout_Status_Level1 { get; set; }
        public string Callout_Status_Level2 { get; set; }
        public string Callout_Status_Level3 { get; set; }
        public string Callout_Status_Level4 { get; set; }
        public string Callout_Status_Level5 { get; set; }
        public string Campaign_Name { get; set; }
        public String City { get; set; }
        public string Company_Name { get; set; }
        public string Count_of_Attempted { get; set; }
        public DateTime Created_On { get; set; }
        public DateTime? Date_Invoice { get; set; }
        public string Email { get; set; }
        public string Financial_Contact_Person { get; set; }
        public string Invoice_Amount { get; set; }
        public string Invoice_No { get; set; }
        public String List_Name { get; set; }
        public String Mode_of_Settlement { get; set; }
        public string New_Contact_No { get; set; }
        public string Number_of_Accounts { get; set; }
        public string Outstanding_Balance { get; set; }
        public string Overdue_Balance { get; set; }
        public String Payment_Channel { get; set; }
        public string Phone_Extension { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Phone4 { get; set; }
        public string PTP_Amount { get; set; }
        public DateTime? PTP_Date { get; set; }
        public string PTP_Dated_By { get; set; }
        public string Reason_for_Non_Payment_Level1 { get; set; }
        public string Reason_for_Non_Payment_Level2 { get; set; }
        public string Recommendation { get; set; }
        public String Region_Primary { get; set; }
        public String Region_Secondary { get; set; }
        public string Remarks { get; set; }
        public string Service_Id { get; set; }
        public string Service_Type { get; set; }
        public String Skip_Trace_Level1 { get; set; }
        public String Skip_Trace_Level2 { get; set; }
        public String Status_Text { get; set; }
        public int Updated_Count { get; set; }
        public DateTime Updated_On { get; set; }
        public string dailer_disposition { get; set; }
        public string last_called_phoneno { get; set; }
    }
}
