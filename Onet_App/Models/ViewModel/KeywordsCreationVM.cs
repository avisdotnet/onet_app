﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class KeywordsCreationVM:KeywordsCreationModel
    {
        public List<KeywordsCreationVM> keywordsList { get; set; }
        public KeywordsCreationVM()
        {
            keywordsList = new List<KeywordsCreationVM>();
        }
    }
}
