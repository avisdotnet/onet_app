﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class EmailTemplateVM: EmailTemplateCreationModel
    {
        public List<EmailTemplateCreationModel> emailTemplateList { get; set; }
        public List<EmailTemplateVM> _emailtemplatelst { get; set; }
        public List<KeywordsCreationVM> keywordlst { get; set; }
        public string keyword { get; set; }
        public EmailTemplateVM()
        {
            _emailtemplatelst = new List<EmailTemplateVM>();
            keywordlst = new List<KeywordsCreationVM>();
            emailTemplateList = new List<EmailTemplateCreationModel>();
        }
    }
}
