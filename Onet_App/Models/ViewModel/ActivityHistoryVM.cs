﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class ActivityHistoryVM:ActivityHistoryModel
    {
        public List<ActivityHistoryVM> activityHistoryList { get; set; }
        public ActivityHistoryVM()
        {
            activityHistoryList = new List<ActivityHistoryVM>();
        }
    }
}
