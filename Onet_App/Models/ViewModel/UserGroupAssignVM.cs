﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class UserGroupAssignVM : UserGroupAssignModel
    {
        public List<UserGroupAssignVM> UserGroupAssignList { get; set; }
        public UserGroupAssignVM()
        {
            UserGroupAssignList = new List<UserGroupAssignVM>();
        }
    }
}
