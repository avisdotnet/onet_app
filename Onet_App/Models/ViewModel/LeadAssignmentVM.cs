﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models.ViewModel
{
    public class LeadAssignmentVM : LeadAssignmentModel
    {
        public List<LeadAssignmentModel> LeadAssignmentList { get; set; }
        public List<LeadAssignmentVM> LeadAssignmentVMList { get; set; }

        public List<CampaignModel> campaignlist { get; set; }
        public List<UserModel> agentlist { get; set; }
        public List<CRMVM> LeadList { get; set; }
        public string campaign_name { get; set; }
        public string agent_name { get; set; }
        public int agent_id_1 { get; set; }
        public int agent_id_2 { get; set; }
        public string phone1 { get; set; }
        public String RowCount { get; set; }
        public LeadAssignmentVM()
        {
            LeadAssignmentList = new List<LeadAssignmentModel>();
            LeadAssignmentVMList = new List<LeadAssignmentVM>();
            campaignlist = new List<CampaignModel>();
            agentlist = new List<UserModel>();
            LeadList = new List<CRMVM>();
        }
    }
}
