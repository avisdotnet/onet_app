﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class SMSTemplateModel : CommonAttributeModel
    { 
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Enter Template Name")]
        [RegularExpression(@"^\w+$", ErrorMessage ="Special Characters and Spaces not allowed")]
        public string sms_template_name { get; set; }

       
        [Required(ErrorMessage ="Enter Template Definition")]
        //[RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Special Characters not allowed")]
        [RegularExpression(@"^[a-zA-Z0-9\s#]+$", ErrorMessage = "Special Characters not allowed")]
        public string sms_template_definition { get; set; }

        [Required(ErrorMessage = "Enter Template Description")]
        public string sms_template_description { get; set; }
      
    }
}
