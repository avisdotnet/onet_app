﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class UserGroupModel : CommonAttributeModel
    {
        [Key]
        public int group_id { get; set; }
        [Required(ErrorMessage = "User Groupname Required")]
        public string group_name { get; set; }
        [Range(0,Int16.MaxValue,ErrorMessage ="Campaign Name Required")]
        public int campaign_id { get; set; }
        [ForeignKey("group_id")]
        public virtual ICollection<GroupUserAssignModel> GroupUserAssignModelDetails { get; set; }
        
    }
}
