﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class ReasonForNonPayementChildModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string reason_for_non_payment_child_name { get; set; }
        public int reason_for_non_payment_parent { get; set; }
    }
}
