﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class CallBackModel:CommonAttributeModel
    {
        [Key]
        public int callback_id { get; set; }
        public int lead_id { get; set; }
        //public int agent_id { get; set; }
        public DateTime callback_date_time { get; set; }
        public bool isattempted { get; set; }
    }
}
