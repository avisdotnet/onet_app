﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class CityModel:CommonAttributeModel
    {
        [Key]
        public int city_id { get; set; }
        public string city_name { get; set; }
    }
}

