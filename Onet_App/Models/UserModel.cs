﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class UserModel : CommonAttributeModel
    {
        [Key]
        public int user_id { get; set; }

        [Required(ErrorMessage = "Username Required")]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password Required")]
        [Display(Name = "Password")]
        public string user_password { get; set; }

        [Required(ErrorMessage ="User Fullname Required")]
        [Display(Name ="User Fullname")]
        public string user_fullname{ get; set; }

        [Display(Name = "IsSuperAdmin")]
        public bool is_superadmin { get; set; }

        [Required(ErrorMessage = "Role Name Required")]
        [Display(Name = "Role Name")]
        public int role_name { get; set; }

       
        [Display(Name = "Campaign Name")]
        public int campaign_id { get; set; }

        
        [Display(Name = "UserGroup Name")]
        public int usergroup_id { get; set; }

        [ForeignKey("user_id")]
        public virtual ICollection<UserGroupAssignModel> UserGroupAssignDetails { get; set; }

    }
}
