﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class RegionSecondaryModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public string region_secondary { get; set; }
        public int parent_region_id { get; set; }
    }
}
