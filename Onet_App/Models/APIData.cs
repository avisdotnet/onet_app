﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class APIData
    {
        [Key]
        public int id { get;set; }
        public DateTime lastapitime { get; set; }
    }
}
