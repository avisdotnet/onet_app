﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Onet_App.Models.ViewModel;
using System.ComponentModel.DataAnnotations;

namespace Onet_App.Models
{
    public class TemplateModel : CommonAttributeModel
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Template Name")]
        public string template_name { get; set; }

        [Display(Name ="Template Area Name")]
        public string template_areaname { get; set; }

        [Display(Name = "Template Controller Name")]
        public string template_controller_name { get; set; }

        [Display(Name = "Template Action Name")]
        public string template_action_name { get; set; }

        [Display(Name = "Template Query String")]
        public string template_query_string { get; set; }

        
    }
   
}
