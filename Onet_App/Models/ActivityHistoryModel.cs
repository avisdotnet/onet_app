﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onet_App.Models
{
    public class ActivityHistoryModel:CommonAttributeModel
    {
        [Key]
        public int id { get; set; }
        public int lead_id { get; set; }

        [Display(Name = "Activity Name")]
        [Required(ErrorMessage = "Activity Name is Required")]
        public string activity_name { get; set; }

        [Display(Name = "Time Ntamp")]
        [Required(ErrorMessage = "Time Stamp is Required")]
        public DateTime time_stamp { get; set; }

        [Display(Name = "Status")]
        [Required(ErrorMessage = "Status is Required")]
        public string status { get; set; }

        [Display(Name = "Summary")]
        [Required(ErrorMessage = "Summary is Required")]
        public string summary  { get; set; }
    }
}
